/*Sanoj*/
package com.utilities;

import java.util.ArrayList;

import Flight.Classes.Address;
import Flight.Classes.ReservationInfo;
import Flight.Classes.SearchObject;
import Flight.Classes.Traveler;

public class FillReservationDetails 
{
	
	public ReservationInfo Fill_Reservation_details(SearchObject sobj)
	{
		String[] name = {"one","two","three","four","five","six","seven","eight","nine"};
		ReservationInfo resvFillobj = new ReservationInfo();
		int Noofadults = Integer.parseInt(sobj.getAdult());
		int Noofchildren = Integer.parseInt(sobj.getChildren());
		int Noofinfants = Integer.parseInt(sobj.getInfant());
		ArrayList<Traveler> adultlist = new ArrayList<Traveler>();
		ArrayList<Traveler> childrenlist = new ArrayList<Traveler>();
		ArrayList<Traveler> infantlist = new ArrayList<Traveler>(); 
		String adultfname = "adf";
		String adultsurname = "ads";
		String childfname = "chf";
		String childsurname = "chs";
		String infantfname = "inff";
		String infantsurname = "infs";
		
		Traveler maincustomer = new Traveler();
		maincustomer.setGivenName("Dan");
		maincustomer.setSurname("Win");
		
		Address add = new Address();
		add.setAddressStreetNo("Third eye St.");
		add.setAddressCity("Christmas");
		add.setAddressCountry("Christmas Island");
		
		maincustomer.setAddress(add);
		maincustomer.setPhoneNumber("2333444");
		maincustomer.setEmail("sanoj@colombo.rezgateway.com");
		
		resvFillobj.setMaincustomer(maincustomer);
		
		for(int a = 0; a<Noofadults; a++)
		{
			adultfname = "Dan";
			adultsurname = "Win";
			adultfname = adultfname.concat(name[a]);
			adultsurname = adultsurname.concat(name[a]);
			Traveler adult = new Traveler();
			adult.setPassengertypeCode("ADT");
			adult.setNamePrefixTitle("Mr");
			adult.setGivenName(adultfname);
			adult.setSurname(adultsurname);
			adult.setPhoneNumber("2212121");
			adult.setPassportNo("123ew341231");
			adult.setPassportExpDate("01-01-2020");
			adult.setPassprtIssuDate("01-01-2010");
			adult.setBirthDay("01-01-1995");
			adult.setGender("Male");
			adult.setNationality("Christmas Island");
			adult.setPassportIssuCountry("Christmas Island");
			adult.setPhoneType("HOME");
			adultlist.add(adult);
		}
		resvFillobj.setAdult(adultlist);
		
		for(int c = 0; c<Noofchildren; c++)
		{
			childfname = "DanS";
			childsurname = "WinS";
			childfname = childfname.concat(name[c]);
			childsurname = childsurname.concat(name[c]);
			Traveler child = new Traveler();
			child.setPassengertypeCode("CHD");
			child.setNamePrefixTitle("Mst");
			child.setGivenName(childfname);
			child.setSurname(childsurname);
			child.setPhoneNumber("2434343");
			child.setPassportNo("111r1111e1e11");
			child.setPassportExpDate("01-01-2020");
			child.setPassprtIssuDate("01-01-2010");
			child.setBirthDay("01-01-2005");
			child.setGender("Male");
			child.setNationality("Christmas Island");
			child.setPassportIssuCountry("Christmas Island");
			child.setPhoneType("HOME");
			childrenlist.add(child);
		}
		resvFillobj.setChildren(childrenlist);
		
		for(int c = 0; c<Noofinfants; c++)
		{
			infantfname = "DanB";
			infantsurname = "WinB";
			infantfname = infantfname.concat(name[c]);
			infantsurname = infantsurname.concat(name[c]);
			Traveler infant = new Traveler();
			infant.setPassengertypeCode("INF");
			infant.setNamePrefixTitle("Mst");
			infant.setGivenName(infantfname);
			infant.setSurname(infantsurname);
			infant.setBirthDay("01-01-2014");
			infant.setGender("Male");
			infant.setNationality("Christmas Island");
			infant.setPassportNo("1a11s11d1f1d1");
			infantlist.add(infant);
		}
		resvFillobj.setInfant(infantlist);
		
		return resvFillobj;
	}

}
