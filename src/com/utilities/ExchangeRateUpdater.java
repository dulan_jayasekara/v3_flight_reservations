/*Sanoj*/
package com.utilities;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;
//import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;


public class ExchangeRateUpdater 
{
	
	private HashMap<String, String>  CurrencyMap = new HashMap<String, String>();
	//private WebDriver              driver		 = null;
	private boolean                  Validator   = true;
	private int                      Count       = 0;
	HashMap<String, String> 		 PropertyMap = new HashMap<String, String>();
	SupportMethods 					 SUP 		 = null;

	public Map<String, String> getExchangeRates(HashMap<String, String> PropMap, WebDriver driver )throws IOException
	{   
		PropertyMap = PropMap;
		SUP 		= new SupportMethods(PropertyMap);
		
	    //driver.get(PropertyMap.get("Portal.Url").concat("/admin/setup/CurrencyExchangeRateSetupPage.do"));
	    driver.get(PropertyMap.get("Portal.Url").concat("/admin/setup/CurrencyExchangeRateSetupPage.do"));
	    
	
	    for(int count = 0;Validator;count++)
	    {
	    	try 
	    	{
	    		
	    		String Key   = driver.findElement(By.name("currencyCode["+count+"]")).getAttribute("value");
	    		String Value = driver.findElement(By.name("exchangeRate["+count+"]")).getAttribute("value");
	    		CurrencyMap.put(Key, Value);
	    		
			} 
	    	catch (Exception e) 
	    	{	
				Validator = false;
				Count     = count;	
			}
	    	
	    }
	    
		System.out.println(Count);
		//driver.findElement(By.id("main_mnu_5")).click();
		//driver.manage().timeouts().implicitlyWait(3, TimeUnit.SECONDS);
		//driver.quit();
		
		return CurrencyMap;
	}
	

}

