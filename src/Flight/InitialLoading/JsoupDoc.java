/*Sanoj*/
package Flight.InitialLoading;


import java.io.File;
//import java.io.FileReader;
import java.io.IOException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.concurrent.TimeUnit;
//import java.util.Map;
//import java.util.Properties;

import org.apache.log4j.Logger;
//import org.apache.log4j.xml.DOMConfigurator;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.firefox.FirefoxProfile;
/*import org.openqa.selenium.support.ui.ExpectedCondition;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;*/

import com.utilities.SupportMethods;
import com.utilities.Repository;

import Flight.TypeEnum.XMLFileType;
import Flight.TypeEnum.XMLLocateType;

public class JsoupDoc {
	

	Logger 							logger				= null;
	private HashMap<String, String> Propertymap 		= null;
	File							input				= null;
	SupportMethods SUP = null;
	
	public JsoupDoc(HashMap<String, String> Propymap)
	{
	    	Propertymap         = Propymap;
	    	SUP = new SupportMethods(Propertymap);
	}
	

	public String urlGenerator()
	{
		//logger.debug("Generating URL (Append current date)....");
		String URL1 = "";
		String URL = "";
		String url = Propertymap.get("URL");
		//logger.info("URL loaded from property map");
		
		DateFormat dateFormat = new SimpleDateFormat("dd-MM-yyyy");
		Calendar cal = Calendar.getInstance();
		String DATE = dateFormat.format(cal.getTime());
		//System.out.println(DATE);
		//logger.debug("Replace the Word DATE by current system date");
		URL1 = url.replace("DATE", DATE);
		URL = URL1.replace("SUPPLIER", Propertymap.get("Flight_Supplier"));
		
		System.out.println(URL);
		//logger.info("Returning URL ");
		return URL;
		
	}
	
	public WebDriver initializeDriver()
	{
		//logger.debug("Initializing Web driver....");
		WebDriver driver = new FirefoxDriver(new FirefoxProfile(new File(Propertymap.get("Profile.Path"))));

		return driver;	
	}
	
	public Document createDoc(XMLLocateType Locator,String Value, XMLFileType tracerPrefix)
	{
		//WebDriverWait wait = new WebDriverWait(driver, 10);
		Document RetDoc = null;
		if(Locator == XMLLocateType.FILEPATH)
		{
			input = new File(Value);
			try 
			{
				//logger.debug("Start parsing file to document");
				RetDoc   = Jsoup.parse(input,"UTF-8");
				//logger.info("File is successfully converted to Jsoup Doc..");
			} 
			catch (IOException e) 
			{
				//logger.fatal("File could not be converted to Jsoup document..!!");
				e.printStackTrace();
			}
		}
		else if(Locator == XMLLocateType.TRACER)
		{
			WebDriver driver		= null;
			//WebDriverWait wait2 	= new WebDriverWait(driver, 10);
			String PageSource		= "";	
			String TracerPrefix		= tracerPrefix.toString();
			String TracerValue		= Value;
			String TracerID			= TracerPrefix.concat(TracerValue);

			try 
			{
				driver = SUP.initalizeDriver();
			} 
			catch (IOException e) 
			{
				//e.printStackTrace();
			}
			String URL = urlGenerator();
			
			driver.get(URL);
			//logger.info("Driver get URL");
			String urlall = "";
			try
			{
				//wait.until(ExpectedConditions.presenceOfElementLocated(By.partialLinkText(TracerID)));
				driver.findElement(By.partialLinkText(TracerID)).click();
				ArrayList<String> windows = new ArrayList<String> (driver.getWindowHandles());
				
				driver.switchTo().window(windows.get(1));
				urlall = driver.getCurrentUrl();
				driver.manage().timeouts().implicitlyWait(7, TimeUnit.SECONDS);
				PageSource = driver.getPageSource();
				driver.close();	
				ArrayList<String> windowsall = new ArrayList<String> (driver.getWindowHandles());
				driver.switchTo().window(windowsall.get(0));
				driver.manage().timeouts().implicitlyWait(4, TimeUnit.SECONDS);
				driver.close();
				//driver.quit();
				RetDoc = Jsoup.parse(PageSource, "UTF-8");
			}
			catch(Exception e)
			{
				e.printStackTrace();
				//driver.quit();
			}
			
			if(TracerID.contains("LowFareSearchRequest"))
			{
				Repository.LowFareSearchRequest = urlall;
			}
			else if(TracerID.contains("LowFareSearchResponse"))
			{
				Repository.LowFareSearchResponse = urlall;
			}
			else if(TracerID.contains("ReservationRequest"))
			{
				Repository.ReservationRequest = urlall;
			}
			else if(TracerID.contains("ReservationResponse"))
			{
				Repository.ReservationResponse = urlall;
			}
			else if(TracerID.contains("PrefAirlineSearchRequest"))
			{
				Repository.PrefAirlineSearchRequest = urlall;
			}
			else if(TracerID.contains("PrefAirlineSearchResponse"))
			{
				Repository.PrefAirlineSearchResponse = urlall;
			}
			else if(TracerID.contains("PriceRequest"))
			{
				Repository.PriceRequest = urlall;
			}
			else if(TracerID.contains("PriceResponse"))
			{
				Repository.PriceResponse = urlall;
			}
			else if(TracerID.contains("ETicketRequest"))
			{
				Repository.ETicketRequest = urlall;
			}
			else if(TracerID.contains("ETicketResponse"))
			{
				Repository.ETicketResponse = urlall;
			}
			else if(TracerID.contains("PNRUpdateRequest"))
			{
				Repository.PNRUpdateRequest = urlall;
			}
			else if(TracerID.contains("PNRUpdateResponse"))
			{
				Repository.PNRUpdateResponse = urlall;
			}
			else if(URL.contains("Cancellationrequest"))
			{
				Repository.Cancellationrequest = urlall;
			}
			else if(URL.contains("CancellationResponse"))
			{
				Repository.CancellationResponse = urlall;
			}
		}
		
		return RetDoc;
	}

}
