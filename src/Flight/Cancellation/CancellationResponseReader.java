package Flight.Cancellation;

//import java.util.ArrayList;
import java.util.HashMap;

//import javax.lang.model.util.Elements;

import org.jsoup.nodes.Document;
//import org.jsoup.nodes.Element;

import Flight.Classes.XMLError;
import Flight.InitialLoading.JsoupDoc;
import Flight.TypeEnum.XMLFileType;
import Flight.TypeEnum.XMLLocateType;

public class CancellationResponseReader
{
	//Logger 							logger				= null;
	private HashMap<String, String> Propertymap 		= null;
	JsoupDoc						getdoc				= null;
		
	public CancellationResponseReader(HashMap<String, String> Propmap)
	{
		Propertymap = Propmap;
		getdoc = new JsoupDoc(Propertymap);
	}
		
	public CancellationResponse RequestReader(XMLLocateType Value, String Val, XMLFileType type)
	{
		Document doc = getdoc.createDoc(Value, Val, type);
		CancellationResponse cancellationResponse = new CancellationResponse();
			
		XMLError error = new XMLError();
		try
		{
			error.setErrorMessage(doc.getElementsByTag("Error").text());
			error.setErrortype(doc.getElementsByTag("Error").attr("Type"));
			error.setErrorcode(doc.getElementsByTag("Error").attr("Code"));
		}
		catch(Exception e)
		{
			e.printStackTrace();
		}
			
		cancellationResponse.setError(error);
		
		if(cancellationResponse.getError().getErrorMessage().equals(""))
		{
			try {
								
				String general = "";
				
				
				//general = list.get(0).attr("Status");
				//org.jsoup.select.Elements ele = doc.getElementsByAttributeValue("Status", "Cancelled");
				//general = ele.get(0).attr("Status");
				general = doc.getElementsByTag("OTA_CancelRS").attr("Status");
				cancellationResponse.setStatus(general);
					
				general = doc.getElementsByTag("UniqueID").attr("ID");
				cancellationResponse.setUniqueID(general);
					
			} catch (Exception e) {
				e.printStackTrace();
			}
		}

		return cancellationResponse;
	}
}
