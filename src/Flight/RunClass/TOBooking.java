package Flight.RunClass;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import Flight.Classes.UIConfirmationPage;
//import system.classes.TO;

//import system.pages.WebConfirmationPage;

public class TOBooking
{
	public void bookingListReportTOBfinal(WebDriver driver, UIConfirmationPage webConfirmationPage, StringBuffer ReportPrinter)
	{
		try {
			driver.findElement(By.xpath(".//*[@id='partnermenuitems']/table[4]/tbody/tr[14]/td[2]/a")).click();
			driver.switchTo().defaultContent();
			driver.switchTo().frame("reportIframe");
			driver.findElement(By.id("reservationno")).clear();
			driver.findElement(By.id("reservationno")).sendKeys(webConfirmationPage.getReservationNo());
			//System.out.println(webConfirmationPage);
			driver.findElement(By.id("reservationno_lkup")).click();
			driver.switchTo().defaultContent();
			driver.switchTo().frame("lookup");
			driver.findElement(By.className("rowstyle0")).click();
			driver.switchTo().defaultContent();
			driver.switchTo().frame("reportIframe");
		} catch (Exception e) {
			
		}
		
		WebDriverWait wait = new WebDriverWait(driver, 5);
		
		try {
			wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath(".//*[@id='topControlBar']/tbody/tr/td[2]/table/tbody/tr/td[2]/table/tbody/tr[2]/td[3]/a/img")));
		} catch (Exception e) {
			System.out.println("Control bar missing");
		}
		driver.findElement(By.xpath(".//*[@id='topControlBar']/tbody/tr/td[2]/table/tbody/tr/td[2]/table/tbody/tr[2]/td[3]/a/img")).click();
		String invoice = "";
		String payment = "";
		String voucher = "";
		
		if(driver.findElements(By.id("imgiorange")).size() > 0)
		{
			invoice = "not issued";
		}
		else
		{
			invoice = "issued";
		}
		
		if(driver.findElements(By.id("imgporange")).size() > 0)
		{
			payment = "not issued";
		}
		else
		{
			payment = "issued";
		}
		
		if(driver.findElements(By.id("imgvorange")).size() > 0)
		{
			voucher = "not issued";
		}
		else
		{
			voucher = "issued";
		}
		
		ReportPrinter.append("<span><center><p class='Hedding0'>Notifications before issueing the voucher</p></center></span>");
		ReportPrinter.append("<table style=width:100%>"
		+ "<tr><th>Notification</th>"
		+ "<th>Status</th></tr>");
		/*ReportPrinter.append("<br>");
		ReportPrinter.append("<span>Notifications before issueing the voucher</span>");
		ReportPrinter.append("<br>");*/
		if(invoice.equals("not issued") && payment.equals("not issued") && voucher.equals("not issued"))
		{
			ReportPrinter.append("<tr>");
			ReportPrinter.append("<td>Invoice</td>");
			ReportPrinter.append("<td class = 'Passed'>"+ invoice +"</td>");
			ReportPrinter.append("</tr>");
			
			ReportPrinter.append("<tr>");
			ReportPrinter.append("<td>Payment</td>");
			ReportPrinter.append("<td class = 'Passed'>"+ payment +"</td>");
			ReportPrinter.append("</tr>");
			
			ReportPrinter.append("<tr>");
			ReportPrinter.append("<td>Voucher</td>");
			ReportPrinter.append("<td class = 'Passed'>"+ voucher +"</td>");
			ReportPrinter.append("</tr>");
			
			/*ReportPrinter.append("<br>");
			ReportPrinter.append("<span class = 'NotIssued'>Payment - "+ payment +"</span>");
			ReportPrinter.append("<br>");
			
			ReportPrinter.append("<br>");
			ReportPrinter.append("<span class = 'NotIssued'>Voucher - "+ voucher +"</span>");
			ReportPrinter.append("<br>");*/
		}
		else if(!invoice.equals("not issued"))
		{
			ReportPrinter.append("<tr>");
			ReportPrinter.append("<td>Invoice</td>");
			ReportPrinter.append("<td class = 'Failed'>"+ invoice +"</td>");
			ReportPrinter.append("</tr>");
			
			/*ReportPrinter.append("<br>");
			ReportPrinter.append("<br>");
			ReportPrinter.append("<span class = 'Failed'>Invoice - "+ invoice +"</span>");
			ReportPrinter.append("<br>");*/
		}
		else if(!payment.equals("not issued"))
		{
			ReportPrinter.append("<tr>");
			ReportPrinter.append("<td>Payment</td>");
			ReportPrinter.append("<td class = 'Failed'>"+ payment +"</td>");
			ReportPrinter.append("</tr>");
			
			/*ReportPrinter.append("<br>");
			ReportPrinter.append("<br>");
			ReportPrinter.append("<span class = 'Failed'>Payment - "+ payment +"</span>");
			ReportPrinter.append("<br>");*/
		}
		else if(!voucher.equals("not issued"))
		{
			ReportPrinter.append("<tr>");
			ReportPrinter.append("<td>Voucher</td>");
			ReportPrinter.append("<td class = 'Failed'>"+ voucher +"</td>");
			ReportPrinter.append("</tr>");
			
			/*ReportPrinter.append("<br>");
			ReportPrinter.append("<br>");
			ReportPrinter.append("<span class = 'Failed'>Voucher - "+ voucher +"</span>");
			ReportPrinter.append("<br>");*/
		}
		
		ReportPrinter.append("</table>");
		ReportPrinter.append("<br><br><br>");
		
		driver.findElement(By.xpath(".//*[@id='reportData1']/tbody/tr[5]/td[19]/a/img")).click();
		driver.switchTo().frame("dialogwindow");
		
		//issue voucher
		try {
			driver.findElement(By.xpath(".//*[@id='issuevoucher']/a/img")).click();
		} catch (Exception e) {
			System.out.println("Voucher issue failed");
		}
		
		driver.switchTo().defaultContent();
		driver.switchTo().frame("reportIframe");
		try {
			wait.until(ExpectedConditions.presenceOfElementLocated(By.id("imgvgreen")));
		} catch (Exception e) {
			System.out.println("Issue green icon indicator missing");
		}
		//validate after issueing the voucher
		/*ReportPrinter.append("<br>");
		ReportPrinter.append("<span>After issuing the voucher</span>");*/
		if(driver.findElements(By.id("imgigreen")).size() > 0)
		{
			invoice = "issued";
		}
		else
		{
			invoice = "not issued";
		}
		
		if(driver.findElements(By.id("imgporange")).size() > 0)
		{
			payment = "not issued";
		}
		else
		{
			payment = "issued";
		}
		
		if(driver.findElements(By.id("imgvgreen")).size() > 0)
		{
			voucher = "issued";
		}
		else
		{
			voucher = "not issued";
		}
		
		ReportPrinter.append("<span><center><p class='Hedding0'>Notifications after issueing the voucher</p></center></span>");
		ReportPrinter.append("<table style=width:100%>"
		+ "<tr><th>Notification</th>"
		+ "<th>Status</th></tr>");
		if(invoice.equals("issued") && payment.equals("not issued") && voucher.equals("issued"))
		{
			ReportPrinter.append("<tr>");
			ReportPrinter.append("<td>Invoice</td>");
			ReportPrinter.append("<td class = 'Passed'>"+ invoice +"</td>");
			ReportPrinter.append("</tr>");
			
			ReportPrinter.append("<tr>");
			ReportPrinter.append("<td>Payment</td>");
			ReportPrinter.append("<td class = 'Passed'>"+ payment +"</td>");
			ReportPrinter.append("</tr>");
			
			ReportPrinter.append("<tr>");
			ReportPrinter.append("<td>Voucher</td>");
			ReportPrinter.append("<td class = 'Passed'>"+ voucher +"</td>");
			ReportPrinter.append("</tr>");
			
			/*ReportPrinter.append("<br>");
			ReportPrinter.append("<br>");
			ReportPrinter.append("<span class = 'Issued'>Invoice - "+ invoice +"</span>");
			ReportPrinter.append("<br>");
			ReportPrinter.append("<br>");
			ReportPrinter.append("<span class = 'NotIssued'>Payment - "+ payment +"</span>");
			ReportPrinter.append("<br>");
			ReportPrinter.append("<br>");
			ReportPrinter.append("<span class = 'Issued'>Voucher - "+ voucher +"</span>");
			ReportPrinter.append("<br>");*/
		}
		else if(!invoice.equals("issued"))
		{
			ReportPrinter.append("<tr>");
			ReportPrinter.append("<td>Invoice</td>");
			ReportPrinter.append("<td class = 'Failed'>"+ invoice +"</td>");
			ReportPrinter.append("</tr>");
			
			/*ReportPrinter.append("<br>");
			ReportPrinter.append("<br>");
			ReportPrinter.append("<span class = 'Failed'>Invoice - "+ invoice +"</span>");
			ReportPrinter.append("<br>");*/
		}
		else if(!payment.equals("not issued"))
		{
			ReportPrinter.append("<tr>");
			ReportPrinter.append("<td>Payment</td>");
			ReportPrinter.append("<td class = 'Failed'>"+ payment +"</td>");
			ReportPrinter.append("</tr>");
			
			/*ReportPrinter.append("<br>");
			ReportPrinter.append("<br>");
			ReportPrinter.append("<span class = 'Failed'>Payment - "+ payment +"</span>");
			ReportPrinter.append("<br>");*/
		}
		else if(!voucher.equals("issued"))
		{
			ReportPrinter.append("<tr>");
			ReportPrinter.append("<td>Voucher</td>");
			ReportPrinter.append("<td class = 'Failed'>"+ voucher +"</td>");
			ReportPrinter.append("</tr>");
			
			/*ReportPrinter.append("<br>");
			ReportPrinter.append("<br>");
			ReportPrinter.append("<span class = 'Failed'>Voucher - "+ voucher +"</span>");
			ReportPrinter.append("<br>");*/
		}
		
		ReportPrinter.append("</table>");
		ReportPrinter.append("<br><br><br>");
		
		try {
			driver.switchTo().defaultContent();
			driver.findElement(By.xpath(".//*[@id='topMenuItemsRight']/table/tbody/tr/td[1]")).click();
		} catch (Exception e) {
			
		}
	}

	public void pay(WebDriver driver, UIConfirmationPage webConfirmationPage, String url)
	{
		try {
			driver.get(url);
			driver.switchTo().defaultContent();
			driver.switchTo().frame("reportIframe");
			driver.findElement(By.id("reportCtegory_detail")).click();
			driver.findElement(By.id("searchby_reservationno")).click();
			driver.findElement(By.id("reservationno")).clear();
			driver.findElement(By.id("reservationno")).sendKeys(webConfirmationPage.getReservationNo());
			driver.findElement(By.id("reservationno_lkup")).click();
			driver.switchTo().defaultContent();
			driver.switchTo().frame("lookup");
			driver.findElement(By.xpath(".//*[@id='row-1']/td")).click();
			driver.switchTo().defaultContent();
			driver.switchTo().frame("reportIframe");
			driver.findElement(By.xpath(".//*[@id='topControlBar']/tbody/tr/td[3]/table/tbody/tr/td[2]/table/tbody/tr[2]/td[7]/a/img")).click();
			driver.findElement(By.xpath(".//*[@id='RowNo_1_2']/td[5]/a/img")).click();
			driver.switchTo().defaultContent();
			driver.switchTo().frame("reportIframe");
			driver.switchTo().frame("dialogwindow");
			driver.findElement(By.id("paidAmountArray[0]")).sendKeys(webConfirmationPage.getUisummarypay().getTotalCost());
			driver.findElement(By.id("saveButId")).click();
			driver.switchTo().defaultContent();
			driver.switchTo().frame("reportIframe");
			try {
				driver.findElement(By.xpath(".//*[@id='dialogMsgActionButtons']/a/img")).click();
			} catch (Exception e) {
				
			}
		} catch (Exception e) {
			
		}
	}
	
	public void bookingListReportToFinal(WebDriver driver, UIConfirmationPage webConfirmationPage, StringBuffer ReportPrinter/*, TO tourOperator, TO tourOperator2*/)
	{
		try {
			driver.findElement(By.xpath(".//*[@id='partnermenuitems']/table[4]/tbody/tr[14]/td[2]/a")).click();
			driver.switchTo().defaultContent();
			driver.switchTo().frame("reportIframe");
			driver.findElement(By.id("reservationno")).clear();
			driver.findElement(By.id("reservationno")).sendKeys(webConfirmationPage.getReservationNo());
			System.out.println(webConfirmationPage.getReservationNo());
			driver.findElement(By.id("reservationno_lkup")).click();
			driver.switchTo().defaultContent();
			driver.switchTo().frame("lookup");
			driver.findElement(By.className("rowstyle0")).click();
			driver.switchTo().defaultContent();
			driver.switchTo().frame("reportIframe");
		} catch (Exception e) {
			
		}
		
		WebDriverWait wait	 			= new WebDriverWait(driver, 5);
		
		try {
			wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath(".//*[@id='topControlBar']/tbody/tr/td[2]/table/tbody/tr/td[2]/table/tbody/tr[2]/td[3]/a/img")));
		} catch (Exception e) {
			
		}
		driver.findElement(By.xpath(".//*[@id='topControlBar']/tbody/tr/td[2]/table/tbody/tr/td[2]/table/tbody/tr[2]/td[3]/a/img")).click();
		String invoice = "";
		String payment = "";
		String voucher = "";
		if(driver.findElements(By.id("imgigreen")).size() > 0)
		{
			invoice = "issued";
		}
		else
		{
			invoice = "not issued";
		}
		
		if(driver.findElements(By.id("imgpgreen")).size() > 0)
		{
			payment = "issued";
		}
		else
		{
			payment = "not issued";
		}
		
		if(driver.findElements(By.id("imgvgreen")).size() > 0)
		{
			voucher = "issued";
		}
		else
		{
			voucher = "not issued";
		}
		
		
		ReportPrinter.append("<span><center><p class='Hedding0'>Notifications after payment voucher</p></center></span>");
		ReportPrinter.append("<table style=width:100%>"
		+ "<tr><th>Notification</th>"
		+ "<th>Status</th></tr>");
		if(invoice.equals("issued") && payment.equals("issued") && voucher.equals("issued"))
		{
			ReportPrinter.append("<tr>");
			ReportPrinter.append("<td>Invoice</td>");
			ReportPrinter.append("<td class = 'Passed'>"+ invoice +"</td>");
			ReportPrinter.append("</tr>");
			
			ReportPrinter.append("<tr>");
			ReportPrinter.append("<td>Payment</td>");
			ReportPrinter.append("<td class = 'Passed'>"+ payment +"</td>");
			ReportPrinter.append("</tr>");
			
			ReportPrinter.append("<tr>");
			ReportPrinter.append("<td>Voucher</td>");
			ReportPrinter.append("<td class = 'Passed'>"+ voucher +"</td>");
			ReportPrinter.append("</tr>");
			
			/*ReportPrinter.append("<br>");
			ReportPrinter.append("<span class = 'Issued'>Invoice - "+ invoice +"</span>");
			ReportPrinter.append("<br>");
			ReportPrinter.append("<br>");
			ReportPrinter.append("<span class = 'Issued'>Payment - "+ payment +"</span>");
			ReportPrinter.append("<br>");
			ReportPrinter.append("<br>");
			ReportPrinter.append("<span class = 'Issued'>Voucher - "+ voucher +"</span>");
			ReportPrinter.append("<br>");*/
		}
		else if(!invoice.equals("issued"))
		{
			ReportPrinter.append("<tr>");
			ReportPrinter.append("<td>Invoice</td>");
			ReportPrinter.append("<td class = 'Failed'>"+ invoice +"</td>");
			ReportPrinter.append("</tr>");
			
			/*ReportPrinter.append("<br>");
			ReportPrinter.append("<br>");
			ReportPrinter.append("<span class = 'Failed'>Invoice - "+ invoice +"</span>");
			ReportPrinter.append("<br>");*/
		}
		else if(!payment.equals("issued"))
		{
			ReportPrinter.append("<tr>");
			ReportPrinter.append("<td>Payment</td>");
			ReportPrinter.append("<td class = 'Failed'>"+ payment +"</td>");
			ReportPrinter.append("</tr>");
			
			/*ReportPrinter.append("<br>");
			ReportPrinter.append("<br>");
			ReportPrinter.append("<span class = 'Failed'>Payment - "+ payment +"</span>");
			ReportPrinter.append("<br>");*/
		}
		else if(!voucher.equals("issued"))
		{
			ReportPrinter.append("<tr>");
			ReportPrinter.append("<td>Voucher</td>");
			ReportPrinter.append("<td class = 'Failed'>"+ voucher +"</td>");
			ReportPrinter.append("</tr>");
			
			/*ReportPrinter.append("<br>");
			ReportPrinter.append("<br>");
			ReportPrinter.append("<span class = 'Failed'>Voucher - "+ voucher +"</span>");
			ReportPrinter.append("<br>");*/
		}
		
		ReportPrinter.append("</table>");
		ReportPrinter.append("<br><br><br>");
		
		try {
			driver.switchTo().defaultContent();
			driver.findElement(By.xpath(".//*[@id='topMenuItemsRight']/table/tbody/tr/td[1]")).click();
		} catch (Exception e) {
			
		}
		
		System.out.println("to done");
	}

}
