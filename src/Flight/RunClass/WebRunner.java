/*Sanoj*/
package Flight.RunClass;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.concurrent.TimeUnit;

import org.apache.commons.io.FileUtils;
import org.openqa.selenium.Alert;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebDriverException;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;

import com.utilities.SupportMethods;

import Flight.Classes.*;


public class WebRunner
{
	String								PropfilePath	= "";
	String								XLPath			= "";
	ArrayList<Map<Integer, String>>		WebXLtestData	= null;
	HashMap<String, String>				Propertymap		= new HashMap<String, String>();
	ArrayList<Flight>					WebflightList	= null;
	ArrayList<SearchObject>				WebSearchList	= null;
	WebDriver							driver			= null;
	String								tracer			= "";
	SupportMethods						SUP				= null;
	boolean								twoway			= false;
	
	
	public WebRunner(HashMap<String, String> Propymap)
	{
		Propertymap = Propymap;
		SUP = new SupportMethods(Propertymap);
	}

	public WebDriver WebRUN(WebDriver driver, SearchObject searchObject)
	{
		try
		{
			//driver.get("http://rezdemo2.rezgateway.com/rezpackageReservations/");
			//driver.get("http://qav3.rezgateway.com/rezbase_v3Reservations/");
			//driver.get("http://dev3.rezg.net/rezbase_v3Reservations/");
			driver.get(Propertymap.get("Portal.Url").concat("Reservations/Welcome.do?method=welcome&template=template4"));
			
			Thread.sleep(10000);
			try {
				Alert alert = driver.switchTo().alert();
				alert.accept();
			} catch (Exception e) {
				e.printStackTrace();
			}
			//WebDriverWait wait = new WebDriverWait(driver, 10,5);
			Thread.sleep(5000);
			//driver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);
			//System.out.println();
			driver.switchTo().defaultContent();
			//driver.switchTo().frame("live_message_frame");
			//new Select(driver.findElement(By.id("select_currency"))).selectByVisibleText(searchObject.getSellingCurrency().trim());
			
			driver.switchTo().frame("bec_container_frame");
			
			driver.findElement(By.id("flights")).click();
			
			new Select(driver.findElement(By.id("F_Country"))).selectByVisibleText(searchObject.getCountry().trim());		
			
			if(searchObject.getTriptype().equals("Round Trip"))
			{
				driver.findElement(By.id("Air_TripType2")).click();
				((JavascriptExecutor)driver).executeScript("$('#air_departure').val('"+searchObject.getDepartureDate()+"');");
				((JavascriptExecutor)driver).executeScript("$('#air_arrival').val('"+searchObject.getReturnDate()+"');");
				new Select(driver.findElement(By.id("Air_DepTime_a"))).selectByVisibleText(searchObject.getDepartureTime().trim());
				new Select(driver.findElement(By.id("Air_RetTime_a"))).selectByVisibleText(searchObject.getReturnTime().trim());
			}
			if(searchObject.getTriptype().equals("One Way Trip"))
			{
				driver.findElement(By.id("Air_TripType1")).click();
				((JavascriptExecutor)driver).executeScript("$('#air_departure').val('"+searchObject.getDepartureDate()+"');");
				new Select(driver.findElement(By.id("Air_DepTime_a"))).selectByVisibleText(searchObject.getDepartureTime().trim());
			}
			if(searchObject.getTriptype().equals("Multi Destination"))
			{
				driver.findElement(By.id("Air_TripType3")).click();
			}
			
			driver.findElement(By.id("air_Loc_a")).sendKeys("Sri Lanka,  Colombo"/*searchObject.getFrom().trim().split(" ")[0]*/);//Type to avoid the location value missing error
			((JavascriptExecutor)driver).executeScript("document.getElementById('hid_air_Loc_a').setAttribute('value','"+searchObject.getFrom()+"');");//Setting the hidden value
			driver.findElement(By.id("air_Loc1_a")).sendKeys("Sri Lanka,  Colombo"/*searchObject.getTo().trim().split(" ")[0]*/);//Type to avoid the location value missing error
			((JavascriptExecutor)driver).executeScript("document.getElementById('hid_air_Loc1_a').setAttribute('value','"+searchObject.getTo()+"');");//Setting the hidden value
			
			
			if(searchObject.isFlexible())
			{
				driver.findElement(By.id("isFlexSearch_F")).click();
			}
			
			/*------------------Select Adult / Child--------------------*/
			new Select(driver.findElement(By.id("R1occAdults_F"))).selectByValue(searchObject.getAdult());//select number of adults
			//System.out.println("----------------------------------");
			//System.out.println(searchObject.getChildren().trim());
			//System.out.println("----------------------------------");
			if(!searchObject.getChildren().trim().equals("0") /*|| !searchObject.getChildren().trim().equals("") || !searchObject.getChildren().trim().equals(null)*/ )//Count no of children from excel
			{
				new Select(driver.findElement(By.id("R1occChildren_F"))).selectByValue(searchObject.getChildren().trim());
				
				for(int i = 1; i<=searchObject.getChildrenAge().size(); i++)
				{
					new Select(driver.findElement(By.xpath(".//*[@id='F_R1childage_"+i+"']"))).selectByValue(searchObject.getChildrenAge().get((i-1)).trim());//Child Loop
				}
			}
	
			if(!searchObject.getInfant().trim().equals("0"))
			{
				new Select(driver.findElement(By.id("R1occInfant_F"))).selectByValue(searchObject.getInfant().trim());
			}
			
			
			driver.findElement(By.xpath(".//*[@id='airDisplay']/div[2]/div[1]/div/a")).click();
			
			new Select(driver.findElement(By.id("Aira_FlightClass"))).selectByValue(searchObject.getCabinClass().trim());
			
			if(!searchObject.getPreferredCurrency().trim().equals("Select Currency"))
			{
				new Select(driver.findElement(By.id("F_consumerCurrencyCode"))).selectByValue(searchObject.getPreferredCurrency().trim());
			}
			
			if(!searchObject.getPreferredAirline().trim().equals("NONE"))
			{
				driver.findElement(By.id("Air_line")).sendKeys(searchObject.getPreferredAirline().trim());
			}
			
			if(searchObject.isNonStop())
			{
				driver.findElement(By.xpath(".//*[@id='pref_nonstop_F']/div/input")).click();
			}
	
			driver.findElement(By.id("search_btns_f")).click();
	
		}
		catch(Exception e)
		{
			e.printStackTrace();
		}
		
	return driver;

	}

	public ArrayList<UIFlightItinerary> getResults(WebDriver driver, SearchObject searchObject)
	{
		ArrayList<UIFlightItinerary> Resultlist = new ArrayList<UIFlightItinerary>(); 

		int nflights;
		driver.switchTo().defaultContent();
		String numofflights = driver.findElement(By.id("crt_count_WJ_12")).getText();
		//System.out.println(numofflights);
		String[] hotels = numofflights.split(" ");
		
		nflights = Integer.parseInt(hotels[0]);//Get no of flights
		//System.out.println("No of flights:"+nflights);
		int navcount1 = nflights/10;//Per page 10 find how many pages to navigate
		//System.out.println("pages :"+navcount1);
		int navcount2 = nflights%10;//Remaining flights
		//System.out.println("remainder :"+navcount2);
		
		int navigate = navcount1;
		
		if((navcount2>0)&&(navcount2<10))
		{
			navigate+=1;
			
		}
		int clickpattern = 2; 
		//System.out.println("NAVIGATE  :  "+navigate);
		int count = 0;
		
		
		ArrayList<WebElement> flights = null;
		//ArrayList<WebElement> allflights = new ArrayList<WebElement>();
		
		if(Integer.parseInt(Propertymap.get("Flight_PageIterations")) <= navigate)
		navigate  = Integer.parseInt(Propertymap.get("Flight_PageIterations"));
		
		
		while(count<navigate)
		{
			try
			{	
				flights = new ArrayList<WebElement>(driver.findElements(By.className("result-container-flights")));
				
				Iterator<WebElement> flightsIter = flights.iterator();
				int nt = 1;
				whileloop:while(flightsIter.hasNext())
				{
					if(nt > Integer.parseInt(Propertymap.get("Flight_Iterations")))
					break whileloop;
					
					//System.out.println("<<<<<<<<<<<<<<<<<<<<FLIGHT ITINERARY "+nt+" STARTED>>>>>>>>>>>>>>>>>>>>");
					WebElement flight = flightsIter.next();
					UIFlightItinerary flightItinearary = new UIFlightItinerary();
					WebElement infoleft = flight.findElement(By.id("KB_result-container-in-flights"));
					WebElement inforight= flight.findElements(By.className("result-container-in-r")).get(0);
		
					ArrayList<WebElement> boundflight = (ArrayList<WebElement>) infoleft.findElements(By.className("KB_result-container-in-flights-in"));
				    WebElement OutBoundFlightinfo  =  boundflight.get(0);
				    try
				    {
				    	ArrayList<WebElement> Note = (ArrayList<WebElement>) infoleft.findElements(By.className("flight_next_day"));
				    	flightItinearary.setNote(Note);
				    }
				    catch(Exception e)
				    {
				    	e.printStackTrace();
				    }

				    ArrayList<WebElement>  Leavelist1 = new ArrayList<WebElement>(OutBoundFlightinfo.findElements(By.className("KB_dt_text")));
				    ArrayList<WebElement>  Leavelist2 = new ArrayList<WebElement>(OutBoundFlightinfo.findElements(By.className("KB_ftext")));
				    
				    Leavelist1.addAll(Leavelist2);
				    ArrayList<String>  FinalLeaveinfo = new ArrayList<String>();
				    for (Iterator<WebElement> iterator = Leavelist1.iterator(); iterator.hasNext();) 
				    {
						WebElement webElement = (WebElement) iterator.next();
                        if(webElement.getText().isEmpty())
                        {
                        	continue;
                        }
                        else
                        {
                        	FinalLeaveinfo.add(webElement.getText());
                        }
                        	
					}
				    //System.out.println(FinalLeaveinfo);
				    
				    flightItinearary.setDeparting(FinalLeaveinfo.get(0));
				    flightItinearary.setLeaving(FinalLeaveinfo.get(0));
				    flightItinearary.setLeaveDepartTime(FinalLeaveinfo.get(1));
				    flightItinearary.setLeaveArriveTime(FinalLeaveinfo.get(2));
				    flightItinearary.setLeaveduration(FinalLeaveinfo.get(3));
				    flightItinearary.setDirectFlightLeave(FinalLeaveinfo.get(4));
				    flightItinearary.setLeaveFlight(FinalLeaveinfo.get(5));
				    String LeaveFrom = FinalLeaveinfo.get(7);
				    LeaveFrom = LeaveFrom.substring(1, LeaveFrom.length() - 1);
				    //System.out.println(LeaveFrom);
				    flightItinearary.setLeaveDepartFrom(LeaveFrom);
				    String LeaveTo = FinalLeaveinfo.get(9);
				    LeaveTo = LeaveTo.substring(1, LeaveTo.length() - 1);
				    //System.out.println(LeaveTo);
				    flightItinearary.setLeaveArriveTo(LeaveTo);
				    
				    
				    if(searchObject.getTriptype().equals("Round Trip"))
				    {
				    	twoway = true;
				    }
				    
				    if(twoway)
				    {
				    	WebElement InBoundFlightinfo =  boundflight.get(1);
					
						ArrayList<WebElement>  Returnlist1 = new ArrayList<WebElement>(InBoundFlightinfo.findElements(By.className("KB_dt_text")));
					    ArrayList<WebElement>  Returnlist2 = new ArrayList<WebElement>(InBoundFlightinfo.findElements(By.className("KB_ftext")));
						
					    Returnlist1.addAll(Returnlist2);
					    ArrayList<String>  FinalReturninfo = new ArrayList<String>();
					    for (Iterator<WebElement> iterator = Returnlist1.iterator(); iterator.hasNext();) 
					    {
							WebElement webElement = (WebElement) iterator.next();
							if(webElement.getText().isEmpty())
	                        {
	                        	continue;
	                        }
	                        else
	                        {
	                        	FinalReturninfo.add(webElement.getText());
	                        }						
						}
					    //System.out.println(FinalReturninfo);
					    flightItinearary.setReturning(FinalReturninfo.get(0));
					    flightItinearary.setReturnDepartTime(FinalReturninfo.get(1));
					    flightItinearary.setReturnArrivalTime(FinalReturninfo.get(2));
					    flightItinearary.setReturnduration(FinalReturninfo.get(3));
					    flightItinearary.setReturnDirectFlight(FinalReturninfo.get(4));
					    flightItinearary.setReturnFlight(FinalReturninfo.get(5));
					    String ReturnTo = FinalReturninfo.get(7);
					    ReturnTo = ReturnTo.substring(1, ReturnTo.length() - 1);
					    //System.out.println(ReturnTo);
					    flightItinearary.setReturnDepartFrom(ReturnTo);
					    String ReturnFrom = FinalReturninfo.get(9);
					    ReturnFrom = ReturnFrom.substring(1, ReturnFrom.length() - 1);
					    //System.out.println(ReturnFrom);
					    flightItinearary.setReturnArrivalTo(ReturnFrom);
					    
					    
						WebElement price = inforight.findElement(By.className("hprice-price"));
						flightItinearary.setCurrencyCode(price.findElement(By.className("hotel_currency")).getText());
						flightItinearary.setCost(price.getText().split(" ")[1].trim());
						
						WebElement cabinclass =  inforight.findElement(By.className("KB_economy-class-p"));
						flightItinearary.setCabintype(cabinclass.getText().trim().split(" ")[0].trim());
				    }

					/*-----------------MORE INFORMATION INBOUND AND OUTBOUND------------------*/
					inforight.findElement(By.cssSelector("html body div#wraper div#container div#content div#content-m.box-shadow div div#flight_dv_WJ_1.margintop10 div.result-container-flights div.result-container-in-r.price_button_container_main div.KB_flight_details.fright.add_btn.padd_btn.desktop_show a.KB_flight_details_p.desktop_show")).click();
				//	driver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);
					
					WebDriverWait wait = new WebDriverWait(driver, 60);
					try {
						wait.until(ExpectedConditions.presenceOfElementLocated(By.id("backgroundPopup")));
						Thread.sleep(2000);
					} catch (Exception e) {
						//System.out.println("Popup not Availableeeeeeeeeee");
						e.printStackTrace();
					}
					
					if(driver.findElement(By.id("backgroundPopup")).isDisplayed())
					{
						driver.manage().timeouts().implicitlyWait(3, TimeUnit.SECONDS);
						//System.out.println("Background popup displayed.............");
						WebElement InFlights = null;
						ArrayList<WebElement> inboundflight = null;
						WebElement OutFlights = driver.findElement(By.className("out_bound"));
						ArrayList<WebElement> outboundflight = new ArrayList<WebElement>(OutFlights.findElements(By.className("lineheight25")));
						
						if(twoway)
						{
							InFlights = driver.findElement(By.className("in_bound"));
							inboundflight = new ArrayList<WebElement>(InFlights.findElements(By.className("lineheight25")));
						}
					
						/*OUTBOUND FLIGHT LOOP*/
						Outbound Outobj = new Outbound();
						ArrayList<Flight> OBflightlist = new ArrayList<Flight>();
						//System.out.println("OUTBOUND FLIGHT SIZE : "+outboundflight.size());
						for(int i = 0; i<outboundflight.size(); i++)
						{
							Flight obj = new Flight();
							try
							{
								//System.out.println("OUTBOUND FLIGHT : "+i+"..........started");
								driver.manage().timeouts().implicitlyWait(3, TimeUnit.SECONDS);
								
								obj.setDepartureDate(outboundflight.get(i).findElement(By.className("flight_info_cl_01")).getText());
								obj.setArrivalDate(outboundflight.get(i).findElement(By.className("flight_info_cl_01")).getText());
								
								String portid = outboundflight.get(i).findElement(By.className("flight_info_cl_02")).getText();
								//System.out.println("PortID : "+portid);
								String depart = portid.split("\\n")[0];
								obj.setDeparture_port(depart.split(" ")[0]);
								
								String arrive = portid.split("\\n")[1];
								obj.setArrival_port(arrive.split(" ")[0]);
								
								String Departportid = portid.split("\\n")[0].split("[()]")[1];
								//System.out.println(Departportid);
								obj.setDepartureLocationCode(Departportid);
								
								String Arriveportid = portid.split("\\n")[1].split("[()]")[1];
								//System.out.println(Arriveportid);
								obj.setArrivalLocationCode(Arriveportid);
								
								obj.setDepartureTime(outboundflight.get(i).findElement(By.className("flight_info_cl_03")).getText().split("\\n")[0]);
								obj.setArrivalTime(outboundflight.get(i).findElement(By.className("flight_info_cl_03")).getText().split("\\n")[1]);							
								
								int size = outboundflight.get(i).findElement(By.className("flight_info_cl_05")).getText().split(" ").length;
								obj.setFlightNo(outboundflight.get(i).findElement(By.className("flight_info_cl_05")).getText().split(" ")[size-1]);
								obj.setMarketingAirline(outboundflight.get(i).findElement(By.className("flight_info_cl_05")).getText().replace(obj.getFlightNo(), ""));
								
							}
							catch(Exception e)
							{
								e.printStackTrace();
								TakesScreenshot screen = (TakesScreenshot)driver;
								FileUtils.copyFile(screen.getScreenshotAs(OutputType.FILE),new File("FailedScreenshots/Outbound details"+System.currentTimeMillis()/1000+".jpg"));
							}
							
							OBflightlist.add(obj);
							//System.out.println("OUTBOUND FLIGHT : "+i+"..........ended");
						}
						
						Outobj.setOutBflightlist(OBflightlist);
						flightItinearary.setOutbound(Outobj);
						
						
						if(twoway)
						{
							/*INBOUND FLIGHT LOOP*/
							Inbound Inobj = new Inbound();
							ArrayList<Flight> IBflightlist = new ArrayList<Flight>();
							//System.out.println("INBOUND FLIGHT SIZE : "+inboundflight.size());
							for(int i = 0; i<inboundflight.size(); i++)
							{
								Flight obj = new Flight();
								try
								{
									//System.out.println("INBOUND FLIGHT : "+i+"..........started");
									driver.manage().timeouts().implicitlyWait(3, TimeUnit.SECONDS);
									
									obj.setDepartureDate(inboundflight.get(i).findElement(By.className("flight_info_cl_01")).getText());
									obj.setArrivalDate(inboundflight.get(i).findElement(By.className("flight_info_cl_01")).getText());
									
									String portid =  inboundflight.get(i).findElement(By.className("flight_info_cl_02")).getText();
									//System.out.println("PortID : "+portid);
									String depart = portid.split("\\n")[0];
									obj.setDeparture_port(depart.split(" ")[0]);
									
									String arrive = portid.split("\\n")[1];
									obj.setArrival_port(arrive.split(" ")[0]);
									
									String Departportid = portid.split("\\n")[0].split("[()]")[1];
									//System.out.println(Departportid);
									obj.setDepartureLocationCode(Departportid);
									
									String Arriveportid = portid.split("\\n")[1].split("[()]")[1];
									//System.out.println(Arriveportid);
									obj.setArrivalLocationCode(Arriveportid);
									
									//System.out.println(obj.getDepartureLocationCode());
									//System.out.println(obj.getArrivalLocationCode());
									
									//System.out.println(inboundflight.get(i).findElement(By.cssSelector("html body div.popup_container div#more_info_flights_WJ_21.modal_window.popup-box div.popup_content div#oneway_return div.flightinfo.paddingbot5 div.in_bound div.lineheight25.margin10 div.flight_info_cl_03.fleft")).getText());
									obj.setDepartureTime(inboundflight.get(i).findElement(By.className("flight_info_cl_03")).getText().split("\\n")[0]);
									obj.setArrivalTime(inboundflight.get(i).findElement(By.className("flight_info_cl_03")).getText().split("\\n")[1]);							
									
									int size = inboundflight.get(i).findElement(By.className("flight_info_cl_05")).getText().split(" ").length;
									
									obj.setFlightNo(inboundflight.get(i).findElement(By.className("flight_info_cl_05")).getText().split(" ")[size-1]);
									obj.setMarketingAirline(inboundflight.get(i).findElement(By.className("flight_info_cl_05")).getText().replace(obj.getFlightNo(), ""));
									
									
								}
								catch(Exception e)
								{
									e.printStackTrace();
									TakesScreenshot screen = (TakesScreenshot)driver;
									FileUtils.copyFile(screen.getScreenshotAs(OutputType.FILE),new File("FailedScreenshots/Inbound Details"+System.currentTimeMillis()/1000+".jpg"));
								}
								
								IBflightlist.add(obj);
								//System.out.println("INBOUND FLIGHT : "+i+"..........ended");
							}
							
							Inobj.setInBflightlist(IBflightlist);
							flightItinearary.setInbound(Inobj);
						}
						
						
						//System.out.println("Done"); 
						
					}
					driver.findElement(By.cssSelector("html body div.popup_container div#more_info_flights_WJ_21.modal_window.popup-box div.close.pop-up-close-button")).click();

					Resultlist.add(flightItinearary);
					//System.out.println("<<<<<<<<<<<<<<<<<<<<FLIGHT ITINERARY ENDED>>>>>>>>>>>>>>>>>>>>");
					nt++;
					//System.out.println();
				}
				
			}	
			catch(Exception e)
			{
				//System.out.println();
				e.printStackTrace();
				
			}

			((JavascriptExecutor)driver).executeScript("doIWC(new IWCParams('PAGING','RESULTS','PAGE','', {'pageNo':"+clickpattern+"},'','WJ_2'))");
			clickpattern++;		
			count++; 

		}
		
		clickpattern = 1;
		((JavascriptExecutor)driver).executeScript("doIWC(new IWCParams('PAGING','RESULTS','PAGE','', {'pageNo':"+clickpattern+"},'','WJ_2'))");

		return Resultlist;
	}
	
	public CartFlight getWEBCART(WebDriver driver)
	{
		CartFlight cartFlight = new CartFlight();
		
		String basket = driver.findElement(By.cssSelector("html body div#wraper div#container div#content div#content-l div#content-l-b1 div#cart_display_WJ_7 div#popup_container div#b1-items p.b1-items")).getText();
		System.out.println(basket);
		String basketpriceCurrCode = driver.findElement(By.cssSelector("html body div#wraper div#container div#content div#content-l div#content-l-b1 div#cart_display_WJ_7 div#popup_container div#b1-items div.basket-price")).getText().split(" ")[0];
		String basketprice = driver.findElement(By.cssSelector("html body div#wraper div#container div#content div#content-l div#content-l-b1 div#cart_display_WJ_7 div#popup_container div#b1-items div.basket-price")).getText().split(" ")[1];
		String[] details = basket.split("\\n");
		String to = "";
		ArrayList<String> det = new ArrayList<String>(Arrays.asList(details));

		System.out.println(det);
		for(int y=0; y<details.length; y++)
		{
			if(details[y].contains("To"))
			{
				to = det.get(y);
				det.remove(det.get(y));
				break;
			}
		}
		//System.out.println("ARRAY LENGTH : "+details.length);
		
		cartFlight.setLeaveAirport(det.get(1));
		
		cartFlight.setReturnAirport(det.get(2));
		
		cartFlight.setLeaveDate(det.get(3).split(" ")[2]);
		cartFlight.setLeaveTime(det.get(3).split(" ")[3]);
		
		cartFlight.setReturnDate(to.split(" ")[2]);
		cartFlight.setReturnTime(to.split(" ")[3]);
		det.remove(cartFlight.getLeaveAirport());det.remove(cartFlight.getReturnAirport());det.remove(details[0]);det.remove(details[3]);
		//System.out.println(det);
		
		cartFlight.setCurrencyCode(basketpriceCurrCode);
		cartFlight.setTotal(basketprice);
		driver.findElement(By.id("load_modal_window_breakup")).click();
		
		WebElement ele = driver.findElement(By.id("summary_breakup"));
		ArrayList<WebElement> elelist = (ArrayList<WebElement>) ele.findElements(By.className("breakup_row"));
		//System.out.println(elelist.get(0).findElement(By.className("breakup_value")).getText());
		cartFlight.setSubtotal1(elelist.get(0).findElement(By.className("breakup_value")).getText());
		cartFlight.setTaxesandOther1(elelist.get(1).findElement(By.className("breakup_value")).getText());
		cartFlight.setTotalforflight1(elelist.get(2).findElement(By.className("breakup_value")).getText());
		cartFlight.setSubtotalFinal(elelist.get(3).findElement(By.className("breakup_sub_value")).getText());
		cartFlight.setTaxesandOtherFinal(elelist.get(4).findElement(By.className("breakup_sub_value")).getText());
		cartFlight.setAmountprocessnowFinal(elelist.get(5).findElement(By.className("breakup_sub_value")).getText());
		cartFlight.setAmountbyairlineFinal(elelist.get(6).findElement(By.className("breakup_sub_value")).getText());
		cartFlight.setFinalvalue(elelist.get(7).findElement(By.className("breakup_tot_value")).getText());
		cartFlight.setFlights(det);
		
		 ((JavascriptExecutor)driver).executeScript("disableT4popuo();");
		//driver.findElement(By.xpath("/html/body/div[9]/div[1]/div[2]/div[1]/div[1]/div[2]/div/div[1]/div[1]")).click();
		//driver.findElement(By.xpath("/html/body/div[6]/div/div[1]")).click();
		return cartFlight;
	}
		
	public WebDriver Fill_Reservation_Details(WebDriver driver, ReservationInfo fillObj) throws WebDriverException, IOException
	{
		int adults = fillObj.getAdult().size();
		int children = fillObj.getChildren().size();
		int infants = fillObj.getInfant().size();
		try
		{
			Traveler main = fillObj.getMaincustomer();
			driver.findElement(By.id("cusFName")).clear();
			driver.findElement(By.id("cusFName")).sendKeys(main.getGivenName());
			driver.findElement(By.id("cusLName")).clear();
			driver.findElement(By.id("cusLName")).sendKeys(main.getSurname());
			driver.findElement(By.id("cusAdd1")).clear();
			driver.findElement(By.id("cusAdd1")).sendKeys(main.getAddress().getAddressStreetNo());
			driver.findElement(By.id("cusAdd2")).clear();
			driver.findElement(By.id("cusAdd2")).sendKeys(main.getAddress().getAddressStreetNo()+"123");
			driver.findElement(By.id("cusCity")).clear();
			driver.findElement(By.id("cusCity")).sendKeys(main.getAddress().getAddressCity());
			new Select(driver.findElement(By.id("cusCountry"))).selectByVisibleText(main.getAddress().getAddressCountry());
			driver.findElement(By.id("cusareacodetext")).clear();
			driver.findElement(By.id("cusareacodetext")).sendKeys("11");
			driver.findElement(By.id("cusPhonetext")).clear();
			driver.findElement(By.id("cusPhonetext")).sendKeys(main.getPhoneNumber());
			driver.findElement(By.id("cusmobnos")).clear();
			driver.findElement(By.id("cusmobnos")).sendKeys("88");
			driver.findElement(By.id("cusmobnoe")).clear();
			driver.findElement(By.id("cusmobnoe")).sendKeys(main.getPhoneNumber());
			driver.findElement(By.id("cusEmail")).clear();
			driver.findElement(By.id("cusEmail")).sendKeys(main.getEmail());
			driver.findElement(By.id("cusConfEmail")).sendKeys(main.getEmail());
			
			for(int a = 0; a<adults; a++)
			{
				new Select(driver.findElement(By.name("adultTitle_Air["+a+"]"))).selectByVisibleText(fillObj.getAdult().get(a).getNamePrefixTitle());
				driver.findElement(By.id("adultFNm_Air["+a+"]")).clear();
				driver.findElement(By.id("adultFNm_Air["+a+"]")).sendKeys(fillObj.getAdult().get(a).getGivenName());
				driver.findElement(By.id("adultLNm_Air["+a+"]")).clear();
				driver.findElement(By.id("adultLNm_Air["+a+"]")).sendKeys(fillObj.getAdult().get(a).getSurname());
				driver.findElement(By.id("adultContactNo_Air["+a+"]")).clear();
				driver.findElement(By.id("adultContactNo_Air["+a+"]")).sendKeys(fillObj.getAdult().get(a).getPhoneNumber());
				driver.findElement(By.id("adultPassportNo_Air["+a+"]")).clear();
				driver.findElement(By.id("adultPassportNo_Air["+a+"]")).sendKeys(fillObj.getAdult().get(a).getPassportNo());

				((JavascriptExecutor)driver).executeScript("$('#adultPassportExpDate_Air_"+a+"').val('"+fillObj.getAdult().get(a).getPassportExpDate().trim()+"');");
				
				((JavascriptExecutor)driver).executeScript("$('#adultPassportIssueDate_Air_"+a+"').val('"+fillObj.getAdult().get(a).getPassprtIssuDate().trim()+"');");
				
				((JavascriptExecutor)driver).executeScript("$('#adultDateOfBirth_Air_"+a+"').val('"+fillObj.getAdult().get(a).getBirthDay().trim()+"');");
				
				new Select(driver.findElement(By.name("adultGender["+a+"]"))).selectByVisibleText(fillObj.getAdult().get(a).getGender());
				new Select(driver.findElement(By.name("adultNationality["+a+"]"))).selectByVisibleText(fillObj.getAdult().get(a).getNationality());
				new Select(driver.findElement(By.name("adultPassportIssueCountry["+a+"]"))).selectByVisibleText(fillObj.getAdult().get(a).getPassportIssuCountry());
			}
			
			for(int c=0; c<children; c++)
			{
				new Select(driver.findElement(By.name("childTitle_Air["+c+"]"))).selectByVisibleText(fillObj.getChildren().get(c).getNamePrefixTitle());
				driver.findElement(By.id("childFNm_Air["+c+"]")).clear();
				driver.findElement(By.id("childFNm_Air["+c+"]")).sendKeys(fillObj.getChildren().get(c).getGivenName());
				driver.findElement(By.id("childLNm_Air["+c+"]")).clear();
				driver.findElement(By.id("childLNm_Air["+c+"]")).sendKeys(fillObj.getChildren().get(c).getSurname());
				driver.findElement(By.id("childContactNo_Air["+c+"]")).clear();
				driver.findElement(By.id("childContactNo_Air["+c+"]")).sendKeys(fillObj.getChildren().get(c).getPhoneNumber());
				driver.findElement(By.id("childPassportNo_Air["+c+"]")).clear();
				driver.findElement(By.id("childPassportNo_Air["+c+"]")).sendKeys(fillObj.getChildren().get(c).getPassportNo());
				((JavascriptExecutor)driver).executeScript("$('#childPassportExpDate_Air_"+c+"').val('"+fillObj.getChildren().get(c).getPassportExpDate().trim()+"');");
				
				((JavascriptExecutor)driver).executeScript("$('#childPassportIssueDate_Air_"+c+"').val('"+fillObj.getChildren().get(c).getPassprtIssuDate().trim()+"');");
				
				((JavascriptExecutor)driver).executeScript("$('#childDateOfBirth_Air_"+c+"').val('"+fillObj.getChildren().get(c).getBirthDay().trim()+"');");
				
				new Select(driver.findElement(By.name("childGender["+c+"]"))).selectByVisibleText(fillObj.getChildren().get(c).getGender());
				new Select(driver.findElement(By.name("childNationality["+c+"]"))).selectByVisibleText(fillObj.getChildren().get(c).getNationality());
				new Select(driver.findElement(By.name("childPassportIssueCountry["+c+"]"))).selectByVisibleText(fillObj.getChildren().get(c).getPassportIssuCountry());
			}
			
			for(int i = 0; i<infants; i++)
			{
				new Select(driver.findElement(By.name("infantTitle_Air["+i+"]"))).selectByVisibleText(fillObj.getInfant().get(i).getNamePrefixTitle());
				driver.findElement(By.id("infantFNm_Air["+i+"]")).clear();
				driver.findElement(By.id("infantFNm_Air["+i+"]")).sendKeys(fillObj.getInfant().get(i).getGivenName());
				driver.findElement(By.id("infantLNm_Air["+i+"]")).clear();
				driver.findElement(By.id("infantLNm_Air["+i+"]")).sendKeys(fillObj.getInfant().get(i).getSurname());
				driver.findElement(By.id("tfpi_infant_passport_number_"+i+"")).sendKeys(fillObj.getInfant().get(i).getPassportNo());                                                

				((JavascriptExecutor)driver).executeScript("$('#infantBirthDate_Air_"+i+"').val('"+fillObj.getInfant().get(i).getBirthDay().trim()+"');");
			
				new Select(driver.findElement(By.name("infantGender["+i+"]"))).selectByVisibleText(fillObj.getInfant().get(i).getGender());
				new Select(driver.findElement(By.name("infantNationality["+i+"]"))).selectByVisibleText(fillObj.getInfant().get(i).getNationality());
			}
			
			driver.findElement(By.id("confreadPolicy")).click();
			driver.findElement(By.id("confTnC")).click();
			
		}
		catch(Exception e)
		{
			e.printStackTrace();
			TakesScreenshot screen = (TakesScreenshot)driver;
			FileUtils.copyFile(screen.getScreenshotAs(OutputType.FILE),new File("FailedScreenshots/Filling Reservation Details"+System.currentTimeMillis()/1000+".jpg"));
		}	
		
		return driver;
	}
	
	public UIPaymentPage getPaymentPage(WebDriver driver, XMLPriceItinerary XMLSelectedFlight) throws WebDriverException, IOException
	{
		boolean twoway = false;
		if(XMLSelectedFlight.getDirectiontype().equals("Circle"))
		{
			twoway = true;
		}
		UIPaymentPage uipaymentpage = new UIPaymentPage();
		try
		{
			WebElement flightdetails = driver.findElement(By.className("booking-hotl-dec"));
			WebElement paymentdetails = driver.findElement(By.cssSelector("html body form#ResPkgBookingDetailsForm div#wraper div#container div#content div#content-booking.box-shadow div#tripsummary div.result-container-booking div.payment_trip_summery_flightdetail_box.fleft.paddingall15"));
			
			String[] flightdetail = flightdetails.getText().split("\\n");
			ArrayList<String> list = new ArrayList<String>();
			for (int i=0; i<flightdetail.length; i++) 
			{
				list.add(flightdetail[i]);
			}
			
			uipaymentpage.setDeparture(list.get(0).split("to")[0]);
			uipaymentpage.setDestination(list.get(0).split("to")[1]);
			uipaymentpage.setFromDate(list.get(1).split("To")[0]);
			uipaymentpage.setToDate(list.get(1).split("To")[1]);
			list.remove(0);
			list.remove(0);
			list.remove(list.size()-1);
			list.removeAll(Collections.singleton(null)); 
			//System.out.println(list.size());
			ArrayList<RatesperPassenger> ratelist = new ArrayList<RatesperPassenger>();
			if(list.size()%4 == 0)
			{
				int u = list.size()/4;
				for(int y=0; y<u; y++)
				{
					RatesperPassenger rate = new RatesperPassenger();
					rate.setPassengertype(list.get(0).split("-")[1].trim());
					rate.setRateperpsngr(list.get(1).split("-")[1].trim());
					rate.setNoofpassengers(list.get(2).split("-")[1].trim());
					rate.setTotal(list.get(3).split("-")[1].trim());
					list.remove(0);
					list.remove(0);
					list.remove(0);
					list.remove(0);
					list.removeAll(Collections.singleton(null));
					ratelist.add(rate);
				}
				//System.out.println(ratelist);
				uipaymentpage.setRatelist(ratelist);
			}
			
			ArrayList<WebElement> outbounddepart = new ArrayList<WebElement>(paymentdetails.findElements(By.cssSelector("html body form#ResPkgBookingDetailsForm div#wraper div#container div#content div#content-booking.box-shadow div#tripsummary div.result-container-booking div.payment_trip_summery_flightdetail_box.fleft.paddingall15 div.clearf.paddingtop5.paddingbot5.flight_outbound_depart_mobile")));
			ArrayList<WebElement> outboundarrive = new ArrayList<WebElement>(paymentdetails.findElements(By.cssSelector("html body form#ResPkgBookingDetailsForm div#wraper div#container div#content div#content-booking.box-shadow div#tripsummary div.result-container-booking div.payment_trip_summery_flightdetail_box.fleft.paddingall15 div.clearf.paddingtop5.paddingbot5.flight_outbound_arrive_mobile")));
			ArrayList<WebElement> inbounddepart  = null;
			ArrayList<WebElement> inboundarrive  = null;
			if(twoway)
			{
				inbounddepart = new ArrayList<WebElement>(paymentdetails.findElements(By.cssSelector("html body form#ResPkgBookingDetailsForm div#wraper div#container div#content div#content-booking.box-shadow div#tripsummary div.result-container-booking div.payment_trip_summery_flightdetail_box.fleft.paddingall15 div.clearf.paddingtop5.paddingbot5.flight_inbound_depart_mobile")));
				inboundarrive = new ArrayList<WebElement>(paymentdetails.findElements(By.cssSelector("html body form#ResPkgBookingDetailsForm div#wraper div#container div#content div#content-booking.box-shadow div#tripsummary div.result-container-booking div.payment_trip_summery_flightdetail_box.fleft.paddingall15 div.clearf.paddingtop5.paddingbot5.flight_inbound_arrive_mobile")));
			}
			

			ArrayList<Flight> outboundflights = new ArrayList<Flight>();
			Outbound outbound = new Outbound();
			for(int out=0; out<outbounddepart.size(); out++)
			{	
				WebElement depart = outbounddepart.get(out);
				Flight flight = new Flight();
				
				flight.setDepartureDate(depart.findElement(By.cssSelector("html body form#ResPkgBookingDetailsForm div#wraper div#container div#content div#content-booking.box-shadow div#tripsummary div.result-container-booking div.payment_trip_summery_flightdetail_box.fleft.paddingall15 div.clearf.paddingtop5.paddingbot5.flight_outbound_depart_mobile div.flightdate.fleft.marginlef10")).getText().trim().split(" ")[0]);
				flight.setDepartureTime(depart.findElement(By.cssSelector("html body form#ResPkgBookingDetailsForm div#wraper div#container div#content div#content-booking.box-shadow div#tripsummary div.result-container-booking div.payment_trip_summery_flightdetail_box.fleft.paddingall15 div.clearf.paddingtop5.paddingbot5.flight_outbound_depart_mobile div.flightdate.fleft.marginlef10")).getText().trim().split(" ")[1]);
					
				flight.setDeparture_port(depart.findElement(By.cssSelector("html body form#ResPkgBookingDetailsForm div#wraper div#container div#content div#content-booking.box-shadow div#tripsummary div.result-container-booking div.payment_trip_summery_flightdetail_box.fleft.paddingall15 div.clearf.paddingtop5.paddingbot5.flight_outbound_depart_mobile div.flightcode.fleft")).getText().trim().split("-")[0].trim());
				ArrayList<WebElement> list1 = new ArrayList<WebElement> (depart.findElements(By.cssSelector("html body form#ResPkgBookingDetailsForm div#wraper div#container div#content div#content-booking.box-shadow div#tripsummary div.result-container-booking div.payment_trip_summery_flightdetail_box.fleft.paddingall15 div.clearf.paddingtop5.paddingbot5.flight_outbound_depart_mobile div.flightcode.fleft")));
				System.out.println(list1.get(0).getText());
				flight.setDeparture_port(list1.get(0).getText().trim().split("-")[0].trim());
				flight.setDepartureLocationCode(list1.get(0).getText().trim().split("[()]")[1].trim());
				System.out.println(list1.get(1).getText());
				flight.setMarketingAirline(list1.get(1).getText().trim().split("[(]")[0]);
				flight.setMarketingAirline_Loc_Code(list1.get(1).getText().trim().split("[()]")[1]);
				flight.setFlightNo(list1.get(1).getText().trim().split("[()]")[1]);
					
				WebElement arrive = outboundarrive.get(out);
				flight.setArrivalDate(arrive.findElement(By.cssSelector("html body form#ResPkgBookingDetailsForm div#wraper div#container div#content div#content-booking.box-shadow div#tripsummary div.result-container-booking div.payment_trip_summery_flightdetail_box.fleft.paddingall15 div.clearf.paddingtop5.paddingbot5.flight_outbound_arrive_mobile div.flightdate.fleft.marginlef10")).getText().trim().split(" ")[0]);
				flight.setArrivalTime(arrive.findElement(By.cssSelector("html body form#ResPkgBookingDetailsForm div#wraper div#container div#content div#content-booking.box-shadow div#tripsummary div.result-container-booking div.payment_trip_summery_flightdetail_box.fleft.paddingall15 div.clearf.paddingtop5.paddingbot5.flight_outbound_arrive_mobile div.flightdate.fleft.marginlef10")).getText().trim().split(" ")[1]);
				flight.setArrival_port(arrive.findElement(By.cssSelector("html body form#ResPkgBookingDetailsForm div#wraper div#container div#content div#content-booking.box-shadow div#tripsummary div.result-container-booking div.payment_trip_summery_flightdetail_box.fleft.paddingall15 div.clearf.paddingtop5.paddingbot5.flight_outbound_arrive_mobile div.flightcode.fleft")).getText().trim().split("-")[0].trim());
				flight.setArrivalLocationCode(arrive.findElement(By.cssSelector("html body form#ResPkgBookingDetailsForm div#wraper div#container div#content div#content-booking.box-shadow div#tripsummary div.result-container-booking div.payment_trip_summery_flightdetail_box.fleft.paddingall15 div.clearf.paddingtop5.paddingbot5.flight_outbound_arrive_mobile div.flightcode.fleft")).getText().trim().split("[()]")[1].trim());					
				outboundflights.add(flight);
			}
			outbound.setOutBflightlist(outboundflights);
			uipaymentpage.setOutbound(outbound);
			
			if(twoway)
			{
				ArrayList<Flight> inboundflights = new ArrayList<Flight>();
				Inbound inbound = new Inbound();
				for(int in=0; in<inbounddepart.size(); in++)
				{
					WebElement depart = inbounddepart.get(in);
					Flight flight = new Flight();
						
					flight.setDepartureDate(depart.findElement(By.cssSelector("html body form#ResPkgBookingDetailsForm div#wraper div#container div#content div#content-booking.box-shadow div#tripsummary div.result-container-booking div.payment_trip_summery_flightdetail_box.fleft.paddingall15 div.clearf.paddingtop5.paddingbot5.flight_inbound_depart_mobile div.flightdate.fleft.marginlef10")).getText().trim().split(" ")[0]);
					flight.setDepartureTime(depart.findElement(By.cssSelector("html body form#ResPkgBookingDetailsForm div#wraper div#container div#content div#content-booking.box-shadow div#tripsummary div.result-container-booking div.payment_trip_summery_flightdetail_box.fleft.paddingall15 div.clearf.paddingtop5.paddingbot5.flight_inbound_depart_mobile div.flightdate.fleft.marginlef10")).getText().trim().split(" ")[1]);
					ArrayList<WebElement> list1 = new ArrayList<WebElement> (depart.findElements(By.cssSelector("html body form#ResPkgBookingDetailsForm div#wraper div#container div#content div#content-booking.box-shadow div#tripsummary div.result-container-booking div.payment_trip_summery_flightdetail_box.fleft.paddingall15 div.clearf.paddingtop5.paddingbot5.flight_inbound_depart_mobile div.flightcode.fleft")));
					//System.out.println(list1.get(0).getText());
					flight.setDeparture_port(list1.get(0).getText().trim().split("-")[0].trim());
					flight.setDepartureLocationCode(list1.get(0).getText().trim().split("[()]")[1].trim());
					//System.out.println(list1.get(1).getText());
					flight.setMarketingAirline(list1.get(1).getText().trim().split("[(]")[0]);
					flight.setMarketingAirline_Loc_Code(list1.get(1).getText().trim().split("[()]")[1]);
					flight.setFlightNo(list1.get(1).getText().trim().split("[()]")[1]);
						
					WebElement arrive = inboundarrive.get(in);
					flight.setArrivalDate(arrive.findElement(By.cssSelector("html body form#ResPkgBookingDetailsForm div#wraper div#container div#content div#content-booking.box-shadow div#tripsummary div.result-container-booking div.payment_trip_summery_flightdetail_box.fleft.paddingall15 div.clearf.paddingtop5.paddingbot5.flight_inbound_arrive_mobile div.flightdate.fleft.marginlef10")).getText().trim().split(" ")[0]);
					flight.setArrivalTime(arrive.findElement(By.cssSelector("html body form#ResPkgBookingDetailsForm div#wraper div#container div#content div#content-booking.box-shadow div#tripsummary div.result-container-booking div.payment_trip_summery_flightdetail_box.fleft.paddingall15 div.clearf.paddingtop5.paddingbot5.flight_inbound_arrive_mobile div.flightdate.fleft.marginlef10")).getText().trim().split(" ")[1]);
					flight.setArrival_port(arrive.findElement(By.cssSelector("html body form#ResPkgBookingDetailsForm div#wraper div#container div#content div#content-booking.box-shadow div#tripsummary div.result-container-booking div.payment_trip_summery_flightdetail_box.fleft.paddingall15 div.clearf.paddingtop5.paddingbot5.flight_inbound_arrive_mobile div.flightcode.fleft")).getText().trim().split("-")[0].trim());
					flight.setArrivalLocationCode(arrive.findElement(By.cssSelector("html body form#ResPkgBookingDetailsForm div#wraper div#container div#content div#content-booking.box-shadow div#tripsummary div.result-container-booking div.payment_trip_summery_flightdetail_box.fleft.paddingall15 div.clearf.paddingtop5.paddingbot5.flight_inbound_arrive_mobile div.flightcode.fleft")).getText().trim().split("[()]")[1].trim());
						
					inboundflights.add(flight);
				}
				
				inbound.setInBflightlist(inboundflights);
				uipaymentpage.setInbound(inbound);
			}
			
			
			WebElement payment = paymentdetails.findElement(By.cssSelector("html body form#ResPkgBookingDetailsForm div#wraper div#container div#content div#content-booking.box-shadow div#tripsummary div.result-container-booking div.payment_trip_summery_flightdetail_box.fleft.paddingall15 div.trip_summery_total_box.fright.paddingall15"));
			UIPriceInfo priceinfo = new UIPriceInfo();
			priceinfo.setCurrencycode(payment.findElement(By.cssSelector("html body form#ResPkgBookingDetailsForm div#wraper div#container div#content div#content-booking.box-shadow div#tripsummary div.result-container-booking div.payment_trip_summery_flightdetail_box.fleft.paddingall15 div.trip_summery_total_box.fright.paddingall15 div.fright.textright")).getText());
			
			ArrayList<WebElement> costlist = new ArrayList<WebElement>(payment.findElements(By.cssSelector("html body form#ResPkgBookingDetailsForm div#wraper div#container div#content div#content-booking.box-shadow div#tripsummary div.result-container-booking div.payment_trip_summery_flightdetail_box.fleft.paddingall15 div.trip_summery_total_box.fright.paddingall15 div.fright.trip_summery_total_box_values.textright")));
			priceinfo.setTotalbefore(costlist.get(0).getText());
			priceinfo.setTax(costlist.get(1).getText());
			priceinfo.setBookingfee(costlist.get(2).getText());
			
			WebElement totalcost = payment.findElement(By.cssSelector("html body form#ResPkgBookingDetailsForm div#wraper div#container div#content div#content-booking.box-shadow div#tripsummary div.result-container-booking div.payment_trip_summery_flightdetail_box.fleft.paddingall15 div.trip_summery_total_box.fright.paddingall15 div.fleft.trip_summery_total_box_subtotal.wid100per"));
			priceinfo.setTotalCost(totalcost.findElement(By.cssSelector("html body form#ResPkgBookingDetailsForm div#wraper div#container div#content div#content-booking.box-shadow div#tripsummary div.result-container-booking div.payment_trip_summery_flightdetail_box.fleft.paddingall15 div.trip_summery_total_box.fright.paddingall15 div.fleft.trip_summery_total_box_subtotal.wid100per div.fright.textright")).getText());
			
			WebElement finaltotalele = driver.findElement(By.cssSelector("html body form#ResPkgBookingDetailsForm div#wraper div#container div#content div#content-booking.box-shadow div#tripsummary div.result-container-booking div.trip_summery_fulltotal_box.fright.paddingall15"));
			priceinfo.setSubtotal(finaltotalele.findElement(By.cssSelector("html body form#ResPkgBookingDetailsForm div#wraper div#container div#content div#content-booking.box-shadow div#tripsummary div.result-container-booking div.trip_summery_fulltotal_box.fright.paddingall15 div.fright.trip_summery_total_box_values.textright")).getText());
			priceinfo.setTaxplusother(finaltotalele.findElement(By.id("tottaxamountdivid")).getText());
			priceinfo.setTotal(finaltotalele.findElement(By.id("totchargeamt")).getText());	
			
			WebElement element = finaltotalele.findElement(By.className("total_col"));
			
			try {
				priceinfo.setAmountprocess(element.findElement(By.id("totpayNowamt")).getText());
			} catch (Exception e) {
				e.printStackTrace();
			}
			
			priceinfo.setAmountprocessAirline(element.findElement(By.cssSelector("html body form#ResPkgBookingDetailsForm div#wraper div#container div#content div#content-booking.box-shadow div#tripsummary div.result-container-booking div.trip_summery_fulltotal_box.fright.paddingall15 div.total_col div.fright.trip_summery_total_box_values.textright")).getText());
			
			uipaymentpage.setUisummarypay(priceinfo);
			
			UICreditCardPayInfo cardinfo = new UICreditCardPayInfo();
			
			cardinfo.setTotalpackage_bookVal(driver.findElement(By.id("totchgpgamt")).getText().trim());
			cardinfo.setTotalpackage_bookVal_CurrCode(driver.findElement(By.cssSelector("html body form#ResPkgBookingDetailsForm div#wraper div#container div#content div#content-booking.box-shadow div#paymentsec div.result-container-booking div#xxxx div.clearfix div.paddingall10 div table tbody tr#masterCardPaymentRow td div.booking-tdetails-1 table tbody tr td span.redtext")).getText());
			
			ArrayList<WebElement> cardlist = new ArrayList<WebElement>(driver.findElements(By.cssSelector("html body form#ResPkgBookingDetailsForm div#wraper div#container div#content div#content-booking.box-shadow div#paymentsec div.result-container-booking div#xxxx div.clearfix div.paddingall10 div table tbody tr#masterCardPaymentRow td div.booking-tdetails-1")));
			ArrayList<WebElement> trlist = new ArrayList<WebElement>(cardlist.get(1).findElements(By.tagName("tr")));

			cardinfo.setSubtotal(trlist.get(0).getText().trim().split("[)]")[1].trim());
			cardinfo.setTaxandfees(trlist.get(1).getText().trim().split("[)]")[1].trim());
			cardinfo.setTotal(trlist.get(3).getText().trim().split("[)]")[1].trim());
			cardinfo.setAmountprocess(trlist.get(4).getText().split("[)]")[1].trim());
			cardinfo.setAmountprocessAirline(trlist.get(5).getText().split(" ")[6].trim());
			
			uipaymentpage.setCreditcardpay(cardinfo);
			
			uipaymentpage.setAmendmentemail(driver.findElement(By.cssSelector("html body form#ResPkgBookingDetailsForm div#wraper div#container div#content div#content-booking.box-shadow div#tconditions div.result-container-booking div.fleft.payment_page_form_main div.payment_page_fulltext.fleft.paddingall15 div div div.fleft a")).getText());
			System.out.println(driver.findElement(By.cssSelector("html body form#ResPkgBookingDetailsForm div#wraper div#container div#content div#content-booking.box-shadow div#tconditions div.result-container-booking div.fleft.payment_page_form_main div.payment_page_fulltext.fleft.paddingall15 div div div.fleft")).getText().split(":")[0]);
			uipaymentpage.setAmendmentphone(driver.findElement(By.cssSelector("html body form#ResPkgBookingDetailsForm div#wraper div#container div#content div#content-booking.box-shadow div#tconditions div.result-container-booking div.fleft.payment_page_form_main div.payment_page_fulltext.fleft.paddingall15 div div div.fleft")).getText().split(":")[0].trim());
			uipaymentpage.setCancellationdate(driver.findElement(By.cssSelector("html body form#ResPkgBookingDetailsForm div#wraper div#container div#content div#content-booking.box-shadow div#tconditions div#tconditions div.result-container-booking div div.fleft.payment_page_form_main div.payment_page_fulltext.fleft.paddingall15 div.paddingall15 table tbody tr td b.redtext")).getText().split(":")[1].trim());
		}
		catch(Exception e)
		{
			e.printStackTrace();
			TakesScreenshot screen = (TakesScreenshot)driver;
			FileUtils.copyFile(screen.getScreenshotAs(OutputType.FILE),new File("FailedScreenshots/Payment page - Web"+System.currentTimeMillis()/1000+".jpg"));
		}	
		
		return uipaymentpage;
	}

	public UIConfirmationPage getConfirmationPage(WebDriver driver, XMLPriceItinerary XMLSelectedFlight) throws WebDriverException, IOException
	{
		UIConfirmationPage uiconfirmationpage = new UIConfirmationPage();
		
		@SuppressWarnings("unused")
		boolean twoway = false;
		if(XMLSelectedFlight.getDirectiontype().equals("Circle"))
		{
			twoway = true;
		}
		
		try
		{
			String bookingSummery = driver.findElement(By.className("confirmation_message")).getText();
			String[] Summery = bookingSummery.split("\\n");
			ArrayList<String> summerylist = new ArrayList<String>(Arrays.asList(Summery));
			
			//String canceldate = driver.findElement(By.xpath("/html/body/div[12]/div/div[2]/div[1]/div[10]/div[2]/div[2]/div[2]/div/div/table/tbody/tr[2]/td/b")).getText().split(":")[1].trim();
			//String canceldate = driver.findElement(By.cssSelector("html body div#wraper div#container div#content div#content-booking.box-shadow div#tripsummary div#tripsummary div.result-container-booking div div.fleft.payment_page_form_main div.payment_page_fulltext.fleft.paddingall15 div.paddingall15 table tbody tr td b.redtext")).getText().split(":")[1].trim();
			try
			{
				String canceldate = driver.findElement(By.className("redtext")).getText().split(":")[1].trim();
				uiconfirmationpage.setCancellationdate(canceldate);
			}
			catch(Exception e)
			{
				e.printStackTrace();
				TakesScreenshot screen = (TakesScreenshot)driver;
				FileUtils.copyFile(screen.getScreenshotAs(OutputType.FILE),new File("FailedScreenshots/Cancellation Date(Confirmation page - Web)"+System.currentTimeMillis()/1000+".jpg"));
			}
			
			
			//System.out.println(summerylist.get(0)/*.split(":")[1].trim()*/);
			String general = "";
			
			general = summerylist.get(0).split(":")[1].trim();
			uiconfirmationpage.setBookingReference(general);
			
			general = summerylist.get(1).split("Reservation No  : ")[1].split("Supplier Confirmation No : ")[0].trim();
			uiconfirmationpage.setReservationNo(general.trim());
			if(!general.equals(""))
			{
				uiconfirmationpage.setAvailable(true);
			}
			general = summerylist.get(1).split("Supplier Confirmation No : ")[1].trim();
			uiconfirmationpage.setSupplierConfirmationNo(general.trim());
			
			WebElement Results = driver.findElement(By.xpath("/html/body/div[15]/div[1]/div[2]/div/div[3]/div[1]"));
			ArrayList<WebElement> ResultsTDList = new ArrayList<WebElement>(Results.findElements(By.tagName("div")));
			
			WebElement flightdetails	= ResultsTDList.get(0);
			WebElement paymentdetails	= ResultsTDList.get(1);/*driver.findElement(By.xpath("/html/body/div[12]/div/div[2]/div[1]/div[3]/div/div[2]"));*/
			
			String[] flightdetail		= flightdetails.getText().split("\\n");
			ArrayList<String> list		= new ArrayList<String>();
			for (int i=0; i<flightdetail.length; i++) 
			{
				list.add(flightdetail[i]);
			}
			
			uiconfirmationpage.setDeparture(list.get(0).split("to")[0]);
			uiconfirmationpage.setDestination(list.get(0).split("to")[1]);
			uiconfirmationpage.setFromDate(list.get(1).split("To")[0]);
			uiconfirmationpage.setToDate(list.get(1).split("To")[1]);
			list.remove(0);
			list.remove(0);
			for(int y=0; y<list.size(); y++) 
			{
				if(list.get(y).equals(""))
				{
					list.remove(y);
				}
			}
			//list.remove(a);
			list.removeAll(Collections.singleton(null)); 
			//System.out.println(list.size());
			ArrayList<RatesperPassenger> ratelist = new ArrayList<RatesperPassenger>();
			if(list.size()%4 == 0)
			{
				int u = list.size()/4;
				for(int y=0; y<u; y++)
				{
					RatesperPassenger rate = new RatesperPassenger();
					rate.setPassengertype(list.get(0).split("-")[1].trim());
					rate.setRateperpsngr(list.get(1).split("-")[1].trim());
					rate.setNoofpassengers(list.get(2).split("-")[1].trim());
					rate.setTotal(list.get(3).split("-")[1].trim());
					list.remove(0);
					list.remove(0);
					list.remove(0);
					list.remove(0);
					list.removeAll(Collections.singleton(null));
					ratelist.add(rate);
				}
				
				//System.out.println(ratelist);
				uiconfirmationpage.setRatelist(ratelist);		
			}
			else
			{
				//System.out.println(list.size());
			}
			
			/*ArrayList<WebElement> outbounddepart = new ArrayList<WebElement>(paymentdetails.findElements(By.cssSelector("/html/body/div[12]/div/div[2]/div[1]/div[3]/div/div[2]/div[2]")));
			ArrayList<WebElement> outboundarrive = new ArrayList<WebElement>(paymentdetails.findElements(By.cssSelector("/html/body/div[12]/div/div[2]/div[1]/div[3]/div/div[2]/div[3]")));
			ArrayList<WebElement> inbounddepart  = null;
			ArrayList<WebElement> inboundarrive  = null;
			if(twoway)
			{
				inbounddepart  = new ArrayList<WebElement>(paymentdetails.findElements(By.cssSelector("/html/body/div[12]/div/div[2]/div[1]/div[3]/div/div[2]/div[5]")));
				inboundarrive  = new ArrayList<WebElement>(paymentdetails.findElements(By.cssSelector("/html/body/div[12]/div/div[2]/div[1]/div[3]/div/div[2]/div[6]")));
			}
			
			
			ArrayList<Flight> outboundflights = new ArrayList<Flight>();
			Outbound outbound = new Outbound();
			for(int out=0; out<outbounddepart.size(); out++)
			{
				WebElement depart = outbounddepart.get(out);
				Flight flight = new Flight();
				
				flight.setDepartureDate(depart.findElement(By.xpath("/html/body/div[12]/div/div[2]/div[1]/div[3]/div/div[2]/div[2]/div[2]")).getText().trim().split(" ")[0]);
				flight.setDepartureTime(depart.findElement(By.xpath("/html/body/div[12]/div/div[2]/div[1]/div[3]/div/div[2]/div[2]/div[2]")).getText().trim().split(" ")[1]);
					
				flight.setDeparture_port(depart.findElement(By.cssSelector("/html/body/div[12]/div/div[2]/div[1]/div[3]/div/div[2]/div[2]/div[3]")).getText().trim().split("-")[0].trim());
				ArrayList<WebElement> list1 = new ArrayList<WebElement> (depart.findElements(By.cssSelector("html body form#ResPkgBookingDetailsForm div#wraper div#container div#content div#content-booking.box-shadow div#tripsummary div.result-container-booking div.payment_trip_summery_flightdetail_box.fleft.paddingall15 div.clearf.paddingtop5.paddingbot5.flight_outbound_depart_mobile div.flightcode.fleft")));
				System.out.println(list1.get(0).getText());
				flight.setDeparture_port(list1.get(0).getText().trim().split("-")[0].trim());
				flight.setDepartureLocationCode(list1.get(0).getText().trim().split("[()]")[1].trim());
				System.out.println(list1.get(1).getText());
				flight.setMarketingAirline(list1.get(1).getText().trim().split("[(]")[0]);
				flight.setMarketingAirline_Loc_Code(list1.get(1).getText().trim().split("[()]")[1]);
				flight.setFlightNo(list1.get(1).getText().trim().split("[()]")[1]);
					
				WebElement arrive = outboundarrive.get(out);
				flight.setArrivalDate(arrive.findElement(By.cssSelector("html body form#ResPkgBookingDetailsForm div#wraper div#container div#content div#content-booking.box-shadow div#tripsummary div.result-container-booking div.payment_trip_summery_flightdetail_box.fleft.paddingall15 div.clearf.paddingtop5.paddingbot5.flight_outbound_arrive_mobile div.flightdate.fleft.marginlef10")).getText().trim().split(" ")[0]);
				flight.setArrivalTime(arrive.findElement(By.cssSelector("html body form#ResPkgBookingDetailsForm div#wraper div#container div#content div#content-booking.box-shadow div#tripsummary div.result-container-booking div.payment_trip_summery_flightdetail_box.fleft.paddingall15 div.clearf.paddingtop5.paddingbot5.flight_outbound_arrive_mobile div.flightdate.fleft.marginlef10")).getText().trim().split(" ")[1]);
				flight.setArrival_port(arrive.findElement(By.cssSelector("html body form#ResPkgBookingDetailsForm div#wraper div#container div#content div#content-booking.box-shadow div#tripsummary div.result-container-booking div.payment_trip_summery_flightdetail_box.fleft.paddingall15 div.clearf.paddingtop5.paddingbot5.flight_outbound_arrive_mobile div.flightcode.fleft")).getText().trim().split("-")[0].trim());
				flight.setArrivalLocationCode(arrive.findElement(By.cssSelector("html body form#ResPkgBookingDetailsForm div#wraper div#container div#content div#content-booking.box-shadow div#tripsummary div.result-container-booking div.payment_trip_summery_flightdetail_box.fleft.paddingall15 div.clearf.paddingtop5.paddingbot5.flight_outbound_arrive_mobile div.flightcode.fleft")).getText().trim().split("[()]")[1].trim());					
				outboundflights.add(flight);
			}
			outbound.setOutBflightlist(outboundflights);
			uiconfirmationpage.setOutbound(outbound);*/
			
			try
			{
				WebElement payment = paymentdetails.findElement(By.cssSelector("html body div#wraper div#container div#content div#content-booking.box-shadow div#tripsummary div.group.result-container-booking div.trip_summery_flightdetail_box.fleft.paddingall15 div.clearf.paddingtop5.paddingbot5 div.trip_summery_total_box.fright.paddingall15"));
				//System.out.println(payment.getText());
				//WebElement payment = paymentdetails.findElement(By.className("trip_summery_total_box fright paddingall15"));
				//WebElement payment = paymentdetails.findElement(By.xpath("/html/body/div[12]/div/div[2]/div[1]/div[3]/div/div[2]/div[7]"));
				UIPriceInfo priceinfo = new UIPriceInfo();
				//System.out.println(payment.getText().split("\\n")[0]);
				priceinfo.setCurrencycode(payment.getText().split("\\n")[0]);
				
				ArrayList<WebElement> costlist = new ArrayList<WebElement>(payment.findElements(By.cssSelector("html body div#wraper div#container div#content div#content-booking.box-shadow div#tripsummary div.group.result-container-booking div.trip_summery_flightdetail_box.fleft.paddingall15 div.trip_summery_total_box.fright.paddingall15 div.fright.trip_summery_total_box_values.textright")));
				priceinfo.setTotalbefore(costlist.get(0).getText());
				priceinfo.setTax(costlist.get(1).getText());
				priceinfo.setBookingfee(costlist.get(2).getText());
				
				WebElement totalcost = payment.findElement(By.cssSelector("html body div#wraper div#container div#content div#content-booking.box-shadow div#tripsummary div.group.result-container-booking div.trip_summery_flightdetail_box.fleft.paddingall15 div.trip_summery_total_box.fright.paddingall15 div.fleft.trip_summery_total_box_subtotal.wid100per"));
				priceinfo.setTotalCost(totalcost.findElement(By.cssSelector("html body div#wraper div#container div#content div#content-booking.box-shadow div#tripsummary div.group.result-container-booking div.trip_summery_flightdetail_box.fleft.paddingall15 div.trip_summery_total_box.fright.paddingall15 div.fleft.trip_summery_total_box_subtotal.wid100per div.fright.textright")).getText());
			
				WebElement finaltotalele = driver.findElement(By.cssSelector("html body div#wraper div#container div#content div#content-booking.box-shadow div#tripsummary div.result-container-booking div.group.trip_summery_container div.trip_summery_fulltotal_box.fright.paddingall15"));
				priceinfo.setSubtotal(finaltotalele.findElement(By.cssSelector("html body div#wraper div#container div#content div#content-booking.box-shadow div#tripsummary div.result-container-booking div.group.trip_summery_container div.trip_summery_fulltotal_box.fright.paddingall15 div.fright.trip_summery_total_box_values.textright")).getText());
				priceinfo.setTaxplusother(finaltotalele.findElement(By.id("tottaxamountdivid")).getText());
				priceinfo.setTotal(finaltotalele.findElement(By.id("totchargeamt")).getText());
				
				WebElement element = finaltotalele.findElement(By.className("total_col"));
				//System.out.println(element.getText());
				//ArrayList<WebElement> elelist = new ArrayList<WebElement>(element.findElements(By.className("fright trip_summery_total_box_values textright")));
				
				priceinfo.setAmountprocess(element.getText().split("\\n")[1]/*.findElement(By.xpath("/html/body/div[12]/div/div[2]/div[1]/div[4]/div[2]/div/div[8]/div[2]")).getText()*/);
				priceinfo.setAmountprocessAirline(element.getText().split("\\n")[3]/*element.findElement(By.xpath("/html/body/div[12]/div/div[2]/div[1]/div[4]/div[2]/div/div[8]/div[4]")).getText()*/);
				
				uiconfirmationpage.setUisummarypay(priceinfo);
			}
			catch(Exception e)
			{
				e.printStackTrace();
				TakesScreenshot screen = (TakesScreenshot)driver;
				FileUtils.copyFile(screen.getScreenshotAs(OutputType.FILE),new File("FailedScreenshots/Payment breakdown (Confirmation page - Web)"+System.currentTimeMillis()/1000+".jpg"));
			}

			try
			{
				UICreditCardPayInfo creditcardpay		= new UICreditCardPayInfo();
				
				WebElement creditcard = driver.findElement(By.cssSelector("html body div#wraper div#container div#content div#content-booking.box-shadow div#tripsummary div#tripsummary div.result-container-booking div.rounded_container_min div.fleft.paddingall15"));
				ArrayList<WebElement> carddetailslist = (ArrayList<WebElement>) creditcard.findElements(By.className("fleft"));
				
				/*for(int y=0; y<carddetailslist.size(); y++)
				{
					//System.out.println(carddetailslist.get(y).getText());
				}*/
				
				creditcardpay.setReferenceNo(carddetailslist.get(1).getText());
				creditcardpay.setMerchantTrackID(carddetailslist.get(3).getText());
				creditcardpay.setPaymentId(carddetailslist.get(5).getText());
				//System.out.println(carddetailslist.get(6).getText().split("[()]")[1]);
				creditcardpay.setAmountCurrency(carddetailslist.get(6).getText().split("[()]")[1]);
				creditcardpay.setAmount(carddetailslist.get(7).getText());
			
				uiconfirmationpage.setCreditcardpay(creditcardpay);
			}
			catch(Exception e)
			{
				e.printStackTrace();
				TakesScreenshot screen = (TakesScreenshot)driver;
				FileUtils.copyFile(screen.getScreenshotAs(OutputType.FILE),new File("FailedScreenshots/CreditCard details (Confirmation page - Web)"+System.currentTimeMillis()/1000+".jpg"));
			}
			
			try
			{
				String main = driver.findElement(By.cssSelector("html body div#wraper div#container div#content div#content-booking.box-shadow div#tripsummary div#tripsummary div.result-container-booking")).getText();
				String[] arrmain = main.split("\\n");
				ArrayList<String> maincuslist = new ArrayList<String>(Arrays.asList(arrmain));
				
				Traveler maincus = new Traveler();
				
				maincus.setGivenName(maincuslist.get(1));
				maincus.setSurname(maincuslist.get(3));
				
				Address maincusaddress = new Address();
				maincusaddress.setAddressStreetNo(maincuslist.get(5));
				maincusaddress.setAddressCity(maincuslist.get(9));
				maincusaddress.setAddressCountry(maincuslist.get(11));
				maincusaddress.setStateProv(maincuslist.get(13));
				maincusaddress.setPostalCode(maincuslist.get(15));
				maincus.setAddress(maincusaddress);
				
				maincus.setPhoneNumber(maincuslist.get(17));
				maincus.setEmergencyNo(maincuslist.get(19));
				maincus.setEmail(maincuslist.get(21));
				
				uiconfirmationpage.setMaincustomer(maincus);	
			}
			catch(Exception e)
			{
				e.printStackTrace();
				TakesScreenshot screen = (TakesScreenshot)driver;
				FileUtils.copyFile(screen.getScreenshotAs(OutputType.FILE),new File("FailedScreenshots/CreditCard details (Confirmation page - Web)"+System.currentTimeMillis()/1000+".jpg"));
			}
			
			uiconfirmationpage.setAvailable(true);
		}
		catch(Exception e)
		{
			uiconfirmationpage.setAvailable(false);
			e.printStackTrace();
			TakesScreenshot screen = (TakesScreenshot)driver;
			FileUtils.copyFile(screen.getScreenshotAs(OutputType.FILE),new File("FailedScreenshots/CreditCard details (Confirmation page - Web)"+System.currentTimeMillis()/1000+".jpg"));
		}
		
		return uiconfirmationpage;
	}

}
