/*Sanoj*/
package Flight.Fare;

import java.util.ArrayList;

import Flight.Classes.XMLError;
import Flight.Classes.XMLPriceItinerary;

public class FareResponse 
{
	XMLError error						= null;
	
	ArrayList<XMLPriceItinerary> list	= new ArrayList<XMLPriceItinerary>();
	
	public XMLError getError() {
		return error;
	}

	public void setError(XMLError error) {
		this.error = error;
	}

	public ArrayList<XMLPriceItinerary> getList() {
		return list;
	}

	public void setList(ArrayList<XMLPriceItinerary> list) {
		this.list = list;
	}
}
