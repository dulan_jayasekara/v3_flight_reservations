package Flight.Reports;

//import java.io.File;
//import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
//import org.openqa.selenium.firefox.FirefoxDriver;
//import org.openqa.selenium.firefox.FirefoxProfile;

public class supplier_payble {
	
private WebDriver    driver                          ;
private boolean      isReportLoded                   ;	
private boolean      isPaid                          ;	
private boolean      isRecordExist                   ;
private String       ReservationNumber           = "-";
private String       DocumentNumber              = "-";
private String       BookingDate                 = "-";
private String       BookingChannel              = "-";
private String       ProductType                 = "-";
private String       ProductName                 = "-";
private String       ServiceElementDate          = "-";
private String       SupplierName                = "-";
private String       SupplierConf                = "-";
private String       BookingStatus               = "-";
private String       GuestName                   = "-";
private String       PortalCu_PayableAmount 	 = "-";
private String       PortalCu_TotalPaid 	     = "-";
private String       PortalCu_Balance  	         = "-";
private String       SupCu_Type 	             = "-";
private String       SupCu_Payable	             = "-";
private String       SupCu_TotaPaid              = "-";
private String       SupCu_Balance               = "-";	
private String       PaymentHistory              = "-";
private WebElement   Selectedelement             = null;


/*First Create object of  supplier_payble in Validation Class then call 
 * getpaybledetails method then validate with initially aquired data.
 * As the next step call PaySupplier with amount then again call getpaybledetails and validate.
 */



public supplier_payble(WebDriver driver,String ReservationNumber) {
	this.driver             = driver;
	this.ReservationNumber  = ReservationNumber;
	
}



public boolean loadReport(String BaseUrl, String Supplier)
{
	try {
		driver.get(BaseUrl+"/reports/operational/mainReport.do?reportId=36&reportName=Third Party Supplier Payable Report");
		driver.switchTo().frame("reportIframe");
		driver.findElement(By.id("supplierName")).clear();
		driver.findElement(By.id("supplierName")).sendKeys(Supplier);
		driver.findElement(By.id("supplierName_lkup")).click();
		driver.switchTo().defaultContent();
		driver.switchTo().frame("lookup");
		((JavascriptExecutor)driver).executeScript("selectedRecord(0,'loadAutoFillSelect();');");
		driver.switchTo().defaultContent();
		driver.switchTo().frame("reportIframe");
	    ((JavascriptExecutor)driver).executeScript("getReportData('view');");
	    
	    try {
			driver.findElement(By.name("paymentMethod"));
			return true;
		} catch (Exception e) {
			return false;
		}
	    
	} catch (Exception e) {
		return false;
	}
}

public void  getPaybleDetails(String BaseUrl, String Supplier)
{
	int         index      = 0;

	if(this.loadReport(BaseUrl, Supplier))
	{
		this.isReportLoded = true;
		driver.switchTo().defaultContent();
		driver.switchTo().frame("reportIframe");
		
		forloop:for (int i = 1; i < 12; i++) {
			WebElement  CurrentElement  = driver.findElement(By.id("RowNo_"+i+"_1"));
			String      ReservationNo   = CurrentElement.findElements(By.tagName("td")).get(1).getText().trim();
			
			if(this.ReservationNumber.equalsIgnoreCase(ReservationNo))
			{
				this.isRecordExist = true;
				index              = i;
			    Selectedelement    = CurrentElement;
				break  forloop;
			}
			
		}
		
		if(this.isRecordExist)
		{
			
			this.ProductType               = Selectedelement.findElements(By.tagName("td")).get(2).getText().trim();
			this.SupplierName              = Selectedelement.findElements(By.tagName("td")).get(3).getText().trim();
			this.GuestName                 = Selectedelement.findElements(By.tagName("td")).get(4).getText().trim();
			this.PortalCu_PayableAmount    = Selectedelement.findElements(By.tagName("td")).get(5).getText().trim();
			this.SupCu_Payable             = Selectedelement.findElements(By.tagName("td")).get(6).getText().trim();
			
			WebElement element1            = driver.findElement(By.id("RowNo_"+index+"_2"));
			
			this.DocumentNumber            = element1.findElements(By.tagName("td")).get(0).getText().trim();
			this.ProductName               = element1.findElements(By.tagName("td")).get(1).getText().trim();
			this.SupplierConf              = element1.findElements(By.tagName("td")).get(2).getText().trim();
			this.PortalCu_TotalPaid        = element1.findElements(By.tagName("td")).get(3).getText().trim();
			this.SupCu_TotaPaid            = element1.findElements(By.tagName("td")).get(4).getText().trim();
			
			WebElement element2            = driver.findElement(By.id("RowNo_"+index+"_3"));
			
			this.BookingDate               = element2.findElements(By.tagName("td")).get(0).getText().trim();
			this.ServiceElementDate        = element2.findElements(By.tagName("td")).get(1).getText().trim();
			this.BookingStatus             = element2.findElements(By.tagName("td")).get(2).getText().trim();
			this.PortalCu_Balance          = element2.findElements(By.tagName("td")).get(3).getText().trim();
			this.SupCu_Balance             = element2.findElements(By.tagName("td")).get(4).getText().trim();
			
			
		}
		
	}
	else {
		this.isReportLoded = false;
	}
}


public void paySupplier(String ref)
{
	Selectedelement.findElements(By.tagName("td")).get(8).click();
	driver.findElement(By.id("payReferNo")).sendKeys(ref);
	((JavascriptExecutor)driver).executeScript("JavaScript:getConfirmList();");
	
	//TODO:Handle the consequence here
	this.PaymentHistory   = Selectedelement.findElements(By.tagName("td")).get(9).getText();
	//JavaScript:getConfirmList();
}



/*public static void main(String[] args) {
	
	FirefoxProfile pro    = new FirefoxProfile(new File("C:\\Users\\Dulan\\AppData\\Roaming\\Mozilla\\Firefox\\Profiles\\vozwdepj.default"));
	WebDriver      driver = new FirefoxDriver(pro);
	
	driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
	driver.get("http://rezdemo2.rezgateway.com/rezpackage/admin/common/LoginPage.do");
	driver.findElement(By.id("user_id")).sendKeys("chandika");
	driver.findElement(By.id("password")).sendKeys("123456");
	driver.findElement(By.id("loginbutton")).click();
	
	supplier_payble supp = new supplier_payble(driver,"H0431C240714"); 
	supp.getPaybleDetails("http://rezdemo2.rezgateway.com/rezpackage", "ALL");
}*/




public String getPaymentHistory() {
	return PaymentHistory;
}



public WebDriver getDriver() {
	return driver;
}



public boolean isReportLoded() {
	return isReportLoded;
}



public boolean isPaid() {
	return isPaid;
}



public boolean isRecordExist() {
	return isRecordExist;
}



public String getReservationNumber() {
	return ReservationNumber;
}



public String getDocumentNumber() {
	return DocumentNumber;
}



public String getBookingDate() {
	return BookingDate;
}



public String getBookingChannel() {
	return BookingChannel;
}



public String getProductType() {
	return ProductType;
}



public String getProductName() {
	return ProductName;
}



public String getServiceElementDate() {
	return ServiceElementDate;
}



public String getSupplierName() {
	return SupplierName;
}



public String getSupplierConf() {
	return SupplierConf;
}



public String getBookingStatus() {
	return BookingStatus;
}



public String getGuestName() {
	return GuestName;
}



public String getPortalCu_PayableAmount() {
	return PortalCu_PayableAmount;
}



public String getPortalCu_TotalPaid() {
	return PortalCu_TotalPaid;
}



public String getPortalCu_Balance() {
	return PortalCu_Balance;
}



public String getSupCu_Type() {
	return SupCu_Type;
}



public String getSupCu_Payable() {
	return SupCu_Payable;
}



public String getSupCu_TotaPaid() {
	return SupCu_TotaPaid;
}



public String getSupCu_Balance() {
	return SupCu_Balance;
}





}
