/*Sanoj
package Air.Configuration;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebDriverException;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;

import Air.Configuration.AirConfiguration;
import Flight.Fare.FareRequest;
import Flight.Fare.FareRequestReader;
import Flight.Fare.FareResponse;
import Flight.Fare.FareResponseReader;
import Flight.Price.PriceResponse;
import Flight.Price.PriceResponseReader;
import Flight.Reports.BookingConfReportReader;
import Flight.Reports.BookingListReport;
import Flight.Reports.CustomerVoucherEmail;
import Flight.Reports.ProfitAndLoss;
import Flight.Reports.ReservationReportReader;
import Flight.Reports.SupplierPayableReport;
import Flight.Reservation.ReservationRequest;
import Flight.Reservation.ReservationResponse;
import Flight.Reservation.ResvRequestReader;
import Flight.Reservation.ResvResponseReader;
import Flight.RunClass.CallCenterRunner;
import Flight.RunClass.WebRunner;
import Flight.TypeEnum.SearchType;
import Flight.TypeEnum.XMLFileType;
import Flight.TypeEnum.XMLLocateType;
import Flight.eTicket.EticketRequest;
import Flight.eTicket.EticketRequestReader;
import Flight.eTicket.EticketResponse;
import Flight.eTicket.EticketResponseReader;
import Results.Validate.Validate;

import com.utilities.ExcelReader;
import com.utilities.FillReservationDetails;
import com.utilities.ReadProperties;
//import com.utilities.ReportGenerator;
import com.utilities.Repository;
import com.utilities.SupportMethods;

import Flight.Cancellation.CancellationReport;
import Flight.Cancellation.CancellationRequest;
import Flight.Cancellation.CancellationRequestReader;
import Flight.Cancellation.CancellationResponse;
import Flight.Cancellation.CancellationResponseReader;
import Flight.Cancellation.CancellationScreen;
import Flight.Classes.*;

import org.apache.commons.io.FileUtils;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;


public class RunnerCopy 
{
	HashMap<String, String>			Propertymap			= new HashMap<String, String>();
	String 							PropfilePath		= "../Flight_Reservation_Details/Properties_Dev3.properties";
	WebDriver 						driver				= null;
	boolean 						login				= false;
	ArrayList<Map<Integer, String>> XLtestData			= new ArrayList<Map<Integer,String>>();
	ArrayList<Map<Integer, String>> XLReservationData	= new ArrayList<Map<Integer,String>>();
	String 							tracer				= "";
	SupportMethods 					SUP					= null;
	StringBuffer 					ReportPrinter		= null;
	String							intermediateStatus	= "";
	String 							intermediateMessage	= "";
	int 							Reportcount			= 1;
	String 							payAmount			= "";
	String					 		payAmountCurrency	= "";
	String 							PageSource			= "";
	ArrayList<IntermediateInfo>		intermediateList	= new ArrayList<IntermediateInfo>();
	int 							m 					= 1;
	boolean							eticketIssued		= false;
	boolean							results				= false;
	Validate						validate			= null;
	
	
	
	
	
	@Before
	public void begin()
	{
		ReportPrinter        = new StringBuffer();
		SimpleDateFormat sdf = new SimpleDateFormat("MM/dd/yyyy");
		
		ReportPrinter.append("<html class='report'><head><link rel=\"stylesheet\" type=\"text/css\" href=\"style.css\"> </head>");
		ReportPrinter.append("<div class='header'><img height=\"19px\" src=\"menu_rez.png\"><p class='Hedding1'>Flight Reservation/Web("+sdf.format(Calendar.getInstance().getTime())+")</p></div>");
		ReportPrinter.append("<body>");
		try
		{
			Propertymap		= ReadProperties.readpropeties(PropfilePath);
			SUP				= new SupportMethods(Propertymap);
			driver			= SUP.initalizeDriver();
			login			= SUP.login(driver);
		} 
		catch (IOException e) 
		{
			e.printStackTrace();
		}
		
		ExcelReader Reader 	= new ExcelReader();
		XLtestData 			= Reader.init(Propertymap.get("XLPath"));
	}
	
	@Test
	public void test() throws WebDriverException, IOException 
	{
		WebRunner WebRun				= new WebRunner(Propertymap);
		CallCenterRunner CCRun			= new CallCenterRunner(Propertymap);
		
		ArrayList<SearchObject> list	= getSearchObjList();
		
		AirConfiguration airconfig		= new AirConfiguration(Propertymap);
		ArrayList<AirConfig> configlist	= airconfig.getConfigurationList();
		
		if(login)
		{
			Iterator<SearchObject> listiter = list.iterator();
			
			while( listiter.hasNext() )
			{
				SearchObject searchObject	= listiter.next();
				
				if(searchObject.getExcecuteStatus().equalsIgnoreCase("YES"))
				{
					ReportPrinter.append("<span><center><p class='Hedding0'>TEST CASE "+m+" STARTED</p></center></span>");
					ReportPrinter.append("<br>");
					Repository.setAll();
					
					try 
					{
						AirConfig configuration		= new AirConfig();
						try
						{
							AirConfig configuration1	= configlist.get(m-1);
							driver						= airconfig.setConfiguration(driver, configuration);
							configuration				= configuration1;
						}
						catch(Exception e)
						{
							e.printStackTrace();
						}
					
						IntermediateInfo	intermediateinfo	= new IntermediateInfo();
						String				type				= searchObject.getSearchType().trim();
											validate			= new Validate(Propertymap, driver);
						
						
						if(type.equals(SearchType.Call_Center.toString()) )
						{	
							Repository.setAll();
							
							driver				= CCRun.CCRUN(driver, searchObject);	
							SupportMethods succ = new SupportMethods();
							PopupMessage popup1 = new PopupMessage();
							driver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);
							
							if(popup1.getTitle().equals("Notification"))
							{
								//System.out.println("CAN NOT CONTINUE");
							}
							else
							{
								WebDriverWait wait = new WebDriverWait(driver, 20);
								
								driver.switchTo().defaultContent();
								driver.switchTo().frame("live_message_frame");
								
								String tracer = "null";
								try
								{
									wait.until(ExpectedConditions.presenceOfElementLocated(By.id("airridetracer_0")));
									driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
									tracer	= driver.findElement(By.id("airridetracer_0")).getAttribute("value");
									results = true;
									searchObject.setTracer(tracer);
								}
								catch(Exception e)
								{
									TakesScreenshot screen = (TakesScreenshot)driver;
									FileUtils.copyFile(screen.getScreenshotAs(OutputType.FILE),new File("FailedScreenshots/Test Case "+(m-1)+"- Unexpected.jpg"));
								}
								
								FareRequestReader farereqreader		= new FareRequestReader(Propertymap);
								FareResponseReader fareresreader	= new FareResponseReader(Propertymap);
								
								XMLFileType ReqType = null;
								XMLFileType ResType = null;
								
								if(!searchObject.getPreferredAirline().trim().equals("NONE"))
								{
									ReqType = XMLFileType.AIR1_PrefAirlineSearchRequest_;
									ResType = XMLFileType.AIR1_PrefAirlineSearchResponse_;
								}
								else
								{
									ReqType = XMLFileType.AIR1_LowFareSearchRequest_;
									ResType = XMLFileType.AIR1_LowFareSearchResponse_;
								}
								
								if(!results)
								{
									ReportPrinter.append("<span><center><p class='Hedding0'>No Results Notification Validation</p></center></span>");
									ReportPrinter.append("<table style=width:100%>"
									+ "<tr><th>Test Case</th>"
									+ "<th>Test Description</th>"
									+ "<th>Expected Result</th>"
									+ "<th>Actual Result</th>"
									+ "<th>Test Status</th></tr>");
									
									ReportPrinter.append("<tr><td>"+Reportcount+"</td>"
									+ "<td>Check proper implementation of No Results Notification</td>"
									+ "<td>No Results Available</td>");
									String notification = "";
									try
									{
										notification = driver.findElement(By.id("result_available_message")).getText().trim();
									}
									catch(Exception e)
									{
										e.printStackTrace();
									}
									
									if( (notification.equals("No Results Available")) )
									{
										ReportPrinter.append("<td>No Results Available</td>"
										+ "<td class='Passed'>PASS</td></tr>");
										Reportcount++;
										ReportPrinter.append("</table>");
										ReportPrinter.append("<br><br><br>");
									}
									else
									{
										ReportPrinter.append("<td>"+notification+"</td>"
										+ "<td class='Passed'>PASS</td></tr>");
										Reportcount++;
										ReportPrinter.append("</table>");
										ReportPrinter.append("<br><br><br>");
									}
								}
								
								if(results)
								{
									//=========================================LOWFARE REQUEST XML VALIDATION==============================================
									
									FareRequest fareRequest = farereqreader.RequestReader(XMLLocateType.TRACER, searchObject.getTracer(), ReqType);
									ReportPrinter.append("<span><center><p class='Hedding0'>LowFareRequest XML Validation</p></center></span>");
									ReportPrinter.append("<table style=width:100%>"
									+ "<tr><th>Test Case</th>"
									+ "<th>Test Description</th>"
									+ "<th>Expected Result</th>"
									+ "<th>Actual Result</th>"
									+ "<th>Test Status</th></tr>");
									try {
										ReportPrinter = validate.validateFareRequest(searchObject, fareRequest, ReportPrinter );
									} catch (Exception e) {
										e.printStackTrace();
									}
									ReportPrinter.append("</table>");
									ReportPrinter.append("<br><br><br>");
									
									
									//==============================================LOWFARE RESPONSE VALIDATION===============================================
									
									FareResponse response = new FareResponse();
									try
									{
										ReportPrinter.append("<span><center><p class='Hedding0'>Validation For LowFareResponse XML Errors</p></center></span>");
										ReportPrinter.append("<table style=width:100%>"
										+ "<tr><th>Test Case</th>"
										+ "<th>Test Description</th>"
										+ "<th>Expected Result</th>"
										+ "<th>Actual Result</th>"
										+ "<th>Test Status</th></tr>");
										try {
											response = fareresreader.ResponseReader(XMLLocateType.TRACER, searchObject.getTracer(), ResType);
										} catch (Exception e) {
											
										}
										
										ReportPrinter.append("<tr><td>"+Reportcount+"</td>"
										+ "<td>Check for errors in Low Fare Response XML</td>"
										+ "<td>If error occured - Error Message should be displayed, Else - Available</td>");
										
										if(response.getError().getErrorMessage().equals(""))
										{
											ReportPrinter.append("<td>Available</td>"
											+ "<td class='Passed'>PASS</td></tr>");
											Reportcount++;
											ReportPrinter.append("</table>");
											ReportPrinter.append("<br><br><br>");
										}
										else
										{
											ReportPrinter.append("<td>"+response.getError().getErrorMessage()+"</td>"
											+ "<td class='Failed'>Fail</td></tr>");
											Reportcount++;
											ReportPrinter.append("</table>");
											ReportPrinter.append("<br><br><br>");
										}
									}
									catch(Exception e)
									{
										e.printStackTrace();
										ReportPrinter.append("</table>");
										ReportPrinter.append("<br><br><br>");
									}
									
									
									//==================================RESULTS WITH LOWFARE RESPONSE XML VALIDATION=================================================
									
									ArrayList<UIFlightItinerary> CCResultlist	= new ArrayList<UIFlightItinerary>();
									CCResultlist								= CCRun.getResults(driver, searchObject);
										
									ReportPrinter.append("<span><center><p class='Hedding0'>Results Validation with LowFareResponse XML</p></center></span>");	
									ReportPrinter.append("<table style=width:100%>"
									+ "<tr><th>Test Case</th>"
									+ "<th>Test Description</th>"
									+ "<th>Expected Result</th>"
									+ "<th>Actual Result</th>"
									+ "<th>Test Status</th></tr>");
									try {
										ReportPrinter = validate.validateFareResponse(CCResultlist, response, searchObject, configuration, ReportPrinter );
									} catch (Exception e) {
										
									}
									ReportPrinter.append("</table>");
									ReportPrinter.append("<br><br><br>");
									
									
									//============================================CART VALIDATION===============================================================
									
									String				selection		= searchObject.getSelectingFlight();
									int					selectFlight	= Integer.parseInt(selection);
										
									PopupMessage		popupaddtocart	= succ.addToCartCC(driver, CCResultlist.size(), selectFlight);
										
									XMLPriceItinerary	XMLSelectFlight	= response.getList().get(selectFlight-1);
									PriceResponseReader priceresreader	= new PriceResponseReader(Propertymap);
									PriceResponse		priceresponse1	= priceresreader.AmadeusPriceResponseReader(XMLLocateType.TRACER, searchObject.getTracer(), XMLFileType.AIR1_PriceResponse_);
										
									ReportPrinter.append("<span><center><p class='Hedding0'>Error Validation for PriceResponse XML Errors</p></center></span>");
									ReportPrinter.append("<table style=width:100%>"
									+ "<tr><th>Test Case</th>"
									+ "<th>Test Description</th>"
									+ "<th>Expected Result</th>"
									+ "<th>Actual Result</th>"
									+ "<th>Test Status</th></tr>");
										
									ReportPrinter.append("<tr><td>"+Reportcount+"</td>"
									+ "<td>If error occured in Price Response XML</td>"
									+ "<td>If error occured - Error Message should be displayed, Else - empty</td>");
										
									if(!priceresponse1.getError().getErrorMessage().equals(""))
									{
										ReportPrinter.append("<td>"+priceresponse1.getError().getErrorMessage()+"</td>"
										+ "<td class='Failed'>Fail</td></tr>");
										Reportcount++;
										ReportPrinter.append("</table>");
										ReportPrinter.append("<br><br><br>");
									}
									else
									{		
										ReportPrinter.append("<td>"+priceresponse1.getError().getErrorMessage()+"</td>"
										+ "<td class='Passed'>Pass</td></tr>");
										Reportcount++;
										ReportPrinter.append("</table>");
										ReportPrinter.append("<br><br><br>");
										
										
										CartFlightCC cart	= new CartFlightCC();
										boolean ischange	= false;
										try {
											ischange	= validate.Ispricechanged(priceresponse1.getPriceinfo(), XMLSelectFlight.getPricinginfo());
										} catch (Exception e) {
											
										}
										
										
										//05-11-2014 ADD TO CART PRICE CHANGE
										if(ischange && popupaddtocart.getMessage().contains("Alert! Rates have being changed from"))
										{
											XMLSelectFlight.setPricinginfo(priceresponse1.getPriceinfo());
											XMLSelectFlight.getPricinginfo().setIschanged(ischange);
										}
										//XMLSelectFlight.getPricinginfo().setIschanged(ischange);
											
										ReportPrinter.append("<span><center><p class='Hedding0'>Price Validation While Add To Cart</p></center></span>");
										ReportPrinter.append("<table style=width:100%>"
										+ "<tr><th>Test Case</th>"
										+ "<th>Test Description</th>"
										+ "<th>Expected Result</th>"
										+ "<th>Actual Result</th>"
										+ "<th>Test Status</th></tr>");
												
										ReportPrinter.append("<tr><td>"+Reportcount+"</td>"
										+ "<td>If Prices have changed actual must give an alert message</td>"
										+ "<td>Error/Notification</td>");
												
										if(!popupaddtocart.getTitle().equals("") && ischange)
										{
											ReportPrinter.append("<td>Message : "+popupaddtocart.getMessage()+" Price Changed : "+ischange+"</td>"
											+ "<td class='Passed'>PASS</td></tr>");
											Reportcount++;
										}
										else if(!popupaddtocart.getTitle().equals("") && !ischange)
										{
											ReportPrinter.append("<td>Message : "+popupaddtocart.getMessage()+" Price Changed : "+ischange+"</td>"
											+ "<td class='Failed'>Fail</td></tr>");
											Reportcount++;
										}
										else if(ischange)
										{
											if(popupaddtocart.getTitle().equals(""))
											{
												ReportPrinter.append("<td>No popup but prices have changed!</td>"
												+ "<td class='Failed'>Fail</td></tr>");
												Reportcount++;
											}
										}
										else if(!ischange==false)
										{
											if(!popupaddtocart.getTitle().equals(""))
											{
												ReportPrinter.append("<td>Popup displayed but prices are same!</td>"
												+ "<td class='Failed'>Fail</td></tr>");
												Reportcount++;
											}
										}
										else
										{
											ReportPrinter.append("<td>Prices have not changed</td>"
											+ "<td class = 'Passed'>PASS</td></tr>");
											Reportcount++;
										}
											
										ReportPrinter.append("</table>");
										ReportPrinter.append("<br><br><br>");
										
										try {
											cart			= CCRun.getCCCart(driver, searchObject.getTriptype());
										} catch (Exception e) {
											
										}
												
										
										XMLSelectFlight	= validate.getNewPrice(cart.getCurrencyCode(), priceresponse1.getPriceinfo(), XMLSelectFlight, searchObject, configuration);
										ReportPrinter.append("<span><center><p class='Hedding0'>Cart Item Details Validation</p></center></span>");
										ReportPrinter.append("<table style=width:100%>"
										+ "<tr><th>Test Case</th>"
										+ "<th>Test Description</th>"
										+ "<th>Expected Result</th>"
										+ "<th>Actual Result</th>"
										+ "<th>Test Status</th></tr>");
										try {
											ReportPrinter = validate.validateCartCC(cart, priceresponse1.getPriceinfo(), XMLSelectFlight, searchObject, configuration, ReportPrinter);
										} catch (Exception e) {
											
										}
										ReportPrinter.append("</table>");
										ReportPrinter.append("<br><br>");
										
										
										//======================================================CHECKOUT==============================================================
										
										driver.findElement(By.id("loadpayment")).click();
										
										//================================================FILL PAYMENT PAGE UI DETAILS=================================================
										
										FillReservationDetails fill		= new FillReservationDetails();
										ReservationInfo fillingObject	= new ReservationInfo();
										fillingObject					= fill.Fill_Reservation_details(searchObject);	
										driver							= CCRun.Fill_Reservation_Details(driver, fillingObject, searchObject);
												
										ReportPrinter.append("<span><center><p class='Hedding0'>Unexpected Error Validation While Checkout Cart Item</p></center></span>");
										ReportPrinter.append("<table style=width:100%>"
										+ "<tr><th>Test Case</th>"
										+ "<th>Test Description</th>"
										+ "<th>Expected Result</th>"
										+ "<th>Actual Result</th>"
										+ "<th>Test Status</th></tr>");
										ReportPrinter.append("<tr><td>"+Reportcount+"</td>"
										+ "<td>Check blocker errors that prevents checkout</td>"
										+ "<td>If error occurred - Error Message should be displayed</td>");
													
										PopupMessage popup2	= new PopupMessage();
										popup2				= succ.popupHandler(driver);
												
										if(!popup2.getMessage().equals(""))
										{
											ReportPrinter.append("<td>Popup error message : "+popup2.getMessage()+"</td>"
											+ "<td class='Failed'>Fail</td></tr>");
											Reportcount++;
											ReportPrinter.append("</table>");
											ReportPrinter.append("<br><br><br>");
										}
										else
										{
											ReportPrinter.append("<td>No Errors</td>"
											+ "<td class='Passed'>PASS</td></tr>");
											Reportcount++;
											ReportPrinter.append("</table>");
											ReportPrinter.append("<br><br><br>");	
											
											
											//==============================================PAYMENT PAGE VALIDATION===============================================
											
											UIPaymentPage paymentpage = new UIPaymentPage(); 
											try {
												paymentpage = CCRun.getPaymentPage(driver, XMLSelectFlight, searchObject);	
											} catch (Exception e) {
												
											}
											
											ReportPrinter.append("<span><center><p class='Hedding0'>Payment Page Details Validation</p></center></span>");
											ReportPrinter.append("<table style=width:100%>"
											+ "<tr><th>Test Case</th>"
											+ "<th>Test Description</th>"
											+ "<th>Expected Result</th>"
											+ "<th>Actual Result</th>"
											+ "<th>Test Status</th></tr>");
											try {
												ReportPrinter = validate.validateCCPaymentPage(paymentpage, XMLSelectFlight, searchObject, configuration, ReportPrinter);
											} catch (Exception e) {
												
											}
											ReportPrinter.append("</table>");
											ReportPrinter.append("<br><br><br>");
												
											
											//=====================================CLICK PROCEED BUTTON IN PAYMENT PAGE==========================================
											
											driver.findElement(By.id("continue_submit")).click();
													
											try
											{		
												wait.until(ExpectedConditions.presenceOfAllElementsLocatedBy(By.id("status_F_AIR1")));
												intermediateStatus = driver.findElement(By.id("status_F_AIR1")).getText();
												intermediateinfo.setIntermediateStatus(intermediateStatus);
												intermediateMessage = driver.findElement(By.id("message_F_AIR1")).getText();
												intermediateinfo.setIntermediateMessage(intermediateMessage);
		
												driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
												WebDriverWait wait2 = new WebDriverWait(driver, 20);
												
												try
												{
													driver.findElement(By.id("confirmbooking")).click();
												}
												catch(Exception e)
												{
													e.printStackTrace();
												}
												
												if(searchObject.getPaymentMode().equalsIgnoreCase("Pay Online"))
												{
													try
													{
														driver.manage().timeouts().implicitlyWait(8, TimeUnit.SECONDS);
														driver.switchTo().frame("paygatewayFrame");
														wait2.until(ExpectedConditions.presenceOfElementLocated(By.id("cardnumberpart1")));
														((JavascriptExecutor)driver).executeScript("$('#cardnumberpart1').val('4111');");
														((JavascriptExecutor)driver).executeScript("$('#cardnumberpart2').val('1111');");
														((JavascriptExecutor)driver).executeScript("$('#cardnumberpart3').val('1111');");
														((JavascriptExecutor)driver).executeScript("$('#cardnumberpart4').val('1111');");
					
														new Select(driver.findElement(By.id("cardexpmonth"))).selectByVisibleText("12");
														new Select(driver.findElement(By.id("cardexpyear"))).selectByVisibleText("2032");
														driver.findElement(By.id("cardholdername")).sendKeys("Dan");
														((JavascriptExecutor)driver).executeScript("$('#cv2').val(123);");
													
														payAmountCurrency = driver.findElement(By.xpath("/html/body/div/div/form/div[1]/div[6]/div[1]/h3")).getText();
														intermediateinfo.setPayAmountCurrency(payAmountCurrency);
														payAmount = driver.findElement(By.xpath("/html/body/div/div/form/div[1]/div[6]/div[2]")).getText();
														intermediateinfo.setPayAmount(payAmount);
													
														driver.findElement(By.className("proceed_btn")).click();
													
														driver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);
														PageSource = driver.getPageSource();
														intermediateinfo.setCreditCPaySumeryPage(PageSource);
														intermediateList.add(intermediateinfo);
													
														driver.manage().timeouts().implicitlyWait(12, TimeUnit.SECONDS);
													}
													catch(Exception e)
													{
														
													}
												}
												
												//
												PopupMessage popupatCreditPay	= new PopupMessage();
												popupatCreditPay				= succ.popupHandler(driver);
												
												ReportPrinter.append("<span><center><p class='Hedding0'>Error Validation For Unexpected Blocker Issues while Payment</p></center></span>");
												ReportPrinter.append("<table style=width:100%>"
												+ "<tr><th>Test Case</th>"
												+ "<th>Test Description</th>"
												+ "<th>Expected Result</th>"
												+ "<th>Actual Result</th>"
												+ "<th>Test Status</th></tr>");
												ReportPrinter.append("<tr><td>"+Reportcount+"</td>"
												+ "<td>Test for payment gateway issues</td>"
												+ "<td>If error occured - Error Message should be displayed</td>");
												
												if( !popupatCreditPay.getMessage().equals("") && !popupatCreditPay.getMessage().contains("Unable to create the E-Ticket") )
												{
													ReportPrinter.append("<td>Popup error message : "+popupatCreditPay.getMessage()+"</td>"
													+ "<td class='Failed'>Fail</td></tr>");
													Reportcount++;
													ReportPrinter.append("</table>");
													ReportPrinter.append("<br><br><br>");
												
													ResvRequestReader	resvRequestReader1	= new ResvRequestReader(Propertymap);
													ReservationRequest	reservationReq1		= new ReservationRequest();
													try {
														reservationReq1		= resvRequestReader1.RequestReader(XMLLocateType.TRACER, searchObject.getTracer(), XMLFileType.AIR1_ReservationRequest_);
													} catch (Exception e) {
														
													}
													ReportPrinter.append("<span><center><p class='Hedding0'>Reservation Request XML Validation</p></center></span>");
													ReportPrinter.append("<table style=width:100%>"
													+ "<tr><th>Test Case</th>"
													+ "<th>Test Description</th>"
													+ "<th>Expected Result</th>"
													+ "<th>Actual Result</th>"
													+ "<th>Test Status</th></tr>");
													ReportPrinter.append("<tr><td>"+Reportcount+"</td>"
													+ "<td>Check Availability of Reservation Request XML</td>"
													+ "<td>Available</td>");
													
													if(reservationReq1.isAvailable())
													{
														ReportPrinter.append("<td>Available</td>"
														+ "<td class='Passed'>PASS</td></tr>");
														Reportcount++;
														try {
															ReportPrinter = validate.validateResvRequest(reservationReq1, fillingObject, XMLSelectFlight, ReportPrinter);
														} catch (Exception e) {
															
														}
														
														ReportPrinter.append("</table>");
														ReportPrinter.append("<br><br><br>");
													}
													else
													{
														ReportPrinter.append("<td>Not Available</td>"
														+ "<td class='Failed'>Fail</td></tr>");
														Reportcount++;
														ReportPrinter.append("</table>");
														ReportPrinter.append("<br><br><br>");
													}
													
													ResvResponseReader resvResponseReader1	= new ResvResponseReader(Propertymap);
													ReservationResponse reservationResp1	= new ReservationResponse();
													try {
														reservationResp1	= resvResponseReader1.ResponseReader(XMLLocateType.TRACER, searchObject.getTracer(), XMLFileType.AIR1_ReservationResponse_);
													} catch (Exception e) {
														
													}
													
													
													ReportPrinter.append("<span><center><p class='Hedding0'>Check for Reservation Response XML Error Caused for Blocker</p></center></span>");
													ReportPrinter.append("<table style=width:100%>"
													+ "<tr><th>Test Case</th>"
													+ "<th>Test Description</th>"
													+ "<th>Expected Result</th>"
													+ "<th>Actual Result</th>"
													+ "<th>Test Status</th></tr>");
													
													ReportPrinter.append("<tr><td>"+Reportcount+"</td>"
													+ "<td>Error occured in Reservation Response XML</td>"
													+ "<td>Error Message should be displayed</td>");
													
													if( !reservationResp1.isAvailable() )
													{
														if( !reservationResp1.getError().getErrorMessage().equals("") )
														{
															ReportPrinter.append("<td>XML error message : "+reservationResp1.getError().getErrorMessage()+"</td>"
															+ "<td class='Passed'>PASS</td></tr>");
															Reportcount++;
															
															ReportPrinter.append("<tr><td>"+Reportcount+"</td>"
															+ "<td>XA Popup message should be displayed in the system if an error occured in Reservation Response XML</td>"
															+ "<td>Displayed - TRUE</td>");
															if( !popupatCreditPay.getMessage().equals("") )
															{
																ReportPrinter.append("<td>TRUE</td>"
																+ "<td class='Passed'>PASS</td></tr>");
																Reportcount++;
															}
															else
															{
																ReportPrinter.append("<td>FALSE</td>"
																+ "<td class='Failed'>Fail</td></tr>");
																Reportcount++;
															}	
																
															ReportPrinter.append("</table>");
															ReportPrinter.append("<br><br><br>");
														}
														else
														{
															ReportPrinter.append("</table>");
															ReportPrinter.append("<br><br><br>");
														}
													}
													else
													{
														ReportPrinter.append("<td>XML error message : NO ERROR IN RESPONSE XML</td>"
														+ "<td class='Passed'>PASS</td></tr>");
														Reportcount++;
																
														ReportPrinter.append("<tr><td>"+Reportcount+"</td>"
														+ "<td>If Popup message displayed in the system without any error occurred in Reservation Response XML</td>"
														+ "<td>Displayed - TRUE</td>");
														if( !popupatCreditPay.getMessage().equals("") )
														{
															ReportPrinter.append("<td>TRUE</td>"
															+ "<td class='Passed'>PASS</td></tr>");
															Reportcount++;
														}
														else
														{
															ReportPrinter.append("<td>FALSE</td>"
															+ "<td class='Failed'>Fail</td></tr>");
															Reportcount++;
														}				
														ReportPrinter.append("</table>");
														ReportPrinter.append("<br><br><br>");
													}
												}
												else
												{
													if(popupatCreditPay.getMessage().contains("Unable to create the E-Ticket"))
													{
														eticketIssued = false;
													}
													else
													{
														eticketIssued = true;
													}
														
													ReportPrinter.append("<td>No Payment gateway errors</td>"
													+ "<td class='Passed'>PASS</td></tr>");
													Reportcount++;
													ReportPrinter.append("</table>");
													ReportPrinter.append("<br><br><br>");
													
													ResvRequestReader resvRequestReader = new ResvRequestReader(Propertymap);
													try 
													{
														Thread.sleep(4000);
													} catch (InterruptedException e) 
													{
														e.printStackTrace();
													}
													
													ReservationRequest reservationReq = new ReservationRequest();
													try {
														reservationReq = resvRequestReader.RequestReader(XMLLocateType.TRACER, searchObject.getTracer(), XMLFileType.AIR1_ReservationRequest_);
													} catch (Exception e) {
														
													}
													
													ReportPrinter.append("<span><center><p class='Hedding0'>ReservationRequest XML Validation</p></center></span>");
													ReportPrinter.append("<table style=width:100%>"
													+ "<tr><th>Test Case</th>"
													+ "<th>Test Description</th>"
													+ "<th>Expected Result</th>"
													+ "<th>Actual Result</th>"
													+ "<th>Test Status</th></tr>");
													ReportPrinter.append("<tr><td>"+Reportcount+"</td>"
													+ "<td>Check Availability of Reservation Request XML</td>"
													+ "<td>Available</td>");
													
													if(reservationReq.isAvailable())
													{
														ReportPrinter.append("<td>Available</td>"
														+ "<td class='Passed'>PASS</td></tr>");
														Reportcount++;
														
														try {
															ReportPrinter = validate.validateResvRequest(reservationReq, fillingObject, XMLSelectFlight, ReportPrinter);
														} catch (Exception e) {
															
														}
														
														ReportPrinter.append("</table>");
														ReportPrinter.append("<br><br><br>");
														
														ResvResponseReader resvResponseReader = new ResvResponseReader(Propertymap);
														try 
														{
															Thread.sleep(4000);
														} 
														catch (InterruptedException e) 
														{
															e.printStackTrace();
														}
														
														driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
														
														ReservationResponse reservationResp = new ReservationResponse();
														try {
															reservationResp = resvResponseReader.ResponseReader(XMLLocateType.TRACER, searchObject.getTracer(), XMLFileType.AIR1_ReservationResponse_);
														} catch (Exception e) {
															
														}
														
														
														ReportPrinter.append("<span><center><p class='Hedding0'>Unexpected Error Validation for ReservationResponse XML Errors</p></center></span>");
														ReportPrinter.append("<table style=width:100%>"
														+ "<tr><th>Test Case</th>"
														+ "<th>Test Description</th>"
														+ "<th>Expected Result</th>"
														+ "<th>Actual Result</th>"
														+ "<th>Test Status</th></tr>");
														
														ReportPrinter.append("<tr><td>"+Reportcount+"</td>"
														+ "<td>If error occured in Reservation Response XML</td>"
														+ "<td>If error occured - Error Message should be displayed</td>");
														
														PopupMessage PopupResvRespError = succ.popupHandler(driver);
														
														if( !reservationResp.getError().getErrorMessage().equals("") )
														{
															ReportPrinter.append("<td>XML error message : "+reservationResp.getError().getErrorMessage()+"</td>"
															+ "<td class='Failed'>Fail</td></tr>");
															Reportcount++;
																
															ReportPrinter.append("<tr><td>"+Reportcount+"</td>"
															+ "<td>A popup message should be displayed in the system if an error occured in Reservation Response XML</td>"
															+ "<td>If error occurred - Pop up error message content should be displayed</td>");
															if( !PopupResvRespError.getMessage().equals("") )
															{
																ReportPrinter.append("<td>"+PopupResvRespError.getMessage()+"</td>"
																+ "<td class='Failed'>Fail</td></tr>");
																Reportcount++;
															}
															else
															{
																ReportPrinter.append("<td>"+PopupResvRespError.getMessage()+"</td>"
																+ "<td class='Failed'>Fail</td></tr>");
																Reportcount++;
															}	
																
															ReportPrinter.append("</table>");
															ReportPrinter.append("<br><br><br>");
														}
														else
														{
															ReportPrinter.append("<td>No errors detected in Reservation Response XML</td>"
															+ "<td class='Passed'>Passed</td></tr>");
															Reportcount++;
															ReportPrinter.append("</table>");
															ReportPrinter.append("<br><br><br>");
															
															
															//===================================VALIDATE RESERVATION RESPONSE XML=====================================
															
															ReportPrinter.append("<span><center><p class='Hedding0'>ReservationResponse XML Validation</p></center></span>");
															ReportPrinter.append("<table style=width:100%>"
															+ "<tr><th>Test Case</th>"
															+ "<th>Test Description</th>"
															+ "<th>Expected Result</th>"
															+ "<th>Actual Result</th>"
															+ "<th>Test Status</th></tr>");
															try {
																ReportPrinter = validate.validateResvResponse(reservationResp, fillingObject, XMLSelectFlight, ReportPrinter);
															} catch (Exception e) {
																
															}
															ReportPrinter.append("</table>");
															ReportPrinter.append("<br><br><br>");
															
															
															//====================================VALIDATING PNR AND ETICKET XMLS=======================================
															
															//VALIDATE E-TICKET REQUEST XML WITH RESERVATION RESPONSE XML
															EticketRequestReader eTicketReqReader = new EticketRequestReader(Propertymap);
															EticketRequest eTicketRequest = eTicketReqReader.RequestReader(XMLLocateType.TRACER, searchObject.getTracer(), XMLFileType.AIR1_ETicketRequest_);
															ReportPrinter.append("<span><center><p class='Hedding0'>E-Ticket Request Validation</p></center></span>");
															ReportPrinter.append("<table style=width:100%>"
															+ "<tr><th>Test Case</th>"
															+ "<th>Test Description</th>"
															+ "<th>Expected Result</th>"
															+ "<th>Actual Result</th>"
															+ "<th>Test Status</th></tr>");
															ReportPrinter = validate.validateEticketRequest(eTicketRequest, confirmationpage, reservationResp, ReportPrinter);
															ReportPrinter.append("</table>");
															ReportPrinter.append("<br><br><br>");
															
															//VALIDATE E-TICKET RESPONSE XML WITH E-TICKET REQUEST XML
															EticketResponseReader eTicketResReader = new EticketResponseReader(Propertymap);
															EticketResponse eTicketResponse = eTicketResReader.ResponseReader(XMLLocateType.TRACER, searchObject.getTracer(), XMLFileType.AIR1_ETicketResponse_);
															
															ReportPrinter.append("<span><center><p class='Hedding0'>Testing E-Ticker Response XML errors</p></center></span>");
															ReportPrinter.append("<table style=width:100%>"
															+ "<tr><th>Test Case</th>"
															+ "<th>Test Description</th>"
															+ "<th>Expected Result</th>"
															+ "<th>Actual Result</th>"
															+ "<th>Test Status</th></tr>");
															
															ReportPrinter.append("<tr><td>"+Reportcount+"</td>"
															+ "<td>Check for E-ticket response errors</td>"
															+ "<td>If error occured - Error Message should be displayed</td>");
															if( !eTicketResponse.getError().getErrorMessage().equals("") )
															{
																ReportPrinter.append("<td>Popup error message : "+eTicketResponse.getError().getErrorMessage()+"</td>"
																+ "<td class='Passed'>Error occurred and Message displayed - PASS</td></tr>");
																Reportcount++;
																ReportPrinter.append("</table>");
																ReportPrinter.append("<br><br><br>");
															}
															else
															{
																ReportPrinter.append("<td>E-ticket response contains no errors</td>"
																+ "<td>TEST DID NOT OCCUR</td></tr>");
																Reportcount++;
																ReportPrinter.append("</table>");
																ReportPrinter.append("<br><br><br>");
																
																ReportPrinter.append("<span><center><p class='Hedding0'>E-Ticket Response Validation</p></center></span>");
																ReportPrinter.append("<table style=width:100%>"
																+ "<tr><th>Test Case</th>"
																+ "<th>Test Description</th>"
																+ "<th>Expected Result</th>"
																+ "<th>Actual Result</th>"
																+ "<th>Test Status</th></tr>");
																ReportPrinter = validate.validateEticketResponse(eTicketResponse, eTicketRequest, ReportPrinter);
																ReportPrinter.append("</table>");
																ReportPrinter.append("<br><br><br>");	
															}//End E-ticket response error handler
															
															
															//=========================================VALIDATE CONFIRMATION PAGE=======================================
															
															UIConfirmationPage confirmationpage = new UIConfirmationPage();
															try
															{
																try {
																	confirmationpage = CCRun.getConfirmationPage(driver, XMLSelectFlight);
																} catch (Exception e) {
																	
																}
																
																ReportPrinter.append("<span><center><p class='Hedding0'>Confirmation Page Validation</p></center></span>");
																ReportPrinter.append("<table style=width:100%>"
																+ "<tr><th>Test Case</th>"
																+ "<th>Test Description</th>"
																+ "<th>Expected Result</th>"
																+ "<th>Actual Result</th>"
																+ "<th>Test Status</th></tr>");
																try {
																	ReportPrinter = validate.validateConfirmationPageCC(searchObject, confirmationpage, ReportPrinter, fillingObject, XMLSelectFlight, reservationResp);
																} catch (Exception e) {
																	
																}
																
																ReportPrinter.append("</table>");
																ReportPrinter.append("<br><br><br>");
															}
															catch(Exception e)
															{
																e.printStackTrace();
															}
															
															//=========================================START VALIDATING REPORTS==========================================
															
															if(confirmationpage.isAvailable())
															{
																//===================================RESERVATION REPORT VALIDATION=======================================
																
																try {
																	ReportPrinter = validateReservationReport(confirmationpage, ReportPrinter, searchObject, XMLSelectFlight);
																} catch (Exception e) {
																	
																}
																
																//===================================SUPPLIER PAYABLE VALIDATION=======================================
																
																try {
																	ReportPrinter = validateSuppPlayableReport(confirmationpage, ReportPrinter, searchObject, fillingObject, XMLSelectFlight)(confirmationpage, ReportPrinter, searchObject, XMLSelectFlight);
																} catch (Exception e) {
																	
																}
																
																
																//=================================CUSTOMER VOUCHER E-MAIL VALIDATION====================================
																
																try {	
																	ReportPrinter = validateCustomerVoucherEmail(confirmationpage, ReportPrinter, searchObject, XMLSelectFlight, fillingObject);
																} catch (Exception e) {
																	
																}
																
																
																//===================================BOOKING CONFIRMATION VALIDATION======================================
																
																try {	
																	ReportPrinter = validateBookingConfReport(confirmationpage, searchObject, fillingObject, XMLSelectFlight, ReportPrinter);
																} catch (Exception e) {
																	
																}
																
																
																//==================================BOOKING LIST REPORT VALIDATION=========================================
																try {	
																	ReportPrinter = validateBookingListReport(confirmationpage, searchObject, fillingObject, XMLSelectFlight, ReportPrinter);
																} catch (Exception e) {
																	
																}
																
																
																//=========================================PROFIT AND LOSS REPORT=========================================
																
																try {	
																	ReportPrinter = validateProfitLossReport(confirmationpage, searchObject, fillingObject, XMLSelectFlight, ReportPrinter);
																} catch (Exception e) {
																	
																}
																
																//===========================================CANCELLATION PROCESS============================================
																if(searchObject.getCancellationStatus().equalsIgnoreCase("Yes"))
																{
																	CancellationScreen cancellationScreen = new CancellationScreen(Propertymap);
																	CancellationReport cancellationReport = new CancellationReport(Propertymap);
																	ReportPrinter = doCancellation(cancellationScreen, cancellationReport, confirmationpage, searchObject, fillingObject, XMLSelectFlight, ReportPrinter);
																	
																	//=====================================RESERVATION REPORT VALIDATION=====================================
																	try {
																		ReportPrinter = validateReservationReportAfterCancel(confirmationpage, searchObject, XMLSelectFlight, ReportPrinter);
																	} catch (Exception e) {
																		
																	}
																	
																	//=========================================PROFIT AND LOSS REPORT========================================
																	try {	
																		ReportPrinter = validateProfitLossReportAfterCancel(cancellationScreen, cancellationReport, confirmationpage, searchObject, fillingObject, XMLSelectFlight, ReportPrinter);
																	} catch (Exception e) {
																		
																	}
																	
																	//======================================SUPPLIER PAYABLE VALIDATION======================================
																	try {
																		ReportPrinter = validateThirdPartySuppPayableAfterCancel(confirmationpage, searchObject, XMLSelectFlight, ReportPrinter);
																	} catch (Exception e) {
																		
																	}
																	
																	//=====================================BOOKING LIST REPORT VALIDATION====================================
																	try {	
																		ReportPrinter = validateBookingListReportAfterCancel(confirmationpage, searchObject, fillingObject, XMLSelectFlight, ReportPrinter);
																	} catch (Exception e) {
																		
																	}
																	
																}
																
															}
														}
													}
													else
													{
														ReportPrinter.append("<td>Not Available</td>"
														+ "<td class='Failed'>Fail</td></tr>");
														Reportcount++;
														ReportPrinter.append("</table>");
														ReportPrinter.append("<br><br><br>");
													}
												}
											}
											catch(Exception e)
											{
												e.printStackTrace();
												ReportPrinter.append("</table>");
												ReportPrinter.append("<br><br><br>");
											}	
										}//End Else blocker errors when checkout
									}
								}
	
								if(results)
								{
									ReportPrinter.append("<span><center><p class='Hedding0'>XML Links</p></center></span>");
									ReportPrinter.append("<table style=width:100%>"
									+ "<tr><th>XML Type</th>"
									+ "<th>Link</th></tr>");
									
									if(Repository.LowFareSearchRequest.contains(searchObject.getTracer()))
									{
										ReportPrinter.append("<tr><td>Low Fare Search Request</td>"+"<td><a href="+Repository.LowFareSearchRequest+" target = '_blank' >View XML File</a></td></tr>"); 
									}
									if(Repository.LowFareSearchResponse.contains(searchObject.getTracer()))
									{
										ReportPrinter.append("<tr><td>Low Fare Search Response</td>"+"<td><a href="+Repository.LowFareSearchResponse+" target = '_blank' >View XML File</a></td></tr>");
									}
									if(Repository.ReservationRequest.contains(searchObject.getTracer()))
									{
										ReportPrinter.append("<tr><td>Reservation Request</td>"+"<td><a href="+Repository.ReservationRequest+" target = '_blank' >View XML File</a></td></tr>");
									}
									if(Repository.ReservationResponse.contains(searchObject.getTracer()))
									{
										ReportPrinter.append("<tr><td>Reservation Response</td>"+"<td><a href="+Repository.ReservationResponse+" target = '_blank' >View XML File</a></td></tr>");
									}
									if(Repository.PrefAirlineSearchRequest.contains(searchObject.getTracer()))
									{
										ReportPrinter.append("<tr><td>Pref. Airline Search Request</td>"+"<td><a href="+Repository.PrefAirlineSearchRequest+" target = '_blank' >View XML File</a></td></tr>");
									}
									if(Repository.PrefAirlineSearchResponse.contains(searchObject.getTracer()))
									{
										ReportPrinter.append("<tr><td>Pref. Airline Search Response</td>"+"<td><a href="+Repository.PrefAirlineSearchResponse+" target = '_blank' >View XML File</a></td></tr>");
									}
									if(Repository.PriceRequest.contains(searchObject.getTracer()))
									{
										ReportPrinter.append("<tr><td>Price Request</td>"+"<td><a href="+Repository.PriceRequest+" target = '_blank' >View XML File</a></td></tr>");
									}
									if(Repository.PriceResponse.contains(searchObject.getTracer()))
									{
										ReportPrinter.append("<tr><td>Price Response</td>"+"<td><a href="+Repository.PriceResponse+" target = '_blank' >View XML File</a></td></tr>");
									}
									if(Repository.ETicketRequest.contains(searchObject.getTracer()))
									{
										ReportPrinter.append("<tr><td>E-Ticket Request</td>"+"<td><a href="+Repository.ETicketRequest+" target = '_blank' >View XML File</a></td></tr>");
									}
									if(Repository.ETicketResponse.contains(searchObject.getTracer()))
									{
										ReportPrinter.append("<tr><td>E-Ticket Response</td>"+"<td><a href="+Repository.ETicketResponse+" target = '_blank' >View XML File</a></td></tr>");
									}
									if(Repository.PNRUpdateRequest.contains(searchObject.getTracer()))
									{
										ReportPrinter.append("<tr><td>PNR Update Request</td>"+"<td><a href="+Repository.PNRUpdateRequest+" target = '_blank' >View XML File</a></td></tr>");
									}
									if(Repository.PNRUpdateResponse.contains(searchObject.getTracer()))
									{
										ReportPrinter.append("<tr><td>PNR Update Response</td>"+"<td><a href="+Repository.PNRUpdateResponse+" target = '_blank' >View XML File</a></td></tr>");
									}
									
									ReportPrinter.append("</table>");
									ReportPrinter.append("<br><br><br>");
								}
								
							}
								
						}//////////////////////////////////////////////////////////////////////////////////////////////////////////////////
						
						if(type.equals(SearchType.Web.toString()))
						{
							Repository.setAll();

							driver				= WebRun.WebRUN(driver, searchObject);
							SupportMethods su	= new SupportMethods();
							PopupMessage popup1 = new PopupMessage();
							
							if(popup1.getTitle().equals("Notification"))
							{
								System.out.println("CAN NOT CONTINUE");
							}
							else
							{
								driver.switchTo().defaultContent();
								WebDriverWait wait = new WebDriverWait(driver, 25);
								String tracer = "null";
								try
								{
									wait.until(ExpectedConditions.presenceOfElementLocated(By.id("airridetracer_0")));
									tracer	= driver.findElement(By.id("airridetracer_0")).getAttribute("value");
									results = true;
									searchObject.setTracer(tracer);
								}
								catch(Exception e)
								{
									e.printStackTrace();
								}
								
								FareRequestReader farereqreader		= new FareRequestReader(Propertymap);
								FareResponseReader fareresreader	= new FareResponseReader(Propertymap);
								
								XMLFileType ReqType = null;
								XMLFileType ResType = null;
								if(!searchObject.getPreferredAirline().trim().equals("NONE"))
								{
									ReqType = XMLFileType.AIR1_PrefAirlineSearchRequest_;
									ResType = XMLFileType.AIR1_PrefAirlineSearchResponse_;
								}
								else
								{
									ReqType = XMLFileType.AIR1_LowFareSearchRequest_;
									ResType = XMLFileType.AIR1_LowFareSearchResponse_;
								}
	
								if(!results)
								{
									ReportPrinter.append("<span><center><p class='Hedding0'>No Results Notification Validation</p></center></span>");
									ReportPrinter.append("<table style=width:100%>"
									+ "<tr><th>Test Case</th>"
									+ "<th>Test Description</th>"
									+ "<th>Expected Result</th>"
									+ "<th>Actual Result</th>"
									+ "<th>Test Status</th></tr>");
									
									ReportPrinter.append("<tr><td>"+Reportcount+"</td>"
									+ "<td>Check proper implementation of No Results Notification</td>"
									+ "<td>If Results Not Available In Results Page, Notification should be displayed</td>");
									
									PopupMessage popupErrorFareRes = su.popupHandler(driver);
									if(!popupErrorFareRes.getMessage().equals(""))
									{
										ReportPrinter.append("<td>"+popupErrorFareRes.getMessage()+"</td>"
										+ "<td class='Passed'>PASS</td></tr>");
										Reportcount++;
										ReportPrinter.append("</table>");
										ReportPrinter.append("<br><br><br>");
									}
									else
									{
										ReportPrinter.append("<td>"+popupErrorFareRes.getMessage()+"</td>"
										+ "<td>Fail</td></tr>");
										Reportcount++;
										ReportPrinter.append("</table>");
										ReportPrinter.append("<br><br><br>");
									}
								}
								
								if(results)
								{
									//=========================================LOWFARE REQUEST XML VALIDATION==============================================
									
									FareRequest fareRequest = farereqreader.RequestReader(XMLLocateType.TRACER, searchObject.getTracer(), ReqType);	
									//ReportPrinter.append("<span><center><p class='Hedding0'>TEST CASE "+m+" STARTED</p></center></span><br><br>");
									ReportPrinter.append("<span><center><p class='Hedding0'>LowFareRequest XML Validation</p></center></span>");
									ReportPrinter.append("<table style=width:100%>"
									+ "<tr><th>Test Case</th>"
									+ "<th>Test Description</th>"
									+ "<th>Expected Result</th>"
									+ "<th>Actual Result</th>"
									+ "<th>Test Status</th></tr>");
									try {
										ReportPrinter = validate.validateFareRequest(searchObject, fareRequest, ReportPrinter );
									} catch (Exception e) {
										
									}
									
									ReportPrinter.append("</table>");
									ReportPrinter.append("<br><br><br>");
									
									//==============================================LOWFARE RESPONSE VALIDATION===============================================
									
									FareResponse response = new FareResponse();
									try
									{
										ReportPrinter.append("<span><center><p class='Hedding0'>Validation For LowFareResponse XML Errors</p></center></span>");
										ReportPrinter.append("<table style=width:100%>"
										+ "<tr><th>Test Case</th>"
										+ "<th>Test Description</th>"
										+ "<th>Expected Result</th>"
										+ "<th>Actual Result</th>"
										+ "<th>Test Status</th></tr>");
										try {
											response = fareresreader.ResponseReader(XMLLocateType.TRACER, searchObject.getTracer(), ResType);
										} catch (Exception e) {
											
										}
										
										ReportPrinter.append("<tr><td>"+Reportcount+"</td>"
										+ "<td>Check for errors in Low Fare Response XML</td>"
										+ "<td>If error occured - Error Message should be displayed, Else - Available</td>");
										
										if(response.getError().getErrorMessage().equals(""))
										{
											ReportPrinter.append("<td>Available</td>"
											+ "<td class='Passed'>PASS</td></tr>");
											Reportcount++;
											ReportPrinter.append("</table>");
											ReportPrinter.append("<br><br><br>");
										}
										else
										{
											ReportPrinter.append("<td>"+response.getError().getErrorMessage()+"</td>"
											+ "<td class='Failed'>Fail</td></tr>");
											Reportcount++;
											ReportPrinter.append("</table>");
											ReportPrinter.append("<br><br><br>");
										}
									}
									catch(Exception e)
									{
										e.printStackTrace();
										ReportPrinter.append("</table>");
										ReportPrinter.append("<br><br><br>");
									}
									
									//==================================RESULTS WITH LOWFARE RESPONSE XML VALIDATION=================================================
									
									ArrayList<UIFlightItinerary> WebResultlist	= new ArrayList<UIFlightItinerary>();
									WebResultlist								= WebRun.getResults(driver, searchObject);							
	
									ReportPrinter.append("<span><center><p class='Hedding0'>Results Validation with LowFareResponse XML</p></center></span>");	
									ReportPrinter.append("<table style=width:100%>"
									+ "<tr><th>Test Case</th>"
									+ "<th>Test Description</th>"
									+ "<th>Expected Result</th>"
									+ "<th>Actual Result</th>"
									+ "<th>Test Status</th></tr>");
									try {
										ReportPrinter = validate.validateFareResponse(WebResultlist, response, searchObject, configuration, ReportPrinter );
									} catch (Exception e) {
										
									}
									
									ReportPrinter.append("</table>");
									ReportPrinter.append("<br><br><br>");
										
									//============================================CART VALIDATION===============================================================
									
									String				selection		= searchObject.getSelectingFlight();
									int					selectFlight	= Integer.parseInt(selection);
										
									PopupMessage		popupaddtocart	= su.addToCart(driver, WebResultlist.size(), selectFlight);
			
									XMLPriceItinerary	XMLSelectFlight	= response.getList().get(selectFlight-1);
									PriceResponseReader	priceresreader	= new PriceResponseReader(Propertymap);
									PriceResponse		priceresponse1	= priceresreader.AmadeusPriceResponseReader(XMLLocateType.TRACER, searchObject.getTracer(), XMLFileType.AIR1_PriceResponse_);
										
									ReportPrinter.append("<span><center><p class='Hedding0'>Error Validation For PriceResponse XML Errors</p></center></span>");
									ReportPrinter.append("<table style=width:100%>"
									+ "<tr><th>Test Case</th>"
									+ "<th>Test Description</th>"
									+ "<th>Expected Result</th>"
									+ "<th>Actual Result</th>"
									+ "<th>Test Status</th></tr>");
									
									ReportPrinter.append("<tr><td>"+Reportcount+"</td>"
									+ "<td>If error occured in Price Response XML</td>"
									+ "<td>If error occured - Error Message should be displayed, Else - empty</td>");
									
									if(!priceresponse1.getError().getErrorMessage().equals(""))
									{
										ReportPrinter.append("<td>"+priceresponse1.getError().getErrorMessage()+"</td>"
										+ "<td class='Failed'>Fail</td></tr>");
										Reportcount++;
										ReportPrinter.append("</table>");
										ReportPrinter.append("<br><br><br>");
									}
									else
									{
										ReportPrinter.append("<td>"+priceresponse1.getError()+"</td>"
										+ "<td class='Passed'>PASS</td></tr>");
										Reportcount++;
										ReportPrinter.append("</table>");
										ReportPrinter.append("<br><br><br>");
										
										CartFlight	cart		= new CartFlight();
										boolean		ischange	= false;
										try {
											ischange	= validate.Ispricechanged(priceresponse1.getPriceinfo(), XMLSelectFlight.getPricinginfo());	
										} catch (Exception e) {
											
										}
										
										//05-11-2014 ADD TO CART PRICE CHANGE
										if(ischange && popupaddtocart.getMessage().contains("Alert! Rates have being changed from"))
										{
											XMLSelectFlight.setPricinginfo(priceresponse1.getPriceinfo());
											XMLSelectFlight.getPricinginfo().setIschanged(ischange);
										}
										
										ReportPrinter.append("<span><center><p class='Hedding0'>Price Validation While Add To Cart</p></center></span>");
										ReportPrinter.append("<table style=width:100%>"
										+ "<tr><th>Test Case</th>"
										+ "<th>Test Description</th>"
										+ "<th>Expected Result</th>"
										+ "<th>Actual Result</th>"
										+ "<th>Test Status</th></tr>");
											
										ReportPrinter.append("<tr><td>"+Reportcount+"</td>"
										+ "<td>If Prices have changed actual must give an alert message</td>"
										+ "<td>Error/Notification</td>");
											
										if(!popupaddtocart.getTitle().equals("") && ischange)
										{
											ReportPrinter.append("<td>Message : "+popupaddtocart.getMessage()+" Price Changed : "+ischange+"</td>"
											+ "<td class='Passed'>PASS</td></tr>");
											Reportcount++;
										}
										else if(popupaddtocart.getTitle().equals("Error") && !ischange)
										{
											ReportPrinter.append("<td>Message : "+popupaddtocart.getMessage()+" Price Changed : "+ischange+"</td>"
											+ "<td class='Failed'>Fail</td></tr>");
											Reportcount++;
										}
										else if(ischange)
										{
											if(popupaddtocart.getTitle().equals(""))
											{
												ReportPrinter.append("<td>No popup but prices have changed!</td>"
												+ "<td class='Failed'>Fail</td></tr>");
												Reportcount++;
											}
										}
										else if(!ischange==false)
										{
											if(!popupaddtocart.getTitle().equals(""))
											{
												ReportPrinter.append("<td>Popup displayed but prices are same!</td>"
												+ "<td class='Failed'>Fail</td></tr>");
												Reportcount++;
											}
										}
										else
										{
											ReportPrinter.append("<td>Prices have not changed</td>"
											+ "<td class = 'Passed'>PASS</td></tr>");
											Reportcount++;
										}
										
										ReportPrinter.append("</table>");
										ReportPrinter.append("<br><br><br>");
										
										try {
											cart 			= WebRun.getWEBCART(driver);
										} catch (Exception e) {
											
										}
										
										
										XMLSelectFlight = validate.getNewPrice(cart.getCurrencyCode(), priceresponse1.getPriceinfo(), XMLSelectFlight, searchObject, configuration);
										ReportPrinter.append("<span><center><p class='Hedding0'>Cart Item Validation</p></center></span>");
										ReportPrinter.append("<table style=width:100%>"
										+ "<tr><th>Test Case</th>"
										+ "<th>Test Description</th>"
										+ "<th>Expected Result</th>"
										+ "<th>Actual Result</th>"
										+ "<th>Test Status</th></tr>");
										try {
											ReportPrinter = validate.validateCart(cart, priceresponse1.getPriceinfo(), XMLSelectFlight, searchObject, configuration, ReportPrinter);
										} catch (Exception e) {
											
										}
										ReportPrinter.append("</table>");
										ReportPrinter.append("<br><br>");
											
										//======================================================CHECKOUT==============================================================
										
										((JavascriptExecutor)driver).executeScript("JavaScript:loadPaymentPage();");
											
										//================================================FILL PAYMENT PAGE UI DETAILS=================================================
										
										FillReservationDetails	fill			= new FillReservationDetails();
										ReservationInfo			fillingObject	= new ReservationInfo();
										fillingObject							= fill.Fill_Reservation_details(searchObject);	
										driver									= WebRun.Fill_Reservation_Details(driver, fillingObject);
											
										ReportPrinter.append("<span><center><p class='Hedding0'>Unexpected Error Validation While Checkout Cart Item</p></center></span>");
										ReportPrinter.append("<table style=width:100%>"
										+ "<tr><th>Test Case</th>"
										+ "<th>Test Description</th>"
										+ "<th>Expected Result</th>"
										+ "<th>Actual Result</th>"
										+ "<th>Test Status</th></tr>");
										ReportPrinter.append("<tr><td>"+Reportcount+"</td>"
										+ "<td>Check blocker errors that prevents checkout</td>"
										+ "<td>If error occurred - Error Message should be displayed</td>");
										
										PopupMessage popup2 = new PopupMessage();
										popup2				= su.popupHandler(driver);
										
										if(!popup2.getMessage().equals(""))
										{
											ReportPrinter.append("<td>Popup error message : "+popup2.getMessage()+"</td>"
											+ "<td class='Failed'>Fail</td></tr>");
											Reportcount++;
											ReportPrinter.append("</table>");
											ReportPrinter.append("<br><br><br>");
										}
										else
										{
											ReportPrinter.append("<td>No Errors</td>"
											+ "<td class='Passed'>PASS</td></tr>");
											Reportcount++;
											ReportPrinter.append("</table>");
											ReportPrinter.append("<br><br><br>");
											
											//==============================================PAYMENT PAGE VALIDATION===============================================
											
											UIPaymentPage paymentpage = new UIPaymentPage();
											try {
												paymentpage = WebRun.getPaymentPage(driver, XMLSelectFlight);
											} catch (Exception e) {
												
											}
											
											ReportPrinter.append("<span><center><p class='Hedding0'>Payment Page Details Validation</p></center></span>");
											ReportPrinter.append("<table style=width:100%>"
											+ "<tr><th>Test Case</th>"
											+ "<th>Test Description</th>"
											+ "<th>Expected Result</th>"
											+ "<th>Actual Result</th>"
											+ "<th>Test Status</th></tr>");
											try {
												ReportPrinter = validate.validatePaymentPage(cart, paymentpage, XMLSelectFlight, searchObject, configuration, ReportPrinter);
											} catch (Exception e) {
												
											}
											ReportPrinter.append("</table>");
											ReportPrinter.append("<br><br><br>");
											
											//=====================================CLICK PROCEED BUTTON IN PAYMENT PAGE==========================================
											
											driver.findElement(By.cssSelector("html body form#ResPkgBookingDetailsForm div#wraper div#container div#content div#content-booking.box-shadow div#tconditions div#confirm-booking div.proceed_btn_holder a div.proceed_btn.m_proceed_btn")).click();
											
											try 
											{
												Thread.sleep(8000);
											} 
											catch (InterruptedException e1) 
											{
												e1.printStackTrace();
											}
											
											try
											{
												wait.until(ExpectedConditions.presenceOfAllElementsLocatedBy(By.id("status_F_AIR1")));
												intermediateStatus = driver.findElement(By.id("status_F_AIR1")).getText();
												intermediateinfo.setIntermediateStatus(intermediateStatus);
												intermediateMessage = driver.findElement(By.id("message_F_AIR1")).getText();
												intermediateinfo.setIntermediateMessage(intermediateMessage);
			
												driver.manage().timeouts().implicitlyWait(25, TimeUnit.SECONDS);
												WebDriverWait wait2 = new WebDriverWait(driver, 25);
												driver.switchTo().defaultContent();
												
												wait2.until(ExpectedConditions.presenceOfElementLocated(By.className("proceed_btn")));
												driver.findElement(By.className("proceed_btn")).click();
				
												driver.manage().timeouts().implicitlyWait(8, TimeUnit.SECONDS);
												driver.switchTo().frame("paygatewayFrame");
												wait2.until(ExpectedConditions.presenceOfElementLocated(By.id("cardnumberpart1")));
												((JavascriptExecutor)driver).executeScript("$('#cardnumberpart1').val('4111');");
												((JavascriptExecutor)driver).executeScript("$('#cardnumberpart2').val('1111');");
												((JavascriptExecutor)driver).executeScript("$('#cardnumberpart3').val('1111');");
												((JavascriptExecutor)driver).executeScript("$('#cardnumberpart4').val('1111');");
		
												new Select(driver.findElement(By.id("cardexpmonth"))).selectByVisibleText("12");
												new Select(driver.findElement(By.id("cardexpyear"))).selectByVisibleText("2032");
												driver.findElement(By.id("cardholdername")).sendKeys("Dan");
												((JavascriptExecutor)driver).executeScript("$('#cv2').val(123);");
												
												payAmountCurrency = driver.findElement(By.xpath("/html/body/div/div/form/div[1]/div[6]/div[1]/h3")).getText();
												intermediateinfo.setPayAmountCurrency(payAmountCurrency);
												payAmount = driver.findElement(By.xpath("/html/body/div/div/form/div[1]/div[6]/div[2]")).getText();
												intermediateinfo.setPayAmount(payAmount);
												
												
												driver.findElement(By.className("proceed_btn")).click();
												driver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);
												PageSource = driver.getPageSource();
												intermediateinfo.setCreditCPaySumeryPage(PageSource);
												
												intermediateList.add(intermediateinfo);
												
												driver.manage().timeouts().implicitlyWait(12, TimeUnit.SECONDS);
												
												PopupMessage popupatCreditPay	= new PopupMessage();
												popupatCreditPay				= su.popupHandler(driver);
													
												ReportPrinter.append("<span><center><p class='Hedding0'>Error Validation For Unexpected Blocker Issues while Payment</p></center></span>");
												ReportPrinter.append("<table style=width:100%>"
												+ "<tr><th>Test Case</th>"
												+ "<th>Test Description</th>"
												+ "<th>Expected Result</th>"
												+ "<th>Actual Result</th>"
												+ "<th>Test Status</th></tr>");
												ReportPrinter.append("<tr><td>"+Reportcount+"</td>"
												+ "<td>Test for payment gateway issues</td>"
												+ "<td>If error occured - Error Message should be displayed</td>");
													
												if( !popupatCreditPay.getMessage().equals("") && !popupatCreditPay.getMessage().contains("Unable to create the E-Ticket")  )
												{
													ReportPrinter.append("<td>Popup error message : "+popupatCreditPay.getMessage()+"</td>"
													+ "<td class='Failed'>Fail</td></tr>");
													Reportcount++;
													ReportPrinter.append("</table>");
													ReportPrinter.append("<br><br><br>");
														
													ResvRequestReader	resvRequestReader1	= new ResvRequestReader(Propertymap);
													ReservationRequest	reservationReq1		= new ReservationRequest();
													try {
														resvRequestReader1.RequestReader(XMLLocateType.TRACER, searchObject.getTracer(), XMLFileType.AIR1_ReservationRequest_);
													} catch (Exception e) {
														
													}
															
													ReportPrinter.append("<span><center><p class='Hedding0'>Reservation Request XML Validation</p></center></span>");
													ReportPrinter.append("<table style=width:100%>"
													+ "<tr><th>Test Case</th>"
													+ "<th>Test Description</th>"
													+ "<th>Expected Result</th>"
													+ "<th>Actual Result</th>"
													+ "<th>Test Status</th></tr>");
													ReportPrinter.append("<tr><td>"+Reportcount+"</td>"
													+ "<td>Check Availability of Reservation Request XML</td>"
													+ "<td>Available</td>");
													
													if(reservationReq1.isAvailable())
													{
														ReportPrinter.append("<td>Available</td>"
														+ "<td class='Passed'>PASS</td></tr>");
														Reportcount++;
														try {
															ReportPrinter = validate.validateResvRequest(reservationReq1, fillingObject, XMLSelectFlight, ReportPrinter);
														} catch (Exception e) {
															
														}
														
														ReportPrinter.append("</table>");
														ReportPrinter.append("<br><br><br>");
													}
													else
													{
														ReportPrinter.append("<td>Not Available</td>"
														+ "<td class='Passed'>PASS</td></tr>");
														Reportcount++;
														ReportPrinter.append("</table>");
														ReportPrinter.append("<br><br><br>");
													}
														
													ResvResponseReader	resvResponseReader1	= new ResvResponseReader(Propertymap);
													ReservationResponse reservationResp1	= new ReservationResponse();
													try {
														resvResponseReader1.ResponseReader(XMLLocateType.TRACER, searchObject.getTracer(), XMLFileType.AIR1_ReservationResponse_);
													} catch (Exception e) {
														
													}
															
													
													ReportPrinter.append("<span><center><p class='Hedding0'>Check for Reservation Response XML Error Caused for Blocker</p></center></span>");
													ReportPrinter.append("<table style=width:100%>"
													+ "<tr><th>Test Case</th>"
													+ "<th>Test Description</th>"
													+ "<th>Expected Result</th>"
													+ "<th>Actual Result</th>"
													+ "<th>Test Status</th></tr>");
													
													ReportPrinter.append("<tr><td>"+Reportcount+"</td>"
													+ "<td>Error occured in Reservation Response XML</td>"
													+ "<td>Error Message should be displayed</td>");
													
													if( !reservationResp1.isAvailable() )
													{
														if( !reservationResp1.getError().getErrorMessage().equals("") )
														{
															ReportPrinter.append("<td>XML error message : "+reservationResp1.getError().getErrorMessage()+"</td>"
															+ "<td class='Passed'>PASS</td></tr>");
															Reportcount++;
															
															ReportPrinter.append("<tr><td>"+Reportcount+"</td>"
															+ "<td>XA Popup message should be displayed in the system if an error occured in Reservation Response XML</td>"
															+ "<td>Displayed - TRUE</td>");
															if( !popupatCreditPay.getMessage().equals("") )
															{
																ReportPrinter.append("<td>TRUE</td>"
																+ "<td class='Passed'>PASS</td></tr>");
																Reportcount++;
															}
															else
															{
																ReportPrinter.append("<td>FALSE</td>"
																+ "<td class='Failed'>Fail</td></tr>");
																Reportcount++;
															}	
																
															ReportPrinter.append("</table>");
															ReportPrinter.append("<br><br><br>");
														}
														else
														{
															ReportPrinter.append("</table>");
															ReportPrinter.append("<br><br><br>");
														}
													}
													else
													{
														ReportPrinter.append("<td>XML error message : NO ERROR IN RESPONSE XML</td>"
														+ "<td class='Passed'>PASS</td></tr>");
														Reportcount++;
																
														ReportPrinter.append("<tr><td>"+Reportcount+"</td>"
														+ "<td>If Popup message displayed in the system without any error occurred in Reservation Response XML</td>"
														+ "<td>Displayed - TRUE</td>");
														if( !popupatCreditPay.getMessage().equals("") )
														{
															ReportPrinter.append("<td>TRUE</td>"
															+ "<td class='Passed'>PASS</td></tr>");
															Reportcount++;
														}
														else
														{
															ReportPrinter.append("<td>FALSE</td>"
															+ "<td class='Failed'>Fail</td></tr>");
															Reportcount++;
														}				
														ReportPrinter.append("</table>");
														ReportPrinter.append("<br><br><br>");
													}
												}
												else
												{
													if(popupatCreditPay.getMessage().contains("Unable to create the E-Ticket"))
													{
														eticketIssued = false;
													}
													else
													{
														eticketIssued = true;
													}
													
													ReportPrinter.append("<td>No Payment gateway errors</td>"
													+ "<td class='Passed'>PASS</td></tr>");
													Reportcount++;
													ReportPrinter.append("</table>");
													ReportPrinter.append("<br><br><br>");
														
													ResvRequestReader resvRequestReader = new ResvRequestReader(Propertymap);
													try 
													{
														Thread.sleep(6000);
													} catch (InterruptedException e) 
													{
														e.printStackTrace();
													}
													
													ReservationRequest reservationReq = new ReservationRequest();
													try {
														reservationReq = resvRequestReader.RequestReader(XMLLocateType.TRACER, searchObject.getTracer(), XMLFileType.AIR1_ReservationRequest_);
													} catch (Exception e) {
														
													}
													
													ReportPrinter.append("<span><center><p class='Hedding0'>ReservationRequest XML Validation</p></center></span>");
													ReportPrinter.append("<table style=width:100%>"
													+ "<tr><th>Test Case</th>"
													+ "<th>Test Description</th>"
													+ "<th>Expected Result</th>"
													+ "<th>Actual Result</th>"
													+ "<th>Test Status</th></tr>");
													ReportPrinter.append("<tr><td>"+Reportcount+"</td>"
													+ "<td>Check Availability of Reservation Request XML</td>"
													+ "<td>Available</td>");
													
													if(reservationReq.isAvailable())
													{
														ReportPrinter.append("<td>Available</td>"
														+ "<td class='Passed'>PASS</td></tr>");
														Reportcount++;
														
														try {
															ReportPrinter = validate.validateResvRequest(reservationReq, fillingObject, XMLSelectFlight, ReportPrinter);
														} catch (Exception e) {
															
														}
														
														ReportPrinter.append("</table>");
														ReportPrinter.append("<br><br><br>");
														
														ResvResponseReader resvResponseReader = new ResvResponseReader(Propertymap);
														try 
														{
															Thread.sleep(10000);
														}
														catch (InterruptedException e) 
														{
															e.printStackTrace();
														}
														
														driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
														
														ReservationResponse reservationResp = new ReservationResponse();
														try {
															resvResponseReader.ResponseReader(XMLLocateType.TRACER, searchObject.getTracer(), XMLFileType.AIR1_ReservationResponse_);
														} catch (Exception e) {
															
														}
																
														
														ReportPrinter.append("<span><center><p class='Hedding0'>Unexpected Error Validation For ReservationResponse XML Errors</p></center></span>");
														ReportPrinter.append("<table style=width:100%>"
														+ "<tr><th>Test Case</th>"
														+ "<th>Test Description</th>"
														+ "<th>Expected Result</th>"
														+ "<th>Actual Result</th>"
														+ "<th>Test Status</th></tr>");
														
														ReportPrinter.append("<tr><td>"+Reportcount+"</td>"
														+ "<td>If error occured in Reservation Response XML</td>"
														+ "<td>If error occured - Error Message should be displayed</td>");
														
														PopupMessage PopupResvRespError = su.popupHandler(driver);
															
														if( !reservationResp.getError().getErrorMessage().equals("") )
														{
															ReportPrinter.append("<td>XML error message : "+reservationResp.getError().getErrorMessage()+"</td>"
															+ "<td class='Failed'>Fail</td></tr>");
															Reportcount++;
															
															ReportPrinter.append("<tr><td>"+Reportcount+"</td>"
															+ "<td>A popup message should be displayed in the system if an error occured in Reservation Response XML</td>"
															+ "<td>If error occurred - Pop up error message content should be displayed</td>");
															if( !PopupResvRespError.getMessage().equals("") )
															{
																ReportPrinter.append("<td>"+PopupResvRespError.getMessage()+"</td>"
																+ "<td class='Failed'>Fail</td></tr>");
																Reportcount++;
															}
															else
															{
																ReportPrinter.append("<td>"+PopupResvRespError.getMessage()+"</td>"
																+ "<td class='Failed'>Fail</td></tr>");
																Reportcount++;
															}	
															
															ReportPrinter.append("</table>");
															ReportPrinter.append("<br><br><br>");
														}
														else
														{
															ReportPrinter.append("<td>No errors detected in Reservation Response XML</td>"
															+ "<td class='Passed'>PASS</td></tr>");
															Reportcount++;
															ReportPrinter.append("</table>");
															ReportPrinter.append("<br><br><br>");
															
															//===================================VALIDATE RESERVATION RESPONSE XML=====================================
															
															ReportPrinter.append("<span><center><p class='Hedding0'>ReservationResponse XML Validation</p></center></span>");
															ReportPrinter.append("<table style=width:100%>"
															+ "<tr><th>Test Case</th>"
															+ "<th>Test Description</th>"
															+ "<th>Expected Result</th>"
															+ "<th>Actual Result</th>"
															+ "<th>Test Status</th></tr>");
															try {
																ReportPrinter = validate.validateResvResponse(reservationResp, fillingObject, XMLSelectFlight, ReportPrinter);
															} catch (Exception e) {
																
															}
															
															ReportPrinter.append("</table>");
															ReportPrinter.append("<br><br><br>");
															
															
															//====================================VALIDATING PNR AND ETICKET XMLS=======================================
															
															//VALIDATE E-TICKET REQUEST XML WITH RESERVATION RESPONSE XML
															EticketRequestReader eTicketReqReader = new EticketRequestReader(Propertymap);
															EticketRequest eTicketRequest = eTicketReqReader.RequestReader(XMLLocateType.TRACER, searchObject.getTracer(), XMLFileType.AIR1_ETicketRequest_);
															ReportPrinter.append("<span><center><p class='Hedding0'>E-Ticket Request Validation</p></center></span>");
															ReportPrinter.append("<table style=width:100%>"
															+ "<tr><th>Test Case</th>"
															+ "<th>Test Description</th>"
															+ "<th>Expected Result</th>"
															+ "<th>Actual Result</th>"
															+ "<th>Test Status</th></tr>");
															ReportPrinter = validate.validateEticketRequest(eTicketRequest, confirmationpage, reservationResp, ReportPrinter);
															ReportPrinter.append("</table>");
															ReportPrinter.append("<br><br><br>");
															
															//VALIDATE E-TICKET RESPONSE XML WITH E-TICKET REQUEST XML
															EticketResponseReader eTicketResReader = new EticketResponseReader(Propertymap);
															EticketResponse eTicketResponse = eTicketResReader.ResponseReader(XMLLocateType.TRACER, searchObject.getTracer(), XMLFileType.AIR1_ETicketResponse_);
															
															ReportPrinter.append("<span><center><p class='Hedding0'>Testing E-Ticker Response XML errors</p></center></span>");
															ReportPrinter.append("<table style=width:100%>"
															+ "<tr><th>Test Case</th>"
															+ "<th>Test Description</th>"
															+ "<th>Expected Result</th>"
															+ "<th>Actual Result</th>"
															+ "<th>Test Status</th></tr>");
															
															ReportPrinter.append("<tr><td>"+Reportcount+"</td>"
															+ "<td>Check for E-ticket response errors</td>"
															+ "<td>If error occured - Error Message should be displayed</td>");
															if( !eTicketResponse.getError().getErrorMessage().equals("") )
															{
																ReportPrinter.append("<td>Popup error message : "+eTicketResponse.getError().getErrorMessage()+"</td>"
																+ "<td class='Passed'>Error occurred and Message displayed - PASS</td></tr>");
																Reportcount++;
																ReportPrinter.append("</table>");
																ReportPrinter.append("<br><br><br>");
															}
															else
															{
																ReportPrinter.append("<td>E-ticket response contains no errors</td>"
																+ "<td>TEST DID NOT OCCUR</td></tr>");
																Reportcount++;
																ReportPrinter.append("</table>");
																ReportPrinter.append("<br><br><br>");
																
																ReportPrinter.append("<span><center><p class='Hedding0'>E-Ticket Response Validation</p></center></span>");
																ReportPrinter.append("<table style=width:100%>"
																+ "<tr><th>Test Case</th>"
																+ "<th>Test Description</th>"
																+ "<th>Expected Result</th>"
																+ "<th>Actual Result</th>"
																+ "<th>Test Status</th></tr>");
																ReportPrinter = validate.validateEticketResponse(eTicketResponse, eTicketRequest, ReportPrinter);
																ReportPrinter.append("</table>");
																ReportPrinter.append("<br><br><br>");	
															}//End E-ticket response error handler   	
															
															
															//=========================================VALIDATE CONFIRMATION PAGE=======================================
															
															UIConfirmationPage confirmationpage = new UIConfirmationPage();
															try
															{
																try {
																	confirmationpage = WebRun.getConfirmationPage(driver, XMLSelectFlight);
																} catch (Exception e) {
																	
																}
																
																ReportPrinter.append("<span><center><p class='Hedding0'>Confirmation Page Validation</p></center></span>");
																ReportPrinter.append("<table style=width:100%>"
																+ "<tr><th>Test Case</th>"
																+ "<th>Test Description</th>"
																+ "<th>Expected Result</th>"
																+ "<th>Actual Result</th>"
																+ "<th>Test Status</th></tr>");
																
																try {
																	ReportPrinter = validate.validateConfirmationPage(confirmationpage, ReportPrinter, fillingObject, XMLSelectFlight, reservationResp);
																} catch (Exception e) {
																	
																}
																
																ReportPrinter.append("</table>");
																ReportPrinter.append("<br><br><br>");
															}
															catch(Exception e)
															{
																e.printStackTrace();
															}
															
															//=========================================START VALIDATING REPORTS==========================================
															
															if(confirmationpage.isAvailable())
															{
																//===================================RESERVATION REPORT VALIDATION=======================================
																
																try {
																	ReportPrinter = validateReservationReport(confirmationpage, ReportPrinter, searchObject, XMLSelectFlight);
																} catch (Exception e) {
																	
																}
																
																//===================================SUPPLIER PAYABLE VALIDATION=======================================
																
																try {
																	ReportPrinter = validateSuppPlayableReport(confirmationpage, ReportPrinter, searchObject, fillingObject, XMLSelectFlight)(confirmationpage, ReportPrinter, searchObject, XMLSelectFlight);
																} catch (Exception e) {
																	
																}
																
																
																//=================================CUSTOMER VOUCHER E-MAIL VALIDATION====================================
																
																try {	
																	ReportPrinter = validateCustomerVoucherEmail(confirmationpage, ReportPrinter, searchObject, XMLSelectFlight, fillingObject);
																} catch (Exception e) {
																	
																}
																
																
																//===================================BOOKING CONFIRMATION VALIDATION======================================
																
																try {	
																	ReportPrinter = validateBookingConfReport(confirmationpage, searchObject, fillingObject, XMLSelectFlight, ReportPrinter);
																} catch (Exception e) {
																	
																}
																
																
																//==================================BOOKING LIST REPORT VALIDATION=========================================
																try {	
																	ReportPrinter = validateBookingListReport(confirmationpage, searchObject, fillingObject, XMLSelectFlight, ReportPrinter);
																} catch (Exception e) {
																	
																}
																
																
																//=========================================PROFIT AND LOSS REPORT=========================================
																
																try {	
																	ReportPrinter = validateProfitLossReport(confirmationpage, searchObject, fillingObject, XMLSelectFlight, ReportPrinter);
																} catch (Exception e) {
																	
																}
																
																//===========================================CANCELLATION PROCESS============================================
																if(searchObject.getCancellationStatus().equalsIgnoreCase("Yes"))
																{
																	CancellationScreen cancellationScreen = new CancellationScreen(Propertymap);
																	CancellationReport cancellationReport = new CancellationReport(Propertymap);
																	ReportPrinter = doCancellation(cancellationScreen, cancellationReport, confirmationpage, searchObject, fillingObject, XMLSelectFlight, ReportPrinter);
																	
																	//=====================================RESERVATION REPORT VALIDATION=====================================
																	try {
																		ReportPrinter = validateReservationReportAfterCancel(confirmationpage, searchObject, XMLSelectFlight, ReportPrinter);
																	} catch (Exception e) {
																		
																	}
																	
																	//=========================================PROFIT AND LOSS REPORT========================================
																	try {	
																		ReportPrinter = validateProfitLossReportAfterCancel(cancellationScreen, cancellationReport, confirmationpage, searchObject, fillingObject, XMLSelectFlight, ReportPrinter);
																	} catch (Exception e) {
																		
																	}
																	
																	//======================================SUPPLIER PAYABLE VALIDATION======================================
																	try {
																		ReportPrinter = validateThirdPartySuppPayableAfterCancel(confirmationpage, searchObject, XMLSelectFlight, ReportPrinter);
																	} catch (Exception e) {
																		
																	}
																	
																	//=====================================BOOKING LIST REPORT VALIDATION====================================
																	try {	
																		ReportPrinter = validateBookingListReportAfterCancel(confirmationpage, searchObject, fillingObject, XMLSelectFlight, ReportPrinter);
																	} catch (Exception e) {
																		
																	}
																	
																}
																
															}
																
														}//End Reservation Response error handler
													}
													else
													{
														ReportPrinter.append("<td>Not Available</td>"
														+ "<td class='Failed'>Fail</td></tr>");
														Reportcount++;
														ReportPrinter.append("</table>");
														ReportPrinter.append("<br><br><br>");
													}
	
												}//End Payment Gateway error handler
											}
											catch(Exception e)
											{
												e.printStackTrace();
												ReportPrinter.append("</table>");
												ReportPrinter.append("<br><br><br>");	
											}
											
										}//End confirmation pass
										
									}//Price response error handler end
									
								}//If results not available in results page
								
							}//No Notification
							
							if(results)
							{
								ReportPrinter.append("<span><center><p class='Hedding0'>XML Links</p></center></span>");
								ReportPrinter.append("<table style=width:100%>"
								+ "<tr><th>XML Type</th>"
								+ "<th>Link</th></tr>");
								if(Repository.LowFareSearchRequest.contains(searchObject.getTracer()))
								{
									ReportPrinter.append("<tr><td>Low Fare Search Request</td>"+"<td><a href="+Repository.LowFareSearchRequest+" target = '_blank' >View XML File</a></td></tr>"); 
								}
								if(Repository.LowFareSearchResponse.contains(searchObject.getTracer()))
								{
									ReportPrinter.append("<tr><td>Low Fare Search Response</td>"+"<td><a href="+Repository.LowFareSearchResponse+" target = '_blank' >View XML File</a></td></tr>");
								}
								if(Repository.ReservationRequest.contains(searchObject.getTracer()))
								{
									ReportPrinter.append("<tr><td>Reservation Request</td>"+"<td><a href="+Repository.ReservationRequest+" target = '_blank' >View XML File</a></td></tr>");
								}
								if(Repository.ReservationResponse.contains(searchObject.getTracer()))
								{
									ReportPrinter.append("<tr><td>Reservation Response</td>"+"<td><a href="+Repository.ReservationResponse+" target = '_blank' >View XML File</a></td></tr>");
								}
								if(Repository.PrefAirlineSearchRequest.contains(searchObject.getTracer()))
								{
									ReportPrinter.append("<tr><td>Pref. Airline Search Request</td>"+"<td><a href="+Repository.PrefAirlineSearchRequest+" target = '_blank' >View XML File</a></td></tr>");
								}
								if(Repository.PrefAirlineSearchResponse.contains(searchObject.getTracer()))
								{
									ReportPrinter.append("<tr><td>Pref. Airline Search Response</td>"+"<td><a href="+Repository.PrefAirlineSearchResponse+" target = '_blank' >View XML File</a></td></tr>");
								}
								if(Repository.PriceRequest.contains(searchObject.getTracer()))
								{
									ReportPrinter.append("<tr><td>Price Request</td>"+"<td><a href="+Repository.PriceRequest+" target = '_blank' >View XML File</a></td></tr>");
								}
								if(Repository.PriceResponse.contains(searchObject.getTracer()))
								{
									ReportPrinter.append("<tr><td>Price Response</td>"+"<td><a href="+Repository.PriceResponse+" target = '_blank' >View XML File</a></td></tr>");
								}
								if(Repository.ETicketRequest.contains(searchObject.getTracer()))
								{
									ReportPrinter.append("<tr><td>E-Ticket Request</td>"+"<td><a href="+Repository.ETicketRequest+" target = '_blank' >View XML File</a></td></tr>");
								}
								if(Repository.ETicketResponse.contains(searchObject.getTracer()))
								{
									ReportPrinter.append("<tr><td>E-Ticket Response</td>"+"<td><a href="+Repository.ETicketResponse+" target = '_blank' >View XML File</a></td></tr>");
								}
								if(Repository.PNRUpdateRequest.contains(searchObject.getTracer()))
								{
									ReportPrinter.append("<tr><td>PNR Update Request</td>"+"<td><a href="+Repository.PNRUpdateRequest+" target = '_blank' >View XML File</a></td></tr>");
								}
								if(Repository.PNRUpdateResponse.contains(searchObject.getTracer()))
								{
									ReportPrinter.append("<tr><td>PNR Update Response</td>"+"<td><a href="+Repository.PNRUpdateResponse+" target = '_blank' >View XML File</a></td></tr>");
								}
								
								ReportPrinter.append("</table>");
								ReportPrinter.append("<br><br><br>");
							}
								
				
						}//End executing test case in Web 
						
						m+=1;
						
					}
						
					catch (Exception e)
					{
						e.printStackTrace();
						TakesScreenshot screen = (TakesScreenshot)driver;
						FileUtils.copyFile(screen.getScreenshotAs(OutputType.FILE),new File("FailedScreenshots/Test Case "+(m-1)+"- Unexpected.jpg"));
						
						ReportPrinter.append("<span><center><p class='Hedding0'>XML Links</p></center></span>");
						ReportPrinter.append("<table style=width:100%>"
						+ "<tr><th>XML Type</th>"
						+ "<th>Link</th></tr>");
						ReportPrinter.append("</table>");
						ReportPrinter.append("<br><br><br>");
					}
					
				}//OBJECT EXECUTION STATUS YES/NO
		
			}//End executing all Search objects (While)
				
		}//End login check if condition
		else
		{
			ReportPrinter.append("<span><center><p class='Hedding0'>LOGIN</p></center></span>");
			ReportPrinter.append("<table style=width:100%>" 
			+ "<tr><td>User Name : "+Propertymap.get( "portal.username")+"</td></tr>"
			+ "<tr><td>Password  : "+Propertymap.get( "portal.password")+"</td></tr>"
			+ "<tr><td class='Failed'>LOGIN FAILED !!</td></tr>");
			ReportPrinter.append("</table>");
			ReportPrinter.append("<br><br><br>");
		}//End login check else condition
		
	}//End @Test
	
	@After
	public void after()
	{
		ReportPrinter.append("</body></html>");
		BufferedWriter bwr;
		try 
		{
			bwr = new BufferedWriter(new FileWriter(new File("Reports/DynamicRateReport.html")));
			bwr.write(ReportPrinter.toString());
			bwr.flush();
			bwr.close();
		} 
		catch (IOException e) 
		{
			e.printStackTrace();
		}
		
		//driver.quit();
	}
	
	
	
	
	
	public ArrayList<SearchObject> getSearchObjList()
	{	
		ArrayList<SearchObject> SearchList = new ArrayList<SearchObject>();
		
		for (int y = 0; y<XLtestData.size()1; y++) 
		{
			Map<Integer, String> Sheet = XLtestData.get(y);
			
			for(int x = 0; x<Sheet.size(); x++)
			{
				String[] testData = Sheet.get(x).split(",");
				
				SearchObject SearchObj = new SearchObject();
				
				SearchObj.setSearchType(testData[0].trim());
				SearchObj.setSellingCurrency(testData[1].trim());
				SearchObj.setCountry(testData[2].trim());
				SearchObj.setTriptype(testData[3].trim());
				SearchObj.setFrom(testData[4].trim());
				SearchObj.setTo(testData[5].trim());
				SearchObj.setDepartureDate(testData[6].trim());
				SearchObj.setDepartureTime(testData[7].trim());
				SearchObj.setReturnDate(testData[8].trim());
				SearchObj.setReturnTime(testData[9].trim());
				SearchObj.setFlexible(Boolean.parseBoolean(testData[10].toLowerCase().trim()));
				SearchObj.setAdult(testData[11].trim());
				SearchObj.setChildren(testData[12].trim());
				
				ArrayList<String> childrenage = new ArrayList<String>();
				String[] age = testData[13].trim().split("/");
				for (int u=0; u<age.length; u++ ) 
				{
					childrenage.add(age[u].trim());
				}
				
				SearchObj.setChildrenAge(childrenage);
				SearchObj.setInfant(testData[14].trim());
				SearchObj.setCabinClass(testData[15].trim());
				SearchObj.setPreferredCurrency(testData[16].trim());
				SearchObj.setPreferredAirline(testData[17].trim());
				SearchObj.setNonStop(Boolean.parseBoolean(testData[18].toLowerCase().trim()));
				SearchObj.setProfitType(testData[19].toLowerCase().trim());
				SearchObj.setProfit(Double.parseDouble(testData[20].trim()));
				SearchObj.setSelectingFlight(testData[21].trim());
				SearchObj.setBookingFeeType(testData[22].trim());
				SearchObj.setBookingFee(Double.parseDouble(testData[23].trim()));
				SearchObj.setExcecuteStatus(testData[24].trim());
				SearchObj.setPaymentMode(testData[25].trim());
				SearchObj.setCancellationStatus(testData[26].trim());
				SearchObj.setSupplierPayablePayStatus(testData[27].trim());
				
				SearchList.add(SearchObj);
			}
		}
		
		return SearchList;
	}
	
	public StringBuffer validateReservationReport(UIConfirmationPage confirmationpage, StringBuffer ReportPrinter, SearchObject searchObject, XMLPriceItinerary XMLSelectFlight)
	{
		String ReservationNo = "";
		ReservationNo = confirmationpage.getReservationNo().trim();
			
		//RESERVATION REPORT VALIDATION
		try
		{
			ReportPrinter.append("<span><center><p class='Hedding0'>Reservation Report Validation</p></center></span>");
			ReportPrinter.append("<table style=width:100%>"
			+ "<tr><th>Test Case</th>"
			+ "<th>Test Description</th>"
			+ "<th>Expected Result</th>"
			+ "<th>Actual Result</th>"
			+ "<th>Test Status</th></tr>");
			
			ReservationReportReader	resvReportReader	= new ReservationReportReader(Propertymap);
			ReservationReport		reservationReport	= new ReservationReport();
			try {
				reservationReport	= resvReportReader.getReport(driver, ReservationNo);
			} catch (Exception e) {
				
			}
			
			ReportPrinter.append("<tr><td>"+Reportcount+"</td>"
			+ "<td>Check Reservation Report availability</td>"
			+ "<td>If Available - true , If Not Available - false</td>");
			
			if(reservationReport.isAvailable())
			{
				ReportPrinter.append("<td>"+reservationReport.isAvailable()+"</td>"
				+ "<td class='Passed'>PASS</td></tr>");
				Reportcount++;	
				
				ReportPrinter	= validate.validateReservationReport(reservationReport, searchObject, XMLSelectFlight, ReportPrinter);
				
				ReportPrinter.append("</table>");
				ReportPrinter.append("<br><br><br>");
			}
			else
			{
				ReportPrinter.append("<td>"+reservationReport.isAvailable()+"</td>"
				+ "<td class='Failed'>Fail</td></tr>");
				ReportPrinter.append("</table>");
				ReportPrinter.append("<br><br><br>");
			}
		}
		catch (Exception e)
		{
			e.printStackTrace();
			ReportPrinter.append("</table>");
			ReportPrinter.append("<br><br><br>");
		}

		return ReportPrinter;
	}
	
	public StringBuffer validateSuppPlayableReport(UIConfirmationPage confirmationpage, StringBuffer ReportPrinter, SearchObject searchObject, ReservationInfo fillingObject, XMLPriceItinerary XMLSelectFlight)
	{
		String ReservationNo = "";
		ReservationNo = confirmationpage.getReservationNo().trim();
			
		//SUPPLIER PAYABLE REPORT VALIDATION
		try
		{
			ReportPrinter.append("<span><center><p class='Hedding0'>Supplier Payable Report Validation</p></center></span>");
			ReportPrinter.append("<table style=width:100%>"
			+ "<tr><th>Test Case</th>"
			+ "<th>Test Description</th>"
			+ "<th>Expected Result</th>"
			+ "<th>Actual Result</th>"
			+ "<th>Test Status</th></tr>");
			
			SupplierPayableReport suppPayableReport = new SupplierPayableReport(Propertymap);
			
			suppPayableReport.getPaybleDetails(driver, Propertymap.get("Flight_Supplier"), ReservationNo);
			SupplierPayableReport suppPayableReportBeforePay = suppPayableReport;
			
			if( searchObject.getSupplierPayablePayStatus().equalsIgnoreCase("YES") )
			{
				suppPayableReport.paySupplier(Propertymap.get("Payment.Reference"));
			}
			
			SupplierPayableReport suppPayableReportAfterPay = suppPayableReport;
			
			ReportPrinter.append("<tr><td>"+Reportcount+"</td>"
			+ "<td>Check Supplier Payable Report availability</td>"
			+ "<td>If Available - true , If Not Available - false</td>");
			
			if(suppPayableReport.isRecordExist())
			{
				ReportPrinter.append("<td>"+suppPayableReport.isRecordExist()+"</td>"
				+ "<td class='Passed'>PASS</td></tr>");
				Reportcount++;	
				
				ReportPrinter = validate.validateSupplierPayableReport(suppPayableReportBeforePay, suppPayableReportAfterPay, searchObject, XMLSelectFlight, confirmationpage, fillingObject, ReportPrinter);
				
				ReportPrinter.append("</table>");
				ReportPrinter.append("<br><br><br>");
			}
			else
			{
				ReportPrinter.append("<td>"+suppPayableReport.isRecordExist()+"</td>"
				+ "<td class='Failed'>Fail</td></tr>");
				ReportPrinter.append("</table>");
				ReportPrinter.append("<br><br><br>");
			}
		}
		catch (Exception e)
		{
			e.printStackTrace();
			ReportPrinter.append("</table>");
			ReportPrinter.append("<br><br><br>");
		}

		return ReportPrinter;
	}
	
	public StringBuffer validateCustomerVoucherEmail(UIConfirmationPage confirmationpage, StringBuffer ReportPrinter, SearchObject searchObject, XMLPriceItinerary XMLSelectFlight, ReservationInfo fillingObject )
	{
		String ReservationNo = "";
		ReservationNo = confirmationpage.getReservationNo().trim();
		//CUSTOMER VOUCHER VALIDATION
		try
		{
			ReportPrinter.append("<span><center><p class='Hedding0'>Customer Voucher E-mail Validation</p></center></span>");
			ReportPrinter.append("<table style=width:100%>"
			+ "<tr><th>Test Case</th>"
			+ "<th>Test Description</th>"
			+ "<th>Expected Result</th>"
			+ "<th>Actual Result</th>"
			+ "<th>Test Status</th></tr>");
			
			CustomerVoucherEmail	email	= new CustomerVoucherEmail(Propertymap);
			email.getCusVoucherEmail(driver, ReservationNo);
			
			ReportPrinter.append("<tr><td>"+Reportcount+"</td>"
			+ "<td>Check Customer Voucher email availability</td>"
			+ "<td>If Available - true , If Not Available - false</td>");
			
			if(email.isAvailable())
			{
				ReportPrinter.append("<td>"+email.isAvailable()+"</td>"
				+ "<td class='Passed'>PASS</td></tr>");
				Reportcount++;
				
				ReportPrinter			= validate.validateCustomerVoucherEmail(email, fillingObject, confirmationpage, XMLSelectFlight, searchObject, ReportPrinter);
				
				ReportPrinter.append("</table>");
				ReportPrinter.append("<br><br><br>");
			}
			else
			{
				ReportPrinter.append("<td>"+email.isAvailable()+"</td>"
				+ "<td class='Failed'>Fail</td></tr>");
				ReportPrinter.append("</table>");
				ReportPrinter.append("<br><br><br>");
			}
		}
		catch (Exception e)
		{
			e.printStackTrace();
			ReportPrinter.append("</table>");
			ReportPrinter.append("<br><br><br>");
		}

		return ReportPrinter;
	}	
	
	public StringBuffer validateBookingConfReport(UIConfirmationPage confirmationpage, SearchObject searchObject, ReservationInfo fillingObject, XMLPriceItinerary XMLSelectFlight, StringBuffer ReportPrinter)
	{
		String ReservationNo = "";
		ReservationNo = confirmationpage.getReservationNo().trim();
		
		try
		{
			ReportPrinter.append("<span><center><p class='Hedding0'>Booking Confirmation Report Validation</p></center></span>");
			ReportPrinter.append("<table style=width:100%>"
			+ "<tr><th>Test Case</th>"
			+ "<th>Test Description</th>"
			+ "<th>Expected Result</th>"
			+ "<th>Actual Result</th>"
			+ "<th>Test Status</th></tr>");
			
			BookingConfReportReader	report	= new BookingConfReportReader(Propertymap);
			report.getReport(driver, ReservationNo);
			
			ReportPrinter.append("<tr><td>"+Reportcount+"</td>"
			+ "<td>Check Booking Confirmation Report availability</td>"
			+ "<td>If Available - true , If Not Available - false</td>");
			if(report.isAvailable())
			{
				ReportPrinter.append("<td>"+report.isAvailable()+"</td>"
				+ "<td class='Passed'>PASS</td></tr>");
				Reportcount++;  
				
				ReportPrinter = validate.validateBookingConfEmail(report, searchObject, fillingObject, XMLSelectFlight,ReportPrinter);
				
				ReportPrinter.append("</table>");
				ReportPrinter.append("<br><br><br>");
			}
			else
			{
				ReportPrinter.append("<td>"+report.isAvailable()+"</td>"
				+ "<td class='Failed'>Fail</td></tr>");
				ReportPrinter.append("</table>");
				ReportPrinter.append("<br><br><br>");
			}
		}
		catch (Exception e)
		{
			e.printStackTrace();
			ReportPrinter.append("</table>");
			ReportPrinter.append("<br><br><br>");
		}
		
		return ReportPrinter;
	}
	
	public StringBuffer validateBookingListReport(UIConfirmationPage confirmationpage, SearchObject searchObject, ReservationInfo fillingObject, XMLPriceItinerary XMLSelectFlight, StringBuffer ReportPrinter)
	{
		String ReservationNo = "";
		ReservationNo = confirmationpage.getReservationNo().trim();
			
		//BOOKING LIST REPORT VALIDATION
		try
		{
			ReportPrinter.append("<span><center><p class='Hedding0'>Booking List Report Validation</p></center></span>");
			ReportPrinter.append("<table style=width:100%>"
			+ "<tr><th>Test Case</th>"
			+ "<th>Test Description</th>"
			+ "<th>Expected Result</th>"
			+ "<th>Actual Result</th>"
			+ "<th>Test Status</th></tr>");
			
			BookingListReport	report	= new BookingListReport(Propertymap);
			report.getReport(driver, ReservationNo);
			
			ReportPrinter.append("<tr><td>"+Reportcount+"</td>"
			+ "<td>Check Customer Voucher email availability</td>"
			+ "<td>If Available - true , If Not Available - false</td>");
			if(report.isIsreportLoaded())
			{
				ReportPrinter.append("<td>"+report.isIsreportLoaded()+"</td>"
				+ "<td class='Passed'>PASS</td></tr>");
				Reportcount++;
				
				ReportPrinter = validate.validateBookingListReport(report, fillingObject, XMLSelectFlight, searchObject, confirmationpage, ReportPrinter);
				
				ReportPrinter.append("</table>");
				ReportPrinter.append("<br><br><br>");
			}
			else
			{
				ReportPrinter.append("<td>"+report.isIsreportLoaded()+"</td>"
				+ "<td class='Failed'>Fail</td></tr>");
				ReportPrinter.append("</table>");
				ReportPrinter.append("<br><br><br>");
			}
		}
		catch (Exception e)
		{
			//System.out.println();
			e.printStackTrace();
			ReportPrinter.append("</table>");
			ReportPrinter.append("<br><br><br>");
		}
		return ReportPrinter;
	}
	
	public StringBuffer validateProfitLossReport(UIConfirmationPage confirmationpage, SearchObject searchObject, ReservationInfo fillingObject, XMLPriceItinerary XMLSelectFlight, StringBuffer ReportPrinter)
	{
		String ReservationNo = "";
		ReservationNo = confirmationpage.getReservationNo().trim();
			
		//Profit and Loss Report VALIDATION
		try
		{
			ProfitAndLoss report	= new ProfitAndLoss(Propertymap);
			try {
				report.loadReport(driver, ReservationNo);
			} catch (Exception e) {
				
			}
			
			
			ReportPrinter.append("<span><center><p class='Hedding0'>Profit and Loss Validation</p></center></span>");
			ReportPrinter.append("<table style=width:100%>"
			+ "<tr><th>Test Case</th>"
			+ "<th>Test Description</th>"
			+ "<th>Expected Result</th>"
			+ "<th>Actual Result</th>"
			+ "<th>Test Status</th></tr>");
			ReportPrinter.append("<tr><td>"+Reportcount+"</td>"
			+ "<td>Check profit and loss report availability</td>"
			+ "<td>If Available - true , If Not Available - false</td>");
			if(report.isReportLoaded())
			{
				ReportPrinter.append("<td>"+report.isReportLoaded()+"</td>"
				+ "<td class='Passed'>PASS</td></tr>");
				Reportcount++;
				
				ReportPrinter = validate.validateProfitAndLoss(XMLSelectFlight, searchObject, report, ReportPrinter);
				
				ReportPrinter.append("</table>");
				ReportPrinter.append("<br><br><br>");
			}
			else
			{
				ReportPrinter.append("<td>"+report.isReportLoaded()+"</td>"
				+ "<td class='Failed'>Fail</td></tr>");
				ReportPrinter.append("</table>");
				ReportPrinter.append("<br><br><br>");
			}
		}
		catch (Exception e)
		{
			e.printStackTrace();
			ReportPrinter.append("</table>");
			ReportPrinter.append("<br><br><br>");
		}
		return ReportPrinter;
	}
	
	public StringBuffer doCancellation(CancellationScreen cancellationScreen, CancellationReport cancellationReport, UIConfirmationPage confirmationpage, SearchObject searchObject, ReservationInfo fillingObject, XMLPriceItinerary XMLSelectFlight, StringBuffer ReportPrinter)
	{
		//======================================CANCELLATION SCREEN VALIDATION=====================================
		
		try {
			
			try {
				cancellationScreen.getCancellationScreen(driver, confirmationpage.getReservationNo(), XMLSelectFlight, searchObject);
			} catch (Exception e) {
				
			}
			ReportPrinter.append("<span><center><p class='Hedding0'>Cancellation Screen Validation</p></center></span>");
			ReportPrinter.append("<table style=width:100%>"
			+ "<tr><th>Test Case</th>"
			+ "<th>Test Description</th>"
			+ "<th>Expected Result</th>"
			+ "<th>Actual Result</th>"
			+ "<th>Test Status</th></tr>");
			ReportPrinter.append("<tr><td>"+Reportcount+"</td>"
			+ "<td>Check Cancellation Screen Availability</td>"
			+ "<td>If Available - true , If Not Available - false</td>");
			if(!cancellationScreen.isNotFound())
			{
				ReportPrinter.append("<td>"+cancellationScreen.isNotFound()+"</td>"
				+ "<td class = 'Passed'>PASS</td></tr>" );
				Reportcount++;
				
				try {
					ReportPrinter = validate.validateCancellationScreen(XMLSelectFlight, searchObject, fillingObject, confirmationpage, cancellationScreen, ReportPrinter);
				} catch (Exception e) {
					
				}
				
				ReportPrinter.append("<tr><td>"+Reportcount+"</td>"
				+ "<td>Check Cancellation Status</td>"
				+ "<td>If Cancellation Done - true and Display Message , If Not Done - false and Display Error Message</td>");
				if(cancellationScreen.isCancelled())
				{
					ReportPrinter.append("<td>"+cancellationScreen.isCancelled()+"<br>"+cancellationScreen.getCancellationMessage()+"</td>"
					+ "<td class = 'Passed'>PASS</td></tr>" );
					Reportcount++;
				}
				else
				{
					ReportPrinter.append("<td>"+cancellationScreen.isCancelled()+"<br>"+cancellationScreen.getCancellationMessage()+"</td>"
					+ "<td class = 'Failed'>Fail</td></tr>");
					Reportcount++;
				}
				
				ReportPrinter.append("</table>");
				ReportPrinter.append("<br><br><br>");	
			}
			else
			{
				ReportPrinter.append("<td>"+cancellationScreen.isNotFound()+"</td>"
				+ "<td class = 'Failed'>Fail</td></tr>");
				Reportcount++;
				ReportPrinter.append("</table>");
				ReportPrinter.append("<br><br><br>");
			}
			
			if( !cancellationScreen.isNotFound() && cancellationScreen.isCancelled() )
			{
				
				try {
					cancellationReport.loadReport(driver, confirmationpage.getReservationNo());
				} catch (Exception e) {
						
				}
					
				ReportPrinter.append("<span><center><p class='Hedding0'>Cancellation Report Validation</p></center></span>");
				ReportPrinter.append("<table style=width:100%>"
				+ "<tr><th>Test Case</th>"
				+ "<th>Test Description</th>"
				+ "<th>Expected Result</th>"
				+ "<th>Actual Result</th>"
				+ "<th>Test Status</th></tr>");
				ReportPrinter.append("<tr><td>"+Reportcount+"</td>"
				+ "<td>Check Cancellation Report Availability</td>"
				+ "<td>If Available - true , If Not Available - false</td>");
				if(cancellationReport.isReportLoded())
				{
					ReportPrinter.append("<td>"+cancellationReport.isReportLoded()+"</td>"
					+ "<td class='Passed'>PASS</td></tr>");
					Reportcount++;
					try {
						ReportPrinter = validate.validateCancellationReport(XMLSelectFlight, searchObject, fillingObject, confirmationpage, cancellationScreen, cancellationReport, ReportPrinter);
					} catch (Exception e) {
						
					}
					ReportPrinter.append("</table>");
					ReportPrinter.append("<br><br><br>");
					
					
					try {
						
						CancellationRequest cancellationRequest = new CancellationRequest();
						CancellationRequestReader cancellationReqReader = new CancellationRequestReader(Propertymap);
						ReportPrinter.append("<span><center><p class='Hedding0'>Cancellation Request Validation</p></center></span>");
						ReportPrinter.append("<table style=width:100%>"
						+ "<tr><th>Test Case</th>"
						+ "<th>Test Description</th>"
						+ "<th>Expected Result</th>"
						+ "<th>Actual Result</th>"
						+ "<th>Test Status</th></tr>");
						cancellationRequest = cancellationReqReader.RequestReader(XMLLocateType.TRACER, searchObject.getTracer()confirmationpage.getSupplierConfirmationNo().trim(), XMLFileType.AIR1_Cancellationrequest_);
						ReportPrinter = validate.validateCancellationRequest(cancellationRequest, confirmationpage, ReportPrinter);
						ReportPrinter.append("</table>");
						ReportPrinter.append("<br><br><br>");
					} catch (Exception e) {
						
					}
					ReportPrinter.append("</table>");
					ReportPrinter.append("<br><br><br>");
					
					try {
						
						CancellationResponse cancellationResponse = new CancellationResponse();
						CancellationResponseReader cancellationResReader = new CancellationResponseReader(Propertymap);
						ReportPrinter.append("<span><center><p class='Hedding0'>Cancellation Response Validation</p></center></span>");
						ReportPrinter.append("<table style=width:100%>"
						+ "<tr><th>Test Case</th>"
						+ "<th>Test Description</th>"
						+ "<th>Expected Result</th>"
						+ "<th>Actual Result</th>"
						+ "<th>Test Status</th></tr>");
						cancellationResponse = cancellationResReader.RequestReader(XMLLocateType.TRACER, searchObject.getTracer()confirmationpage.getSupplierConfirmationNo().trim(), XMLFileType.AIR1_Cancellationrequest_);
						ReportPrinter = validate.validateCancellationResponse(cancellationResponse, confirmationpage, cancellationReport, ReportPrinter);
						ReportPrinter.append("</table>");
						ReportPrinter.append("<br><br><br>");
					} catch (Exception e) {
						
					}
					ReportPrinter.append("</table>");
					ReportPrinter.append("<br><br><br>");
					
				}
				else
				{
					ReportPrinter.append("<td>"+cancellationReport.isReportLoded()+"</td>"
					+ "<td class='Failed'>Fail</td></tr>");
					Reportcount++;
					ReportPrinter.append("</table>");
					ReportPrinter.append("<br><br><br>");
				}
			}
			
		} catch (Exception e) {
			ReportPrinter.append("</table>");
			ReportPrinter.append("<br><br><br>");
		}
		
		return ReportPrinter;
	}
	
	public StringBuffer validateProfitLossReportAfterCancel(CancellationScreen cancellationScreen, CancellationReport cancellationReport, UIConfirmationPage confirmationpage, SearchObject searchObject, ReservationInfo fillingObject, XMLPriceItinerary XMLSelectFlight, StringBuffer ReportPrinter)
	{
		String ReservationNo = "";
		ReservationNo = confirmationpage.getReservationNo().trim().concat("-C");
			
		//Profit and Loss Report VALIDATION
		try
		{
			ProfitAndLoss profitlossReport	= new ProfitAndLoss(Propertymap);
			profitlossReport.loadReport(driver, ReservationNo);
			//System.out.println();
			ReportPrinter.append("<span><center><p class='Hedding0'>Profit and Loss Validation After Cancellation</p></center></span>");
			ReportPrinter.append("<table style=width:100%>"
			+ "<tr><th>Test Case</th>"
			+ "<th>Test Description</th>"
			+ "<th>Expected Result</th>"
			+ "<th>Actual Result</th>"
			+ "<th>Test Status</th></tr>");
			ReportPrinter.append("<tr><td>"+Reportcount+"</td>"
			+ "<td>Check profit and loss report availability</td>"
			+ "<td>If Available - true , If Not Available - false</td>");
			if(profitlossReport.isReportLoaded())
			{
				ReportPrinter.append("<td>"+profitlossReport.isReportLoaded()+"</td>"
				+ "<td class='Passed'>PASS</td></tr>");
				Reportcount++;
				
				ReportPrinter = validate.validateProfitAndLossAfterCancellation(profitlossReport, cancellationScreen, cancellationReport, XMLSelectFlight, searchObject, ReportPrinter);
				//System.out.println();
				ReportPrinter.append("</table>");
				ReportPrinter.append("<br><br><br>");
			}
			else
			{
				ReportPrinter.append("<td>"+profitlossReport.isReportLoaded()+"</td>"
				+ "<td class='Failed'>Fail</td></tr>");
				ReportPrinter.append("</table>");
				ReportPrinter.append("<br><br><br>");
			}
		}
		catch (Exception e)
		{
			e.printStackTrace();
			ReportPrinter.append("</table>");
			ReportPrinter.append("<br><br><br>");
		}
		return ReportPrinter;
	}
	
	public StringBuffer validateThirdPartySuppPayableAfterCancel(UIConfirmationPage uiconfirmationpage, SearchObject searchObject, XMLPriceItinerary XMLSelectFlight, StringBuffer ReportPrinter)
	{
		String ReservationNo = "";
		ReservationNo = uiconfirmationpage.getReservationNo().trim();
			
		//SUPPLIER PAYABLE REPORT VALIDATION
		try
		{
			ReportPrinter.append("<span><center><p class='Hedding0'>Supplier Payable Report Validation After Cancellation</p></center></span>");
			ReportPrinter.append("<table style=width:100%>"
			+ "<tr><th>Test Case</th>"
			+ "<th>Test Description</th>"
			+ "<th>Expected Result</th>"
			+ "<th>Actual Result</th>"
			+ "<th>Test Status</th></tr>");
			
			SupplierPayableReport suppPayableReport = new SupplierPayableReport(Propertymap);
			
			suppPayableReport.getPaybleDetails(driver, Propertymap.get("Flight_Supplier"), ReservationNo);
			SupplierPayableReport suppPayableReportBeforePay = suppPayableReport;
			
			if( searchObject.getSupplierPayablePayStatus().equalsIgnoreCase("YES") )
			{
				suppPayableReport.paySupplier(Propertymap.get("Payment.Reference"));
				//SupplierPayableReport suppPayableReportAfterPay = suppPayableReport;
			}
			
			ReportPrinter.append("<tr><td>"+Reportcount+"</td>"
			+ "<td>Check Supplier Payable Report availability</td>"
			+ "<td>If Available - true , If Not Available - false</td>");
			
			if(suppPayableReport.isRecordExist())
			{
				ReportPrinter.append("<td>"+suppPayableReport.isRecordExist()+"</td>"
				+ "<td class='Passed'>PASS</td></tr>");
				Reportcount++;	
				
				ReportPrinter = validate.validateSupplierPayableAfterCancellation(uiconfirmationpage, suppPayableReportBeforePay, XMLSelectFlight, ReportPrinter);
				
				ReportPrinter.append("</table>");
				ReportPrinter.append("<br><br><br>");
			}
			else
			{
				ReportPrinter.append("<td>"+suppPayableReport.isRecordExist()+"</td>"
				+ "<td class='Failed'>Fail</td></tr>");
				ReportPrinter.append("</table>");
				ReportPrinter.append("<br><br><br>");
			}
		}
		catch (Exception e)
		{
			//e.printStackTrace();
			ReportPrinter.append("</table>");
			ReportPrinter.append("<br><br><br>");
		}

		return ReportPrinter;
	}
	
	public StringBuffer validateReservationReportAfterCancel(UIConfirmationPage confirmationpage, SearchObject searchObject, XMLPriceItinerary XMLSelectFlight, StringBuffer ReportPrinter)
	{
		String ReservationNo = "";
		ReservationNo = confirmationpage.getReservationNo().trim();
			
		//RESERVATION REPORT VALIDATION
		try
		{
			ReportPrinter.append("<span><center><p class='Hedding0'>Reservation Report Validation After Cancellation</p></center></span>");
			ReportPrinter.append("<table style=width:100%>"
			+ "<tr><th>Test Case</th>"
			+ "<th>Test Description</th>"
			+ "<th>Expected Result</th>"
			+ "<th>Actual Result</th>"
			+ "<th>Test Status</th></tr>");
			
			ReservationReportReader	resvReportReader	= new ReservationReportReader(Propertymap);
			ReservationReport		reservationReport	= new ReservationReport();
			try {
				reservationReport	= resvReportReader.getReport(driver, ReservationNo);
			} catch (Exception e) {
				System.out.println();
			}
			
			ReportPrinter.append("<tr><td>"+Reportcount+"</td>"
			+ "<td>Reservation should not exist in report after cancellation</td>"
			+ "<td>If Available - true , If Not Available - false</td>");
			
			if(!reservationReport.isAvailable())
			{
				ReportPrinter.append("<td>"+reservationReport.isAvailable()+"</td>"
				+ "<td class='Passed'>PASS</td></tr>");
				Reportcount++;	
				ReportPrinter.append("</table>");
				ReportPrinter.append("<br><br><br>");
			}
			else
			{
				ReportPrinter.append("<td>"+reservationReport.isAvailable()+"</td>"
				+ "<td class='Failed'>Fail</td></tr>");
				ReportPrinter.append("</table>");
				ReportPrinter.append("<br><br><br>");
			}
		}
		catch (Exception e)
		{
			e.printStackTrace();
			ReportPrinter.append("</table>");
			ReportPrinter.append("<br><br><br>");
		}

		return ReportPrinter;
		
	}
	
	public StringBuffer validateBookingListReportAfterCancel(UIConfirmationPage confirmationpage, SearchObject searchObject, ReservationInfo fillingObject, XMLPriceItinerary XMLSelectFlight, StringBuffer ReportPrinter)
	{
		String ReservationNo = "";
		ReservationNo = confirmationpage.getReservationNo().trim();
			
		//BOOKING LIST REPORT VALIDATION
		try
		{
			ReportPrinter.append("<span><center><p class='Hedding0'>Booking List Report Validation After Cancellation</p></center></span>");
			ReportPrinter.append("<table style=width:100%>"
			+ "<tr><th>Test Case</th>"
			+ "<th>Test Description</th>"
			+ "<th>Expected Result</th>"
			+ "<th>Actual Result</th>"
			+ "<th>Test Status</th></tr>");
			
			BookingListReport	report	= new BookingListReport(Propertymap);
			report.getReport(driver, ReservationNo);
			
			ReportPrinter.append("<tr><td>"+Reportcount+"</td>"
			+ "<td>Check Customer Voucher email availability</td>"
			+ "<td>If Available - true , If Not Available - false</td>");
			if(report.isIsreportLoaded())
			{
				ReportPrinter.append("<td>"+report.isIsreportLoaded()+"</td>"
				+ "<td class='Passed'>PASS</td></tr>");
				Reportcount++;
				System.out.println();
				ReportPrinter = validate.validateBookingListReportAfterCancellation(report, fillingObject, XMLSelectFlight, searchObject, confirmationpage, ReportPrinter);
				
				ReportPrinter.append("</table>");
				ReportPrinter.append("<br><br><br>");
			}
			else
			{
				ReportPrinter.append("<td>"+report.isIsreportLoaded()+"</td>"
				+ "<td class='Failed'>Fail</td></tr>");
				ReportPrinter.append("</table>");
				ReportPrinter.append("<br><br><br>");
			}
		}
		catch (Exception e)
		{
			//System.out.println();
			//e.printStackTrace();
			ReportPrinter.append("</table>");
			ReportPrinter.append("<br><br><br>");
		}
		return ReportPrinter;
	}
	
	
}*/