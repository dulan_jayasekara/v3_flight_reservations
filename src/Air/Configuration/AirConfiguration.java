package Air.Configuration;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import org.openqa.selenium.Alert;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

/*import system.enumtypes.ConfigFareType;*/
import Flight.TypeEnum.*;

import com.utilities.ExcelReader;

import Flight.Classes.AirConfig;


public class AirConfiguration 
{
	static HashMap<String, String> propertymap	= new HashMap<String, String>();
	ArrayList<Map<Integer,String>> returnlist	= null;
	String airline								= "";
	ExcelReader xl								= new ExcelReader();
	boolean found								= false;
	
	
	public AirConfiguration(HashMap<String, String> propmap)
	{
		propertymap = propmap;
		returnlist = xl.init(propertymap.get("AirConfigXL_Path"));
	}
	
	public WebDriver setConfiguration(WebDriver driver, AirConfig configObj )
	{
		driver.get(propertymap.get("Portal.Url").concat("/air/setup/AirConfigurationSetupPage.do?module=contract"));
		
		try {
			Alert alert = driver.switchTo().alert();
			alert.accept();
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		driver.findElement(By.id("airLineName")).sendKeys(configObj.getAirLine().trim());
		driver.findElement(By.id("airLineName_lkup")).click();
		try
		{
			driver.switchTo().frame("lookup");
			airline = driver.findElement(By.className("rezgLook0")).getText().trim();
			if(configObj.getAirLine().trim().equalsIgnoreCase(airline))
			{
				found = true;
				driver.findElement(By.id("lookupDataArea")).findElement(By.className("rezgLook0")).click();
			}
			driver.switchTo().defaultContent();
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		if(found)
		{
			boolean set = false;
			
			while(!set)
			{
				
			
				if( !configObj.getPubFareType().equals("") )
				{
					ArrayList<WebElement> publishedRadio = new ArrayList<WebElement>(driver.findElements(By.id("publishedFareOption")));
					
					if( configObj.getPubFareType().equals(ConfigFareType.Booking_Fee) )
					{
						if( !publishedRadio.get(0).isSelected() )
						{	
							publishedRadio.get(0).click();
						}
					}
					else if( configObj.getPubFareType().equals(ConfigFareType.Profit_Markup) )
					{
						if( !publishedRadio.get(1).isSelected() )
						{
							publishedRadio.get(1).click();
						}
					}
					else if( configObj.getPubFareType().equals(ConfigFareType.Both) )
					{
						if( !publishedRadio.get(2).isSelected() )
						{
							publishedRadio.get(2).click();
						}
					}
					
					else if( configObj.getPubFareType().equals(ConfigFareType.None) )
					{
						if( !publishedRadio.get(3).isSelected() )
						{
							publishedRadio.get(3).click();
						}
					}
					
					driver.findElement(By.id("addAirConfigurationMapping")).click();
					
					if(driver.findElement(By.id("dialogMsgText")).isDisplayed())
					{
						((JavascriptExecutor)driver).executeScript("javascript:closeDialogMsg(dialogMsgBox);");
						set = false;
						ArrayList<WebElement> table = new ArrayList<WebElement>(driver.findElements(By.xpath(".//*[@id='air_configuration_mapping_data']/table/tbody")));
						ArrayList<WebElement> tabletr = new ArrayList<WebElement>(table.get(0).findElements(By.className("reportDataRows1")));
						
						for(int y=0; y<tabletr.size(); y++)
						{
							String text = tabletr.get(y).findElements(By.className("tablegridcell")).get(0).getText();
							if(configObj.getAirLine().trim().equalsIgnoreCase(text))
							{
								((JavascriptExecutor)driver).executeScript("removeAirConfigurationMapping("+(y+1)+")");
								break;
							}
						}
					}
					else
					{
						set = true;
					}
				}
			
			}
			
			
			if( !configObj.getPvtFareType().equals("") )
			{
				ArrayList<WebElement> privateRadio = new ArrayList<WebElement>(driver.findElements(By.id("privateFareOption")) );
				
				if(configObj.getPvtFareType().equals(ConfigFareType.Booking_Fee))
				{
					if( !privateRadio.get(0).isSelected())
					{
						privateRadio.get(0).click();
					}
				}
				
				else if( configObj.getPvtFareType().equals(ConfigFareType.Profit_Markup) )
				{
					System.out.println();
					if( !privateRadio.get(1).isSelected() )
					{
						privateRadio.get(1).click();
					}
				}
				
				else if( configObj.getPvtFareType().equals(ConfigFareType.Both) )
				{
					if( !privateRadio.get(2).isSelected())
					{
						privateRadio.get(2).click();
					}
				}
				else
				{
					System.out.println("WRONG INPUT - PvtFareType");
				}
			}
			
			if(!configObj.getFTypeConfgShopCart().equals(""))
			{
				if(configObj.getFTypeConfgShopCart().equals("Published_Fares_Only"))
				{
					driver.findElement(By.id("fareTypeShoppingCart_PUB")).click();
				}
				else
				{
					System.out.println("WRONG INPUT - FTypeConfgShopCart");
				}
			}
			
			
			/*if( !configObj.getFTypeConfgFplusH().equals("") )
			{
				if(configObj.getFTypeConfgFplusH().equalsIgnoreCase("Private_Fares_Only"))
				{
					driver.findElement(By.id("fareTypeVacation_PRO")).click();
				}
				else if(configObj.getFTypeConfgFplusH().equalsIgnoreCase("Private_Fares_and_if_not_available_then_Published_Fares"))
				{
					driver.findElement(By.id("fareTypeVacation_PRP")).click();
				}
				else
				{
					System.out.println("WRONG INPUT - FTypeConfgFplusH");
				}
			}
			
			if( !configObj.getFTypeConfgFixPack().equals("") )
			{
				if(configObj.getFTypeConfgFixPack().equalsIgnoreCase("Private_Fares_Only"))
				{
					driver.findElement(By.id("fareTypeFixPackage_PRO")).click();
				}
				else if(configObj.getFTypeConfgFixPack().equalsIgnoreCase("Private_Fares_and_if_not_available_then_Published_Fares"))
				{
					driver.findElement(By.id("fareTypeFixPackage_PRP")).click();
				}
				else
				{
					System.out.println("WRONG INPUT - FTypeConfgFixPack");
				}
			}*/
			
			if( !configObj.getFlightPayOptCartBooking().equals("") )
			{
				if(configObj.getFlightPayOptCartBooking().equalsIgnoreCase("Pay_Full_Amount_at_Booking"))
				{
					driver.findElement(By.id("payementOptionShoppingCart_PF")).click();
				}
				else if(configObj.getFlightPayOptCartBooking().equalsIgnoreCase("Pay_Booking_Fee_as_Deposit"))
				{
					driver.findElement(By.id("payementOptionShoppingCart_PB")).click();
				}
				else if(configObj.getFlightPayOptCartBooking().equalsIgnoreCase("Pass_to_Air_Line_to_Charge"))
				{
					driver.findElement(By.id("payementOptionShoppingCart_AC")).click();
				}
				else
				{
					System.out.println("WRONG INPUT - FlightPayOptCartBooking");
				}
			}
			
			/*if(!configObj.getFlightPayOptPayfullFplusH().equals(""))
			{
				if(configObj.getFlightPayOptPayfullFplusH().equals("Pay_Full_Amount_at_Booking"))
				{
					driver.findElement(By.id("payementOptionVacation_PF")).click();
				}
				else
				{
					System.out.println("WRONG INPUT - FlightPayOptPayfullFplusH");
				}
			}*/
	
		}//End if(found)
		
		driver.findElement(By.id("saveButId")).click();
		
		if(driver.findElement(By.id("dialogMsgBox")).isDisplayed())
		{
			driver.findElement(By.id("dialogMsgActionButtons")).click();
		}
		return driver;
	}
	
	
	
	public ArrayList<AirConfig> getConfigurationList(/* ArrayList<Map<Integer, String>> list */)
	{
		ArrayList<AirConfig> configObjList = new ArrayList<AirConfig>();
		
		for(int i=0; i<returnlist.size(); i++)
		{
			Map<Integer, String> sheet = returnlist.get(i);
			
			for(int j=0; j<sheet.size(); j++)
			{
				String[] data = sheet.get(j).split(",");
				
				AirConfig obj = new AirConfig();
				
				obj.setTestNo(Integer.parseInt(data[0]));
				obj.setAirLine(data[1]);
				
				if(data[2].equals(ConfigFareType.Booking_Fee.toString()))
				{
					obj.setPubFareType(ConfigFareType.Booking_Fee);
				}
				else if(data[2].equals(ConfigFareType.Profit_Markup.toString()))
				{
					obj.setPubFareType(ConfigFareType.Profit_Markup);
				}
				else if(data[2].equals(ConfigFareType.Both.toString()))
				{
					obj.setPubFareType(ConfigFareType.Both);
				}
				else if(data[2].equals(ConfigFareType.None.toString()))
				{
					obj.setPubFareType(ConfigFareType.None);
				}
				
				if(data[3].equals(ConfigFareType.Booking_Fee.toString()))
				{
					obj.setPvtFareType(ConfigFareType.Booking_Fee);
				}
				else if(data[3].equals(ConfigFareType.Profit_Markup.toString()))
				{
					obj.setPvtFareType(ConfigFareType.Profit_Markup);
				}
				else if(data[3].equals(ConfigFareType.Both.toString()))
				{
					obj.setPvtFareType(ConfigFareType.Both);
				}

				obj.setFTypeConfgShopCart(data[4]);
				obj.setFTypeConfgFplusH(data[5]);
				obj.setFTypeConfgFixPack(data[6]);
				obj.setFlightPayOptCartBooking(data[7]);
				obj.setFlightPayOptPayfullFplusH(data[8]);
			
				configObjList.add(obj);
			}
		}
		
		return configObjList;
	}

}
