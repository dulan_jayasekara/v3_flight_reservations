package Results.Validate;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;






import org.apache.commons.io.FileUtils;
import org.openqa.selenium.By;




//import org.openqa.selenium.By;
import java.util.Iterator;


//import java.util.concurrent.TimeUnit;



//import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebDriverException;
import org.openqa.selenium.WebElement;
//import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;



//import org.openqa.selenium.support.ui.WebDriverWait;


import com.common.Validators.CommonValidator;




//import system.classes.Flight;
//import system.classes.Inbound;
//import system.classes.Outbound;
import Flight.Classes.*;

public class ValidateFilters {

	HashMap<String, String>					PropertyMap		= new HashMap<String, String>();
	//private ArrayList<UIFlightItinerary>	Resultlist		= new ArrayList<UIFlightItinerary>();
	//private HashMap<String, String>			propertyMap		= new HashMap<String, String>();
	private boolean							twoway			= false;
	//private ArrayList<WebElement>			outboundRows	= new ArrayList<WebElement>();
	//private ArrayList<WebElement>			inboundRows		= new ArrayList<WebElement>();
	//private ArrayList<WebElement>			flights			= new ArrayList<WebElement>();
	boolean									done			= false;
	
	public ValidateFilters(HashMap<String, String> propertymap)
	{
		PropertyMap = propertymap;
	}
	
	
	public ArrayList<UIFlightItinerary> getResults(WebDriver driver, String trip) throws WebDriverException, IOException
	{
		@SuppressWarnings("unused")
		boolean twoway = false;
		if(trip.equalsIgnoreCase("Round Trip"))
		{
			twoway = true;
		}
		else if(trip.equalsIgnoreCase("One Way Trip"))
		{
			twoway = false;
		}
		
		ArrayList<UIFlightItinerary> Resultlist = new ArrayList<UIFlightItinerary>(); 
		int nflights;
		driver.switchTo().defaultContent();
		driver.switchTo().frame("live_message_frame");
		String numofflights = driver.findElement(By.xpath("/html/body/div[7]/div[3]/div[1]/div/div[3]/div/div/div/div[1]/div/div[1]")).getText();
		int navcount1 = 0 ;
		int navcount2 = 0;
		try
		{
			String[] hotels = numofflights.split(" ");
		
			nflights = Integer.parseInt(hotels[6]);//Get no of flights
			navcount1 = nflights/10;//Per page 10 find how many pages to navigate
			navcount2 = nflights%10;//Remaining flights
		}
		catch(Exception e)
		{
			e.printStackTrace();
			TakesScreenshot screen = (TakesScreenshot)driver;
			FileUtils.copyFile(screen.getScreenshotAs(OutputType.FILE),new File("FailedScreenshots/"+System.currentTimeMillis()/1000+".jpg"));
		}
		
		int navigate = navcount1;
		
		if((navcount2>0)&&(navcount2<10))
		{
			navigate+=1;	
		}
		
		int clickpattern = 2; 
		int count = 0;
		
		
		ArrayList<WebElement> flights = null;
		
		/*if(Integer.parseInt(PropertyMap.get("Flight_PageIterations")) <= navigate)
			navigate  = Integer.parseInt(PropertyMap.get("Flight_PageIterations"));
		*/
		@SuppressWarnings("unused")
		int counter = 0;
		while(count<navigate)
		{
			try
			{	
				flights = new ArrayList<WebElement>(driver.findElements(By.xpath("/html/body/div[7]/div[3]/div[1]/div/div[3]/div/div/div/div[3]/div[2]/table")));
				Iterator<WebElement> flightsIter = flights.iterator();
				
				while(flightsIter.hasNext())
				{	
					WebElement flight = flightsIter.next();
					UIFlightItinerary flightItinearary = new UIFlightItinerary();
					
					
					ArrayList<WebElement> Topinner1 = (ArrayList<WebElement>) flight.findElements(By.tagName("tr"));
					ArrayList<WebElement> Topinner1ele = (ArrayList<WebElement>) Topinner1.get(0).findElements(By.tagName("th"));
					
					String currCode 	= "";
					//currCode			= Topinner1ele.get(0).findElement(By.id("rate_"+counter+"")).getText().trim().split(" ")[0];
					currCode			= Topinner1ele.get(0).findElement(By.xpath("/html/body/div[7]/div[3]/div[1]/div/div[3]/div/div/div/div[3]/div[2]/table[1]/tbody/tr[1]/th[1]")).getText().trim().split(" ")[0];
					flightItinearary.setCurrencyCode(currCode);
					
					String cost			= "";
					//cost				= Topinner1ele.get(0).findElement(By.id("rate_"+counter+"")).getText().trim().split(" ")[1];
					cost				= Topinner1ele.get(0).findElement(By.xpath("/html/body/div[7]/div[3]/div[1]/div/div[3]/div/div/div/div[3]/div[2]/table[1]/tbody/tr[1]/th[1]")).getText().trim().split(" ")[1];
					flightItinearary.setCost(cost);
					
					String departing	= "";
					departing			= Topinner1ele.get(1).getText().trim().replace("Departing", "").trim();
					departing			= Topinner1ele.get(1).getText().trim().replace("Departing", "").trim();///html/body/div[7]/div[3]/div[1]/div/div[3]/div/div/div/div[3]/div[2]/table[1]/tbody/tr[1]/th[2]
					flightItinearary.setDeparting(departing);
					
					ArrayList<WebElement> trlist		= (ArrayList<WebElement>)Topinner1.get(1).findElements(By.tagName("tr"));
					ArrayList<WebElement> trlistrowbold	= new ArrayList<WebElement>();
					ArrayList<WebElement> trlistrowalt	= new ArrayList<WebElement>();
					ArrayList<WebElement> moreLinkTR	= new ArrayList<WebElement>();
					
					if(trlist.size() == 7)
					{
						trlistrowbold.add(trlist.get(0));
						trlistrowbold.add(trlist.get(3));
						
						trlistrowalt.add(trlist.get(1));
						trlistrowalt.add(trlist.get(2));
						trlistrowalt.add(trlist.get(4));
						trlistrowalt.add(trlist.get(5));
						
						moreLinkTR.add(trlist.get(6));
					}
					else if(trlist.size() == 4)
					{
						trlistrowbold.add(trlist.get(0));
						
						trlistrowalt.add(trlist.get(1));
						trlistrowalt.add(trlist.get(2));
						
						moreLinkTR.add(trlist.get(3));
					}
					
					ArrayList<WebElement> trlistrowboldLeavetop = (ArrayList<WebElement>) trlistrowbold.get(0).findElements(By.tagName("td"));
					
					String leaving		= "";
					leaving				= trlistrowboldLeavetop.get(0).getText().trim();
					flightItinearary.setLeaving(leaving);	
					
					String leaveFlight	= "";
					leaveFlight			= trlistrowboldLeavetop.get(1).getText().trim();
					flightItinearary.setLeaveFlight(leaveFlight);
					
					String leaveDuration = "";
					leaveDuration		 = trlistrowboldLeavetop.get(2).getText().trim();
					flightItinearary.setLeaveduration(leaveDuration);
					
					
					ArrayList<WebElement> trlistrowboldReturntop = null;
					ArrayList<WebElement> trlistrowaltLeave = (ArrayList<WebElement>) trlistrowalt.get(0).findElements(By.tagName("td"));
					
					String LDTime	= "";
					LDTime			= trlistrowaltLeave.get(1).getText().trim().split("\\n")[0];
					flightItinearary.setLeaveDepartTime(LDTime);
					
					String LATime	= "";
					LATime			= trlistrowaltLeave.get(1).getText().trim().split("\\n")[1];
					flightItinearary.setLeaveArriveTime(LATime);
					
					String LDFrom	= "";
					LDFrom			= trlistrowaltLeave.get(2).getText().trim().split("\\n")[0];
					flightItinearary.setLeaveDepartFrom(LDFrom);
					
					String LATo		= "";
					LATo			= trlistrowaltLeave.get(2).getText().trim().split("\\n")[1];
					flightItinearary.setLeaveArriveTo(LATo);
					
					String DFLeeave	= "";
					DFLeeave		= trlistrowaltLeave.get(3).getText().trim();
					flightItinearary.setDirectFlightLeave(DFLeeave);
					
					String cabin	= "";
					cabin			= trlistrowalt.get(1).findElements(By.tagName("td")).get(0).getText();
					flightItinearary.setCabintype(cabin);
					
					ArrayList<WebElement> trlistrowaltReturn = null;
					if(trip.equals("Round Trip"))
					{
						twoway = true;
						trlistrowboldReturntop = (ArrayList<WebElement>) trlistrowbold.get(1).findElements(By.tagName("td"));
						
						String returning	= "";
						returning			= trlistrowboldReturntop.get(0).getText().trim().replace("Return", "").trim();
						flightItinearary.setReturning(returning);	
						
						String returnFlight = "";
						returnFlight		= trlistrowboldReturntop.get(1).getText().trim();
						flightItinearary.setReturnFlight(returnFlight);
						
						String returnDuration	= "";
						returnDuration			= trlistrowboldReturntop.get(2).getText().trim();
						flightItinearary.setReturnduration(returnDuration);
						
						
						trlistrowaltReturn	= (ArrayList<WebElement>) trlistrowalt.get(2).findElements(By.tagName("td"));
						
						String returnDTime	= "";
						returnDTime			= trlistrowaltReturn.get(1).getText().trim().split("\\n")[0];
						flightItinearary.setReturnDepartTime(returnDTime);
						
						String returnATime	= "";
						returnATime			= trlistrowaltReturn.get(1).getText().trim().split("\\n")[1];
						flightItinearary.setReturnArrivalTime(returnATime);
						
						String returnDFrom	= "";
						returnDFrom			= trlistrowaltReturn.get(2).getText().trim().split("\\n")[0];
						flightItinearary.setReturnDepartFrom(returnDFrom);
						
						String returnATo	= "";
						returnATo			= trlistrowaltReturn.get(2).getText().trim().split("\\n")[1];
						flightItinearary.setReturnArrivalTo(returnATo);
						
						String returnRFlight= "";
						returnRFlight		= trlistrowaltReturn.get(3).getText().trim();
						flightItinearary.setReturnFlight(returnRFlight);
						
						String retCabin		= "";
						returnRFlight		= trlistrowalt.get(3).findElements(By.tagName("td")).get(0).getText();
						flightItinearary.setRetCabin(retCabin);
					}
					
					/*moreLinkTR.get(0).findElement(By.id("info_"+counter+"")).click();
					
					WebDriverWait wait = new WebDriverWait(driver, 60);
					try
					{
						wait.until(ExpectedConditions.presenceOfElementLocated(By.id("more_info_flights_WJ_20")));
						Thread.sleep(2000);
					}
					catch (Exception e)
					{
						e.printStackTrace();
					}
					
					if(driver.findElement(By.id("more_info_flights_WJ_20")).isDisplayed())
					{
						driver.manage().timeouts().implicitlyWait(3, TimeUnit.SECONDS);
	
						WebElement OutFlights = driver.findElement(By.className("out_bound"));
						ArrayList<WebElement> outboundflight = new ArrayList<WebElement>(OutFlights.findElements(By.cssSelector("html body div#more_info_flights_WJ_20.modal_window.moreinfo_flight div.more_info div#oneway_return div.more_info_tabs.flightinfo.paddingbot5 div.out_bound div.lineheight25.margin10")));
						
						WebElement InFlights = null;
						ArrayList<WebElement> inboundflight = null;
						
						if(twoway)
						{
							InFlights = driver.findElement(By.className("in_bound"));
							inboundflight = new ArrayList<WebElement>(InFlights.findElements(By.cssSelector("html body div#more_info_flights_WJ_20.modal_window.moreinfo_flight div.more_info div#oneway_return div.more_info_tabs.flightinfo.paddingbot5 div.in_bound div.lineheight25.margin10")));
						}
						
						//OUTBOUND FLIGHT LOOP
						Outbound Outobj = new Outbound();
						ArrayList<Flight> OBflightlist = new ArrayList<Flight>();
						for(int i = 0; i<outboundflight.size() - 1; i++)
						{
							try
							{
								driver.manage().timeouts().implicitlyWait(3, TimeUnit.SECONDS);
								
								Flight obj = new Flight();
								
								String depDate	= "";
								depDate			= outboundflight.get(i).findElement(By.cssSelector("html body div#more_info_flights_WJ_20.modal_window.moreinfo_flight div.more_info div#oneway_return div.more_info_tabs.flightinfo.paddingbot5 div.out_bound div.lineheight25.margin10 div.flight_info_cl_01.fleft")).getText();
								obj.setDepartureDate(depDate);
								
								String arrDate	= "";
								arrDate			= outboundflight.get(i).findElement(By.cssSelector("html body div#more_info_flights_WJ_20.modal_window.moreinfo_flight div.more_info div#oneway_return div.more_info_tabs.flightinfo.paddingbot5 div.out_bound div.lineheight25.margin10 div.flight_info_cl_01.fleft")).getText();
								obj.setArrivalDate(arrDate);
								
								String portid = "";
								portid = outboundflight.get(i).findElement(By.cssSelector("html body div#more_info_flights_WJ_20.modal_window.moreinfo_flight div.more_info div#oneway_return div.more_info_tabs.flightinfo.paddingbot5 div.out_bound div.lineheight25.margin10 div.flight_info_cl_02.fleft")).getText();
								String depart = portid.split("\\n")[0];
								obj.setDeparture_port(depart.split(" ")[0]);
								String arrive = portid.split("\\n")[1];
								obj.setArrival_port(arrive.split(" ")[0]);
								
								String Departportid = "";
								Departportid		= portid.split("\\n")[0].split("[()]")[1];
								obj.setDepartureLocationCode(Departportid);
								
								String Arriveportid = "";
								Arriveportid = portid.split("\\n")[1].split("[()]")[1];
								obj.setArrivalLocationCode(Arriveportid);
								
								String depTime		= "";
								depTime				= outboundflight.get(i).findElement(By.cssSelector("html body div#more_info_flights_WJ_20.modal_window.moreinfo_flight div.more_info div#oneway_return div.more_info_tabs.flightinfo.paddingbot5 div.out_bound div.lineheight25.margin10 div.flight_info_cl_03.fleft")).getText().split("\\n")[0];
								obj.setDepartureTime(depTime);
								
								String arrTime		= "";
								arrTime				= outboundflight.get(i).findElement(By.cssSelector("html body div#more_info_flights_WJ_20.modal_window.moreinfo_flight div.more_info div#oneway_return div.more_info_tabs.flightinfo.paddingbot5 div.out_bound div.lineheight25.margin10 div.flight_info_cl_03.fleft")).getText().split("\\n")[1];
								obj.setArrivalTime(arrTime);							
								
								String marketAirLine = "";
								marketAirLine		 = outboundflight.get(i).findElement(By.cssSelector("html body div#more_info_flights_WJ_20.modal_window.moreinfo_flight div.more_info div#oneway_return div.more_info_tabs.flightinfo.paddingbot5 div.out_bound div.lineheight25.margin10 div.flight_info_cl_05.fleft")).getText();
								obj.setMarketingAirline(marketAirLine);
								
								String flightNo		= "";
								flightNo			= outboundflight.get(i).findElement(By.cssSelector("html body div#more_info_flights_WJ_20.modal_window.moreinfo_flight div.more_info div#oneway_return div.more_info_tabs.flightinfo.paddingbot5 div.out_bound div.lineheight25.margin10 div.flight_info_cl_05.fleft")).getText();
								obj.setFlightNo(flightNo);
								
								String Cabin		= "";
								Cabin				= outboundflight.get(outboundflight.size()-1).getText();
								obj.setCabintype(Cabin);
								
								OBflightlist.add(obj);

							}
							
							catch(Exception e)
							{
								e.printStackTrace();
								TakesScreenshot screen = (TakesScreenshot)driver;
								FileUtils.copyFile(screen.getScreenshotAs(OutputType.FILE),new File("FailedScreenshots/"+System.currentTimeMillis()/1000+".jpg"));
							}
						}
						
						Outobj.setOutBflightlist(OBflightlist);
						flightItinearary.setOutbound(Outobj);
						
						
						if(twoway)
						{
							//INBOUND FLIGHT LOOP
							Inbound Inobj = new Inbound();
							ArrayList<Flight> IBflightlist = new ArrayList<Flight>();
							for(int i = 0; i<inboundflight.size() - 1; i++)
							{
								try
								{
									driver.manage().timeouts().implicitlyWait(3, TimeUnit.SECONDS);
									
									Flight obj = new Flight();
									
									String depDate	= "";
									depDate			= inboundflight.get(i).findElement(By.cssSelector("html body div#more_info_flights_WJ_20.modal_window.moreinfo_flight div.more_info div#oneway_return div.more_info_tabs.flightinfo.paddingbot5 div.in_bound div.lineheight25.margin10 div.flight_info_cl_01.fleft")).getText();
									obj.setDepartureDate(depDate);
									
									String arrDate	= "";
									arrDate			= inboundflight.get(i).findElement(By.cssSelector("html body div#more_info_flights_WJ_20.modal_window.moreinfo_flight div.more_info div#oneway_return div.more_info_tabs.flightinfo.paddingbot5 div.in_bound div.lineheight25.margin10 div.flight_info_cl_01.fleft")).getText(); 
									obj.setArrivalDate(arrDate);
									
									String portid = "";
									portid = inboundflight.get(i).findElement(By.cssSelector("html body div#more_info_flights_WJ_20.modal_window.moreinfo_flight div.more_info div#oneway_return div.more_info_tabs.flightinfo.paddingbot5 div.in_bound div.lineheight25.margin10 div.flight_info_cl_02.fleft")).getText();
									String depart = portid.split("\\n")[0];
									obj.setDeparture_port(depart.split(" ")[0]);
									String arrive = portid.split("\\n")[1];
									obj.setArrival_port(arrive.split(" ")[0]);
									String Departportid = portid.split("\\n")[0].split("[()]")[1];
									obj.setDepartureLocationCode(Departportid);
									String Arriveportid = portid.split("\\n")[1].split("[()]")[1];
									obj.setArrivalLocationCode(Arriveportid);
									
									String depTime	= "";
									depTime			= inboundflight.get(i).findElement(By.cssSelector("html body div#more_info_flights_WJ_20.modal_window.moreinfo_flight div.more_info div#oneway_return div.more_info_tabs.flightinfo.paddingbot5 div.in_bound div.lineheight25.margin10 div.flight_info_cl_03.fleft")).getText().split("\\n")[0];
									obj.setDepartureTime(depTime);
									
									String arrTime	= "";
									arrTime			= inboundflight.get(i).findElement(By.cssSelector("html body div#more_info_flights_WJ_20.modal_window.moreinfo_flight div.more_info div#oneway_return div.more_info_tabs.flightinfo.paddingbot5 div.in_bound div.lineheight25.margin10 div.flight_info_cl_03.fleft")).getText().split("\\n")[1];
									obj.setArrivalTime(arrTime);							
									
									String marketALine	= "";
									marketALine			=  inboundflight.get(i).findElement(By.cssSelector("html body div#more_info_flights_WJ_20.modal_window.moreinfo_flight div.more_info div#oneway_return div.more_info_tabs.flightinfo.paddingbot5 div.in_bound div.lineheight25.margin10 div.flight_info_cl_05.fleft")).getText();
									obj.setMarketingAirline(marketALine);
									
									String flightNo		= "";
									flightNo			= inboundflight.get(i).findElement(By.cssSelector("html body div#more_info_flights_WJ_20.modal_window.moreinfo_flight div.more_info div#oneway_return div.more_info_tabs.flightinfo.paddingbot5 div.in_bound div.lineheight25.margin10 div.flight_info_cl_05.fleft")).getText();
									obj.setFlightNo(flightNo);
									
									String Cabin		= "";
									Cabin				= inboundflight.get(inboundflight.size()-1).getText();
									obj.setCabintype(Cabin);
									
									IBflightlist.add(obj);
								}
								catch(Exception e)
								{
									e.printStackTrace();
									TakesScreenshot screen = (TakesScreenshot)driver;
									FileUtils.copyFile(screen.getScreenshotAs(OutputType.FILE),new File("FailedScreenshots/"+System.currentTimeMillis()/1000+".jpg"));
								}
							}
							
							Inobj.setInBflightlist(IBflightlist);
							flightItinearary.setInbound(Inobj);
						}
						
					}
					*/
					driver.findElement(By.cssSelector("html body div#more_info_flights_WJ_20.modal_window.moreinfo_flight div.close_modal_window")).click();
					Resultlist.add(flightItinearary);
					//nt++;
					counter++;
				}
				
			}

			catch(Exception e)
			{
				e.printStackTrace();
				TakesScreenshot screen = (TakesScreenshot)driver;
				FileUtils.copyFile(screen.getScreenshotAs(OutputType.FILE),new File("FailedScreenshots/"+System.currentTimeMillis()/1000+".jpg"));
			}
				
			((JavascriptExecutor)driver).executeScript("doIWC(new IWCParams('PAGING','RESULTS','PAGE','', {'pageNo':"+clickpattern+"},'','WJ_2'))");
			clickpattern++;		
			count++; 
		}

		clickpattern = 1;
		((JavascriptExecutor)driver).executeScript("doIWC(new IWCParams('PAGING','RESULTS','PAGE','', {'pageNo':"+clickpattern+"},'','WJ_2'))");
			
		return Resultlist;
	}

	
	public boolean validateAllFilters(WebDriver driver, StringBuffer ReportPrinter, int Reportcount, String trip/*, ArrayList<UIFlightItinerary>	Resultlist*/)
	{
		try {
			driver.findElement(By.partialLinkText("Show Additional Filter")).click();
		} catch (Exception e) {
			System.out.println("Where is the show additional filters link bro...???");
		}
		
		boolean status = false;
		
		ArrayList<UIFlightItinerary>	ORIGINAL		= new ArrayList<UIFlightItinerary>();
		ArrayList<Double>				Original		= new ArrayList<Double>();
		try {
			ORIGINAL = getResults(driver, trip);
			System.out.println(ORIGINAL.size());
			for(int y = 0; y<ORIGINAL.size(); y++)
			{
				Original.add( Double.parseDouble(ORIGINAL.get(y).getCost()) );
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		//Filters : Price - Ascending
		try 
		{
			new Select(driver.findElement(By.id("flighsortoptions_WJ_4"))).selectByVisibleText("Price - Ascending");
			ArrayList<UIFlightItinerary> FILTEREDASCE = new ArrayList<UIFlightItinerary>();
			FILTEREDASCE = getResults(driver, trip);
			
			Collections.sort(Original);
			boolean pass = true;
			
			ReportPrinter.append("<span><center><p class='Hedding0'>Filters : Price - Ascending</p></center></span>");
			ReportPrinter.append("<table style=width:100%>"
			+ "<tr><th>Test Case</th>"
			+ "<th>Test Description</th>"
			+ "<th>Expected Result</th>"
			+ "<th>Actual Result</th>"
			+ "<th>Test Status</th></tr>");
			
			for(int i = 0; i<Original.size(); i++)
			{
				ReportPrinter.append("<tr><td>"+Reportcount+"</td>"
				+ "<td> Price Acsending </td>"
				+ "<td>"+Original.get(i)+"</td>");
				if(Original.get(i) == (Double.parseDouble(FILTEREDASCE.get(i).getCost()) ))
				{
					pass = true;
				}
				else
				{
					pass = false;
				}
				
				if( pass ) 
				{
					ReportPrinter.append("<td>"+(Double.parseDouble(FILTEREDASCE.get(i).getCost()) )+"</td>"
					+ "<td class='Passed'>PASS</td></tr>");
					Reportcount++;
				}
				else
				{
					ReportPrinter.append("<td>"+(Double.parseDouble(FILTEREDASCE.get(i).getCost()) )+"</td>"
					+ "<td class='Failed'>Fail</td></tr>");
					Reportcount++;
				}
			}
			
			new Select(driver.findElement(By.id("flighsortoptions_WJ_4"))).selectByVisibleText("Price - Ascending");
			int clickpattern = 1;
			((JavascriptExecutor)driver).executeScript("doIWC(new IWCParams('PAGING','RESULTS','PAGE','', {'pageNo':"+clickpattern+"},'','WJ_2'))");
		} catch (Exception e) {
			
		}
		
		ReportPrinter.append("</table>");
		ReportPrinter.append("<br><br><br>");
		
		
		
		//Filters : Price - Descending
		try 
		{
			Thread.sleep(3000);
			
			new Select(driver.findElement(By.id("flighsortoptions_WJ_4"))).selectByVisibleText("Price - Descending");
			ArrayList<UIFlightItinerary> FILTEREDDESC = new ArrayList<UIFlightItinerary>();
			FILTEREDDESC = getResults(driver, trip);
			
			Collections.sort(Original);
			Collections.reverse(Original);
			boolean pass = true;
			
			ReportPrinter.append("<span><center><p class='Hedding0'>Filters : Price - Descending</p></center></span>");
			ReportPrinter.append("<table style=width:100%>"
			+ "<tr><th>Test Case</th>"
			+ "<th>Test Description</th>"
			+ "<th>Expected Result</th>"
			+ "<th>Actual Result</th>"
			+ "<th>Test Status</th></tr>");
					
			for(int i = 0; i<Original.size(); i++)
			{
				ReportPrinter.append("<tr><td>"+Reportcount+"</td>"
				+ "<td> Price Decsending </td>"
				+ "<td>"+Original.get(i)+"</td>");
				
				if(Original.get(i) == (Double.parseDouble(FILTEREDDESC.get(i).getCost()) ))
				{
					pass = true;
				}
				else
				{
					pass = false;
				}
						
				if( pass ) 
				{
					ReportPrinter.append("<td>"+(Double.parseDouble(FILTEREDDESC.get(i).getCost()) )+"</td>"
					+ "<td class='Passed'>PASS</td></tr>");
					Reportcount++;
				}
				else
				{
					ReportPrinter.append("<td>"+(Double.parseDouble(FILTEREDDESC.get(i).getCost()) )+"</td>"
					+ "<td class='Failed'>Fail</td></tr>");
					Reportcount++;
				}
			}
			
			new Select(driver.findElement(By.id("flighsortoptions_WJ_4"))).selectByVisibleText("Price - Ascending");
			int clickpattern = 1;
			((JavascriptExecutor)driver).executeScript("doIWC(new IWCParams('PAGING','RESULTS','PAGE','', {'pageNo':"+clickpattern+"},'','WJ_2'))");
				
		} catch (Exception e) {
					
		}
				
		ReportPrinter.append("</table>");
		ReportPrinter.append("<br><br><br>");

		
		//SLIDER MINMAX VALUE
		try {
			
			String X = driver.findElement(By.id("price_text_WJ_5")).getText().trim();
			
			String[] v = X.trim().split("-");
			
			String[] place1 = v[0].trim().split(" ");
			String[] place2 = v[1].trim().split(" ");
			
			String MIN = place1[2].trim();
			String MAX = place2[2].trim();
			
			Collections.sort(Original);
			int size = Original.size();
			
			ReportPrinter.append("<span><center><p class='Hedding0'>Filters : Price Slider Validation</p></center></span>");
			ReportPrinter.append("<table style=width:100%>"
			+ "<tr><th>Test Case</th>"
			+ "<th>Test Description</th>"
			+ "<th>Expected Result</th>"
			+ "<th>Actual Result</th>"
			+ "<th>Test Status</th></tr>");
			
			ReportPrinter.append("<tr><td>"+Reportcount+"</td>"
			+ "<td> Price slider minimum value </td>"
			+ "<td>"+Original.get(0)+"</td>");
			if( Original.get(0) ==  Double.parseDouble(MIN) ) 
			{
				ReportPrinter.append("<td>"+MIN+"</td>"
				+ "<td class='Passed'>PASS</td></tr>");
				Reportcount++;
			}
			else
			{
				ReportPrinter.append("<td>"+MIN+"</td>"
				+ "<td class='Failed'>Fail</td></tr>");
				Reportcount++;
			}
			
			ReportPrinter.append("<tr><td>"+Reportcount+"</td>"
			+ "<td> Price slider maximum value </td>"
			+ "<td>"+Original.get(size-1)+"</td>");
			if( Original.get(size-1) ==  Double.parseDouble(MAX) ) 
			{
				ReportPrinter.append("<td>"+MAX+"</td>"
				+ "<td class='Passed'>PASS</td></tr>");
				Reportcount++;
			}
			else
			{
				ReportPrinter.append("<td>"+MAX+"</td>"
				+ "<td class='Failed'>Fail</td></tr>");
				Reportcount++;
			}
			
			new Select(driver.findElement(By.id("flighsortoptions_WJ_4"))).selectByVisibleText("Price - Ascending");
			int clickpattern = 1;
			((JavascriptExecutor)driver).executeScript("doIWC(new IWCParams('PAGING','RESULTS','PAGE','', {'pageNo':"+clickpattern+"},'','WJ_2'))");
			
		} catch (Exception e) {
			
		}
		
		ReportPrinter.append("</table>");
		ReportPrinter.append("<br><br><br>");
		
		
		
		//LAYOVER VALIDATION
		try
		{
			ArrayList<Integer>	OriginalLayOver	= new ArrayList<Integer>();
			
			try {
				for(int y = 0; y<ORIGINAL.size(); y++)
				{
					int q = Integer.valueOf( CommonValidator.formatTimeToCommon(ORIGINAL.get(y).getLeaveduration()).trim()/*.replace(":", "")*/ ) ;
					if(twoway)
					{
						int r = Integer.valueOf( CommonValidator.formatTimeToCommon(ORIGINAL.get(y).getReturnduration()).trim()/*.replace(":", "")*/ ) ;
						OriginalLayOver.add(r);
					}
					OriginalLayOver.add(q);
				}
			} catch (Exception e) {
				
			}
			
			String	X		= driver.findElement(By.id("layover_text_WJ_16")).getText().trim();
			
			String[] part	= X.trim().split("-");
			String	MINLO	= part[0].trim();
			String	strmin	= CommonValidator.formatTimeToCommon(MINLO).trim();
			
			String	MAXLO	= part[1].trim();
			MAXLO			= MAXLO.replace("To ","");
			String	strmax	= CommonValidator.formatTimeToCommon(MAXLO).trim();
			
			Collections.sort(OriginalLayOver);
			int size = OriginalLayOver.size();
			
			ReportPrinter.append("<span><center><p class='Hedding0'>Filters : Layover Slider Validation</p></center></span>");
			ReportPrinter.append("<table style=width:100%>"
			+ "<tr><th>Test Case</th>"
			+ "<th>Test Description</th>"
			+ "<th>Expected Result</th>"
			+ "<th>Actual Result</th>"
			+ "<th>Test Status</th></tr>");
			
			ReportPrinter.append("<tr><td>"+Reportcount+"</td>"
			+ "<td> Layover slider minimum value </td>"
			+ "<td>"+OriginalLayOver.get(0)+"</td>");
			if( String.valueOf(OriginalLayOver.get(0)).contains(strmin) || strmin.contains(String.valueOf(OriginalLayOver.get(0))) ) 
			{
				ReportPrinter.append("<td>"+strmin+"</td>"
				+ "<td class='Passed'>PASS</td></tr>");
				Reportcount++;
				System.out.println(strmin);
			}
			else
			{
				ReportPrinter.append("<td>"+strmin+"</td>"
				+ "<td class='Failed'>Fail</td></tr>");
				Reportcount++;
			}
			
			ReportPrinter.append("<tr><td>"+Reportcount+"</td>"
			+ "<td> Layover slider maximum value </td>"
			+ "<td>"+OriginalLayOver.get(size-1)+"</td>");
			if( String.valueOf(OriginalLayOver.get(size-1)).contains(strmax) || strmax.contains(String.valueOf(OriginalLayOver.get(size-1))) ) 
			{
				ReportPrinter.append("<td>"+strmax+"</td>"
				+ "<td class='Passed'>PASS</td></tr>");
				Reportcount++;
				System.out.println(strmax);
			}
			else
			{
				ReportPrinter.append("<td>"+strmax+"</td>"
				+ "<td class='Failed'>Fail</td></tr>");
				Reportcount++;
			}
			
			new Select(driver.findElement(By.id(PropertyMap.get("ResultPg_FlightSortOptions_DropDwn_id")))).selectByVisibleText("Price - Ascending");
			int clickpattern = 1;
			((JavascriptExecutor)driver).executeScript("doIWC(new IWCParams('PAGING','RESULTS','PAGE','', {'pageNo':"+clickpattern+"},'','WJ_2'))");
			
			
		} catch (Exception e) {
			
		}
		ReportPrinter.append("</table>");
		ReportPrinter.append("<br><br><br>");
		
		
		
		//FLIGHTLEG SLIDER VALIDATION
		try
		{
			ArrayList<Integer>	OriginalLeg1	= new ArrayList<Integer>();
			ArrayList<Integer>	OriginalLeg2	= new ArrayList<Integer>();
					
			try
			{
				for(int y = 0; y<ORIGINAL.size(); y++)
				{
					int q = Integer.valueOf( CommonValidator.formatTimeToCommon(ORIGINAL.get(y).getLeaveDepartTime().trim()));
					OriginalLeg1.add(q);
					
					if(twoway)
					{
						int r = Integer.valueOf( CommonValidator.formatTimeToCommon(ORIGINAL.get(y).getReturnDepartTime().trim()));
						OriginalLeg2.add(r);
					}		
				}
			}
			catch (Exception e)
			{
						
			}
					
			String	X1		= driver.findElement(By.id("flight_leg1_text_WJ_17")).getText().trim();
			String	MINLO1	= X1.trim().split("-")[0].trim();
			String	MAXLO1	= X1.trim().split("-")[1].trim();
					MAXLO1	= MAXLO1.replace("To ", "").trim();
			String	strmin1	= CommonValidator.formatTimeToCommon(MINLO1).trim();
			String	strmax1	= CommonValidator.formatTimeToCommon(MAXLO1).trim();
					
			Collections.sort(OriginalLeg1);
			int size = OriginalLeg1.size();
					
			ReportPrinter.append("<span><center><p class='Hedding0'>Filters : Flightleg  Slider Validation</p></center></span>");
			ReportPrinter.append("<table style=width:100%>"
			+ "<tr><th>Test Case</th>"
			+ "<th>Test Description</th>"
			+ "<th>Expected Result</th>"
			+ "<th>Actual Result</th>"
			+ "<th>Test Status</th></tr>");
					
			ReportPrinter.append("<tr><td>"+Reportcount+"</td>"
			+ "<td> Flightleg 1 slider minimum value </td>"
			+ "<td>"+OriginalLeg1.get(0)+"</td>");
			if( String.valueOf(OriginalLeg1.get(0)).contains(strmin1) || strmin1.contains(String.valueOf(OriginalLeg1.get(0))) ) 
			{
				ReportPrinter.append("<td>"+strmin1+"</td>"
				+ "<td class='Passed'>PASS</td></tr>");
				Reportcount++;
			}
			else
			{
				ReportPrinter.append("<td>"+strmin1+"</td>"
				+ "<td class='Failed'>Fail</td></tr>");
				Reportcount++;
			}
					
			ReportPrinter.append("<tr><td>"+Reportcount+"</td>"
			+ "<td> Flightleg 1 slider maximum value </td>"
			+ "<td>"+OriginalLeg1.get(size-1)+"</td>");
			if( String.valueOf(OriginalLeg1.get(size-1)).contains(strmax1) || strmax1.contains(String.valueOf(OriginalLeg1.get(size-1))) ) 
			{
				ReportPrinter.append("<td>"+strmax1+"</td>"
				+ "<td class='Passed'>PASS</td></tr>");
				Reportcount++;
			}
			else
			{
				ReportPrinter.append("<td>"+strmax1+"</td>"
				+ "<td class='Failed'>Fail</td></tr>");
				Reportcount++;
			}
			
			///////////LEG 2
			size = OriginalLeg2.size();
			if(twoway)
			{
				try
				{
					String	X2		= driver.findElement(By.id("flight_leg2_text_WJ_17")).getText().trim();
					String	MINLO2	= X2.trim().split("-")[0].trim();
					String	MAXLO2	= X2.trim().split("-")[1].trim();
							MAXLO2	= MAXLO2.replace("To ", "").trim();
					String	strmin2	= CommonValidator.formatTimeToCommon(MINLO2).trim();
					String	strmax2	= CommonValidator.formatTimeToCommon(MAXLO2).trim();
							
					Collections.sort(OriginalLeg2);
							
					ReportPrinter.append("<tr><td>"+Reportcount+"</td>"
					+ "<td> Flightleg 2 slider minimum value </td>"
					+ "<td>"+OriginalLeg2.get(0)+"</td>");
					if( String.valueOf(OriginalLeg2.get(0)).contains(strmin2) || strmin2.contains(String.valueOf(OriginalLeg2.get(0))) ) 
					{
						ReportPrinter.append("<td>"+strmin2+"</td>"
						+ "<td class='Passed'>PASS</td></tr>");
						Reportcount++;
					}
					else
					{
						ReportPrinter.append("<td>"+strmin2+"</td>"
						+ "<td class='Failed'>Fail</td></tr>");
						Reportcount++;
					}
							
					ReportPrinter.append("<tr><td>"+Reportcount+"</td>"
					+ "<td> Flightleg 2 slider maximum value </td>"
					+ "<td>"+OriginalLeg2.get(size-1)+"</td>");
					if( String.valueOf(OriginalLeg2.get(size-1)).contains(strmax2) || strmax2.contains(String.valueOf(OriginalLeg2.get(size-1))) ) 
					{
						ReportPrinter.append("<td>"+strmax2+"</td>"
						+ "<td class='Passed'>PASS</td></tr>");
						Reportcount++;
					}
					else
					{
						ReportPrinter.append("<td>"+strmax2+"</td>"
						+ "<td class='Failed'>Fail</td></tr>");
						Reportcount++;
					}
				}
				catch (Exception e)
				{
					
				}//try
				
			}//twoway
			
			new Select(driver.findElement(By.id(PropertyMap.get("ResultPg_FlightSortOptions_DropDwn_id")))).selectByVisibleText("Price - Ascending");
			int clickpattern = 1;
			((JavascriptExecutor)driver).executeScript("doIWC(new IWCParams('PAGING','RESULTS','PAGE','', {'pageNo':"+clickpattern+"},'','WJ_2'))");
					
		}
		catch (Exception e)
		{
			
		}
		
		ReportPrinter.append("</table>");
		ReportPrinter.append("<br><br><br>");
		
		
		return status;
	}

	
}
