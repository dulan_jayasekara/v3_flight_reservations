package Results.Validate;

import java.util.HashMap;

import org.openqa.selenium.WebDriver;

import Flight.Cancellation.CancellationReport;
import Flight.Cancellation.CancellationRequest;
import Flight.Cancellation.CancellationRequestReader;
import Flight.Cancellation.CancellationResponse;
import Flight.Cancellation.CancellationResponseReader;
import Flight.Cancellation.CancellationScreen;
import Flight.Classes.*;
import Flight.Reports.BookingConfReportReader;
import Flight.Reports.BookingListReport;
import Flight.Reports.CustomerVoucherEmail;
import Flight.Reports.ProfitAndLoss;
import Flight.Reports.ReservationReportReader;
import Flight.Reports.SupplierPayableReport;
import Flight.TypeEnum.*;

public class ValidateReports {
	
	HashMap<String, String>	Propertymap	= new HashMap<String, String>();
	Validate				validate	= null;
	
	public ValidateReports(HashMap<String, String>	Property, Validate valid)
	{
		this.Propertymap	= Property;
		this.validate		= valid;
	}
	
	public StringBuffer validateReservationReport(WebDriver driver, UIConfirmationPage confirmationpage, StringBuffer ReportPrinter, SearchObject searchObject, AirConfig configuration, XMLPriceItinerary XMLSelectFlight, int Reportcount)
	{
		String ReservationNo = "";
		ReservationNo = confirmationpage.getReservationNo().trim();
			
		//RESERVATION REPORT VALIDATION
		try
		{
			ReportPrinter.append("<span><center><p class='Hedding0'>Reservation Report Validation</p></center></span>");
			ReportPrinter.append("<table style=width:100%>"
			+ "<tr><th>Test Case</th>"
			+ "<th>Test Description</th>"
			+ "<th>Expected Result</th>"
			+ "<th>Actual Result</th>"
			+ "<th>Test Status</th></tr>");
			
			ReservationReportReader	resvReportReader	= new ReservationReportReader(Propertymap);
			ReservationReport		reservationReport	= new ReservationReport();
			try {
				reservationReport	= resvReportReader.getReport(driver, ReservationNo);
			} catch (Exception e) {
				
			}
			
			ReportPrinter.append("<tr><td>"+Reportcount+"</td>"
			+ "<td>Check Reservation Report availability</td>"
			+ "<td>If Available - true , If Not Available - false</td>");
			
			if(reservationReport.isAvailable())
			{
				ReportPrinter.append("<td>"+reservationReport.isAvailable()+"</td>"
				+ "<td class='Passed'>PASS</td></tr>");
				Reportcount++;	
				
				ReportPrinter	= validate.validateReservationReport(reservationReport, searchObject, XMLSelectFlight, configuration, ReportPrinter);
				
				ReportPrinter.append("</table>");
				ReportPrinter.append("<br><br><br>");
			}
			else
			{
				ReportPrinter.append("<td>"+reservationReport.isAvailable()+"</td>"
				+ "<td class='Failed'>Fail</td></tr>");
				ReportPrinter.append("</table>");
				ReportPrinter.append("<br><br><br>");
			}
		}
		catch (Exception e)
		{
			e.printStackTrace();
			ReportPrinter.append("</table>");
			ReportPrinter.append("<br><br><br>");
		}

		return ReportPrinter;
	}
	
	public StringBuffer validateSuppPlayableReport(WebDriver driver, UIConfirmationPage confirmationpage, StringBuffer ReportPrinter, SearchObject searchObject, ReservationInfo fillingObject, XMLPriceItinerary XMLSelectFlight, AirConfig configuration, int Reportcount)
	{
		String ReservationNo = "";
		ReservationNo = confirmationpage.getReservationNo().trim();
			
		//SUPPLIER PAYABLE REPORT VALIDATION
		try
		{
			ReportPrinter.append("<span><center><p class='Hedding0'>Supplier Payable Report Validation</p></center></span>");
			ReportPrinter.append("<table style=width:100%>"
			+ "<tr><th>Test Case</th>"
			+ "<th>Test Description</th>"
			+ "<th>Expected Result</th>"
			+ "<th>Actual Result</th>"
			+ "<th>Test Status</th></tr>");
			
			SupplierPayableReport suppPayableReport = new SupplierPayableReport(Propertymap);
			
			suppPayableReport.getPaybleDetails(driver, Propertymap.get("Flight_Supplier"), ReservationNo);
			SupplierPayableReport suppPayableReportBeforePay = suppPayableReport;
			
			if( searchObject.getSupplierPayablePayStatus().equalsIgnoreCase("YES") )
			{
				suppPayableReport.paySupplier(Propertymap.get("Payment.Reference"));
			}
			
			SupplierPayableReport suppPayableReportAfterPay = suppPayableReport;
			
			ReportPrinter.append("<tr><td>"+Reportcount+"</td>"
			+ "<td>Check Supplier Payable Report availability</td>"
			+ "<td>If Available - true , If Not Available - false</td>");
			
			if(suppPayableReport.isRecordExist())
			{
				ReportPrinter.append("<td>"+suppPayableReport.isRecordExist()+"</td>"
				+ "<td class='Passed'>PASS</td></tr>");
				Reportcount++;	
				
				ReportPrinter = validate.validateSupplierPayableReport(suppPayableReportBeforePay, suppPayableReportAfterPay, searchObject, XMLSelectFlight, confirmationpage, fillingObject, configuration, ReportPrinter);
				
				ReportPrinter.append("</table>");
				ReportPrinter.append("<br><br><br>");
			}
			else
			{
				ReportPrinter.append("<td>"+suppPayableReport.isRecordExist()+"</td>"
				+ "<td class='Failed'>Fail</td></tr>");
				ReportPrinter.append("</table>");
				ReportPrinter.append("<br><br><br>");
			}
		}
		catch (Exception e)
		{
			e.printStackTrace();
			ReportPrinter.append("</table>");
			ReportPrinter.append("<br><br><br>");
		}

		return ReportPrinter;
	}
	
	public StringBuffer validateCustomerVoucherEmail(WebDriver driver, UIConfirmationPage confirmationpage, StringBuffer ReportPrinter, SearchObject searchObject, XMLPriceItinerary XMLSelectFlight, ReservationInfo fillingObject, AirConfig configuration, int Reportcount)
	{
		String ReservationNo = "";
		ReservationNo = confirmationpage.getReservationNo().trim();
		//CUSTOMER VOUCHER VALIDATION
		try
		{
			ReportPrinter.append("<span><center><p class='Hedding0'>Customer Voucher E-mail Validation</p></center></span>");
			ReportPrinter.append("<table style=width:100%>"
			+ "<tr><th>Test Case</th>"
			+ "<th>Test Description</th>"
			+ "<th>Expected Result</th>"
			+ "<th>Actual Result</th>"
			+ "<th>Test Status</th></tr>");
			
			CustomerVoucherEmail	email	= new CustomerVoucherEmail(Propertymap);
			email.getCusVoucherEmail(driver, ReservationNo);
			
			ReportPrinter.append("<tr><td>"+Reportcount+"</td>"
			+ "<td>Check Customer Voucher email availability</td>"
			+ "<td>If Available - true , If Not Available - false</td>");
			
			if(email.isAvailable())
			{
				ReportPrinter.append("<td>"+email.isAvailable()+"</td>"
				+ "<td class='Passed'>PASS</td></tr>");
				Reportcount++;
				
				ReportPrinter			= validate.validateCustomerVoucherEmail(email, fillingObject, confirmationpage, XMLSelectFlight, searchObject, configuration, ReportPrinter);
				
				ReportPrinter.append("</table>");
				ReportPrinter.append("<br><br><br>");
			}
			else
			{
				ReportPrinter.append("<td>"+email.isAvailable()+"</td>"
				+ "<td class='Failed'>Fail</td></tr>");
				ReportPrinter.append("</table>");
				ReportPrinter.append("<br><br><br>");
			}
		}
		catch (Exception e)
		{
			e.printStackTrace();
			ReportPrinter.append("</table>");
			ReportPrinter.append("<br><br><br>");
		}

		return ReportPrinter;
	}	
	
	public StringBuffer validateBookingConfReport(WebDriver driver, UIConfirmationPage confirmationpage, SearchObject searchObject, ReservationInfo fillingObject, XMLPriceItinerary XMLSelectFlight, AirConfig configuration, StringBuffer ReportPrinter, int Reportcount)
	{
		String ReservationNo = "";
		ReservationNo = confirmationpage.getReservationNo().trim();
		
		try
		{
			ReportPrinter.append("<span><center><p class='Hedding0'>Booking Confirmation Report Validation</p></center></span>");
			ReportPrinter.append("<table style=width:100%>"
			+ "<tr><th>Test Case</th>"
			+ "<th>Test Description</th>"
			+ "<th>Expected Result</th>"
			+ "<th>Actual Result</th>"
			+ "<th>Test Status</th></tr>");
			
			BookingConfReportReader	report	= new BookingConfReportReader(Propertymap);
			report.getReport(driver, ReservationNo);
			
			ReportPrinter.append("<tr><td>"+Reportcount+"</td>"
			+ "<td>Check Booking Confirmation Report availability</td>"
			+ "<td>If Available - true , If Not Available - false</td>");
			if(report.isAvailable())
			{
				ReportPrinter.append("<td>"+report.isAvailable()+"</td>"
				+ "<td class='Passed'>PASS</td></tr>");
				Reportcount++;  
				
				ReportPrinter = validate.validateBookingConfEmail(report, searchObject, fillingObject, XMLSelectFlight, configuration, ReportPrinter);
				
				ReportPrinter.append("</table>");
				ReportPrinter.append("<br><br><br>");
			}
			else
			{
				ReportPrinter.append("<td>"+report.isAvailable()+"</td>"
				+ "<td class='Failed'>Fail</td></tr>");
				ReportPrinter.append("</table>");
				ReportPrinter.append("<br><br><br>");
			}
		}
		catch (Exception e)
		{
			e.printStackTrace();
			ReportPrinter.append("</table>");
			ReportPrinter.append("<br><br><br>");
		}
		
		return ReportPrinter;
	}
	
	public StringBuffer validateBookingListReport(WebDriver driver, UIConfirmationPage confirmationpage, SearchObject searchObject, ReservationInfo fillingObject, XMLPriceItinerary XMLSelectFlight, AirConfig configuration, StringBuffer ReportPrinter, int Reportcount)
	{
		String ReservationNo = "";
		ReservationNo = confirmationpage.getReservationNo().trim();
			
		//BOOKING LIST REPORT VALIDATION
		try
		{
			ReportPrinter.append("<span><center><p class='Hedding0'>Booking List Report Validation</p></center></span>");
			ReportPrinter.append("<table style=width:100%>"
			+ "<tr><th>Test Case</th>"
			+ "<th>Test Description</th>"
			+ "<th>Expected Result</th>"
			+ "<th>Actual Result</th>"
			+ "<th>Test Status</th></tr>");
			
			BookingListReport	report	= new BookingListReport(Propertymap);
			report.getReport(driver, ReservationNo);
			
			ReportPrinter.append("<tr><td>"+Reportcount+"</td>"
			+ "<td>Check Customer Voucher email availability</td>"
			+ "<td>If Available - true , If Not Available - false</td>");
			if(report.isIsreportLoaded())
			{
				ReportPrinter.append("<td>"+report.isIsreportLoaded()+"</td>"
				+ "<td class='Passed'>PASS</td></tr>");
				Reportcount++;
				
				ReportPrinter = validate.validateBookingListReport(report, fillingObject, XMLSelectFlight, searchObject, confirmationpage, configuration, ReportPrinter);
				
				ReportPrinter.append("</table>");
				ReportPrinter.append("<br><br><br>");
			}
			else
			{
				ReportPrinter.append("<td>"+report.isIsreportLoaded()+"</td>"
				+ "<td class='Failed'>Fail</td></tr>");
				ReportPrinter.append("</table>");
				ReportPrinter.append("<br><br><br>");
			}
		}
		catch (Exception e)
		{
			//System.out.println();
			e.printStackTrace();
			ReportPrinter.append("</table>");
			ReportPrinter.append("<br><br><br>");
		}
		return ReportPrinter;
	}
	
	public StringBuffer validateProfitLossReport(WebDriver driver, UIConfirmationPage confirmationpage, SearchObject searchObject, ReservationInfo fillingObject, XMLPriceItinerary XMLSelectFlight, AirConfig configuration, StringBuffer ReportPrinter, int Reportcount)
	{
		String ReservationNo = "";
		ReservationNo = confirmationpage.getReservationNo().trim();
			
		//Profit and Loss Report VALIDATION
		try
		{
			ProfitAndLoss report	= new ProfitAndLoss(Propertymap);
			try {
				report.loadReport(driver, ReservationNo);
			} catch (Exception e) {
				
			}
			
			
			ReportPrinter.append("<span><center><p class='Hedding0'>Profit and Loss Validation</p></center></span>");
			ReportPrinter.append("<table style=width:100%>"
			+ "<tr><th>Test Case</th>"
			+ "<th>Test Description</th>"
			+ "<th>Expected Result</th>"
			+ "<th>Actual Result</th>"
			+ "<th>Test Status</th></tr>");
			ReportPrinter.append("<tr><td>"+Reportcount+"</td>"
			+ "<td>Check profit and loss report availability</td>"
			+ "<td>If Available - true , If Not Available - false</td>");
			if(report.isReportLoaded())
			{
				ReportPrinter.append("<td>"+report.isReportLoaded()+"</td>"
				+ "<td class='Passed'>PASS</td></tr>");
				Reportcount++;
				
				ReportPrinter = validate.validateProfitAndLoss(XMLSelectFlight, searchObject, report, configuration, ReportPrinter);
				
				ReportPrinter.append("</table>");
				ReportPrinter.append("<br><br><br>");
			}
			else
			{
				ReportPrinter.append("<td>"+report.isReportLoaded()+"</td>"
				+ "<td class='Failed'>Fail</td></tr>");
				ReportPrinter.append("</table>");
				ReportPrinter.append("<br><br><br>");
			}
		}
		catch (Exception e)
		{
			e.printStackTrace();
			ReportPrinter.append("</table>");
			ReportPrinter.append("<br><br><br>");
		}
		return ReportPrinter;
	}
	
	public StringBuffer doCancellation(WebDriver driver, CancellationScreen cancellationScreen, CancellationReport cancellationReport, UIConfirmationPage confirmationpage, SearchObject searchObject, ReservationInfo fillingObject, XMLPriceItinerary XMLSelectFlight, StringBuffer ReportPrinter, int Reportcount)
	{
		//======================================CANCELLATION SCREEN VALIDATION=====================================
		
		try {
			
			try {
				cancellationScreen.getCancellationScreen(driver, confirmationpage.getReservationNo(), XMLSelectFlight, searchObject);
			} catch (Exception e) {
				
			}
			ReportPrinter.append("<span><center><p class='Hedding0'>Cancellation Screen Validation</p></center></span>");
			ReportPrinter.append("<table style=width:100%>"
			+ "<tr><th>Test Case</th>"
			+ "<th>Test Description</th>"
			+ "<th>Expected Result</th>"
			+ "<th>Actual Result</th>"
			+ "<th>Test Status</th></tr>");
			ReportPrinter.append("<tr><td>"+Reportcount+"</td>"
			+ "<td>Check Cancellation Screen Availability</td>"
			+ "<td>If Available - true , If Not Available - false</td>");
			if(!cancellationScreen.isNotFound())
			{
				ReportPrinter.append("<td>"+cancellationScreen.isNotFound()+"</td>"
				+ "<td class = 'Passed'>PASS</td></tr>" );
				Reportcount++;
				
				try {
					ReportPrinter = validate.validateCancellationScreen(XMLSelectFlight, searchObject, fillingObject, confirmationpage, cancellationScreen, ReportPrinter);
				} catch (Exception e) {
					
				}
				
				ReportPrinter.append("<tr><td>"+Reportcount+"</td>"
				+ "<td>Check Cancellation Status</td>"
				+ "<td>If Cancellation Done - true and Display Message , If Not Done - false and Display Error Message</td>");
				if(cancellationScreen.isCancelled())
				{
					ReportPrinter.append("<td>"+cancellationScreen.isCancelled()+"<br>"+cancellationScreen.getCancellationMessage()+"</td>"
					+ "<td class = 'Passed'>PASS</td></tr>" );
					Reportcount++;
				}
				else
				{
					ReportPrinter.append("<td>"+cancellationScreen.isCancelled()+"<br>"+cancellationScreen.getCancellationMessage()+"</td>"
					+ "<td class = 'Failed'>Fail</td></tr>");
					Reportcount++;
				}
				
				ReportPrinter.append("</table>");
				ReportPrinter.append("<br><br><br>");	
			}
			else
			{
				ReportPrinter.append("<td>"+cancellationScreen.isNotFound()+"</td>"
				+ "<td class = 'Failed'>Fail</td></tr>");
				Reportcount++;
				ReportPrinter.append("</table>");
				ReportPrinter.append("<br><br><br>");
			}
			
			if( !cancellationScreen.isNotFound() && cancellationScreen.isCancelled() )
			{
				
				try {
					cancellationReport.loadReport(driver, confirmationpage.getReservationNo());
				} catch (Exception e) {
						
				}
					
				ReportPrinter.append("<span><center><p class='Hedding0'>Cancellation Report Validation</p></center></span>");
				ReportPrinter.append("<table style=width:100%>"
				+ "<tr><th>Test Case</th>"
				+ "<th>Test Description</th>"
				+ "<th>Expected Result</th>"
				+ "<th>Actual Result</th>"
				+ "<th>Test Status</th></tr>");
				ReportPrinter.append("<tr><td>"+Reportcount+"</td>"
				+ "<td>Check Cancellation Report Availability</td>"
				+ "<td>If Available - true , If Not Available - false</td>");
				if(cancellationReport.isReportLoded())
				{
					ReportPrinter.append("<td>"+cancellationReport.isReportLoded()+"</td>"
					+ "<td class='Passed'>PASS</td></tr>");
					Reportcount++;
					try {
						ReportPrinter = validate.validateCancellationReport(XMLSelectFlight, searchObject, fillingObject, confirmationpage, cancellationScreen, cancellationReport, ReportPrinter);
					} catch (Exception e) {
						
					}
					ReportPrinter.append("</table>");
					ReportPrinter.append("<br><br><br>");
					
					
					try {
						
						CancellationRequest cancellationRequest = new CancellationRequest();
						CancellationRequestReader cancellationReqReader = new CancellationRequestReader(Propertymap);
						ReportPrinter.append("<span><center><p class='Hedding0'>Cancellation Request Validation</p></center></span>");
						ReportPrinter.append("<table style=width:100%>"
						+ "<tr><th>Test Case</th>"
						+ "<th>Test Description</th>"
						+ "<th>Expected Result</th>"
						+ "<th>Actual Result</th>"
						+ "<th>Test Status</th></tr>");
						cancellationRequest = cancellationReqReader.RequestReader(XMLLocateType.TRACER, /*searchObject.getTracer()*/confirmationpage.getSupplierConfirmationNo().trim(), XMLFileType.AIR1_Cancellationrequest_);
						ReportPrinter = validate.validateCancellationRequest(cancellationRequest, confirmationpage, ReportPrinter);
						ReportPrinter.append("</table>");
						ReportPrinter.append("<br><br><br>");
					} catch (Exception e) {
						
					}
					ReportPrinter.append("</table>");
					ReportPrinter.append("<br><br><br>");
					
					try {
						
						CancellationResponse cancellationResponse = new CancellationResponse();
						CancellationResponseReader cancellationResReader = new CancellationResponseReader(Propertymap);
						ReportPrinter.append("<span><center><p class='Hedding0'>Cancellation Response Validation</p></center></span>");
						ReportPrinter.append("<table style=width:100%>"
						+ "<tr><th>Test Case</th>"
						+ "<th>Test Description</th>"
						+ "<th>Expected Result</th>"
						+ "<th>Actual Result</th>"
						+ "<th>Test Status</th></tr>");
						cancellationResponse = cancellationResReader.RequestReader(XMLLocateType.TRACER, /*searchObject.getTracer()*/confirmationpage.getSupplierConfirmationNo().trim(), XMLFileType.AIR1_CancellationResponse_);
						ReportPrinter = validate.validateCancellationResponse(cancellationResponse, confirmationpage, cancellationReport, ReportPrinter);
						ReportPrinter.append("</table>");
						ReportPrinter.append("<br><br><br>");
					} catch (Exception e) {
						
					}
					ReportPrinter.append("</table>");
					ReportPrinter.append("<br><br><br>");
					
				}
				else
				{
					ReportPrinter.append("<td>"+cancellationReport.isReportLoded()+"</td>"
					+ "<td class='Failed'>Fail</td></tr>");
					Reportcount++;
					ReportPrinter.append("</table>");
					ReportPrinter.append("<br><br><br>");
				}
			}
			
		} catch (Exception e) {
			ReportPrinter.append("</table>");
			ReportPrinter.append("<br><br><br>");
		}
		
		return ReportPrinter;
	}
	
	public StringBuffer validateProfitLossReportAfterCancel(WebDriver driver, CancellationScreen cancellationScreen, CancellationReport cancellationReport, UIConfirmationPage confirmationpage, SearchObject searchObject, ReservationInfo fillingObject, XMLPriceItinerary XMLSelectFlight, AirConfig configuration, StringBuffer ReportPrinter, int Reportcount)
	{
		String ReservationNo = "";
		ReservationNo = confirmationpage.getReservationNo().trim().concat("-C");
			
		//Profit and Loss Report VALIDATION
		try
		{
			ProfitAndLoss profitlossReport	= new ProfitAndLoss(Propertymap);
			profitlossReport.loadReport(driver, ReservationNo);
			//System.out.println();
			ReportPrinter.append("<span><center><p class='Hedding0'>Profit and Loss Validation After Cancellation</p></center></span>");
			ReportPrinter.append("<table style=width:100%>"
			+ "<tr><th>Test Case</th>"
			+ "<th>Test Description</th>"
			+ "<th>Expected Result</th>"
			+ "<th>Actual Result</th>"
			+ "<th>Test Status</th></tr>");
			ReportPrinter.append("<tr><td>"+Reportcount+"</td>"
			+ "<td>Check profit and loss report availability</td>"
			+ "<td>If Available - true , If Not Available - false</td>");
			if(profitlossReport.isReportLoaded())
			{
				ReportPrinter.append("<td>"+profitlossReport.isReportLoaded()+"</td>"
				+ "<td class='Passed'>PASS</td></tr>");
				Reportcount++;
				
				ReportPrinter = validate.validateProfitAndLossAfterCancellation(profitlossReport, cancellationScreen, cancellationReport, XMLSelectFlight, searchObject, ReportPrinter);
				//System.out.println();
				ReportPrinter.append("</table>");
				ReportPrinter.append("<br><br><br>");
			}
			else
			{
				ReportPrinter.append("<td>"+profitlossReport.isReportLoaded()+"</td>"
				+ "<td class='Failed'>Fail</td></tr>");
				ReportPrinter.append("</table>");
				ReportPrinter.append("<br><br><br>");
			}
		}
		catch (Exception e)
		{
			e.printStackTrace();
			ReportPrinter.append("</table>");
			ReportPrinter.append("<br><br><br>");
		}
		return ReportPrinter;
	}
	
	public StringBuffer validateThirdPartySuppPayableAfterCancel(WebDriver driver, UIConfirmationPage uiconfirmationpage, SearchObject searchObject, XMLPriceItinerary XMLSelectFlight, AirConfig configuration, StringBuffer ReportPrinter, int Reportcount)
	{
		String ReservationNo = "";
		ReservationNo = uiconfirmationpage.getReservationNo().trim();
			
		//SUPPLIER PAYABLE REPORT VALIDATION
		try
		{
			ReportPrinter.append("<span><center><p class='Hedding0'>Supplier Payable Report Validation After Cancellation</p></center></span>");
			ReportPrinter.append("<table style=width:100%>"
			+ "<tr><th>Test Case</th>"
			+ "<th>Test Description</th>"
			+ "<th>Expected Result</th>"
			+ "<th>Actual Result</th>"
			+ "<th>Test Status</th></tr>");
			
			SupplierPayableReport suppPayableReport = new SupplierPayableReport(Propertymap);
			
			suppPayableReport.getPaybleDetails(driver, Propertymap.get("Flight_Supplier"), ReservationNo);
			SupplierPayableReport suppPayableReportBeforePay = suppPayableReport;
			
			if( searchObject.getSupplierPayablePayStatus().equalsIgnoreCase("YES") )
			{
				suppPayableReport.paySupplier(Propertymap.get("Payment.Reference"));
				//SupplierPayableReport suppPayableReportAfterPay = suppPayableReport;
			}
			
			ReportPrinter.append("<tr><td>"+Reportcount+"</td>"
			+ "<td>Check Supplier Payable Report availability</td>"
			+ "<td>If Available - true , If Not Available - false</td>");
			
			if(suppPayableReport.isRecordExist())
			{
				ReportPrinter.append("<td>"+suppPayableReport.isRecordExist()+"</td>"
				+ "<td class='Passed'>PASS</td></tr>");
				Reportcount++;	
				
				ReportPrinter = validate.validateSupplierPayableAfterCancellation(uiconfirmationpage, suppPayableReportBeforePay, XMLSelectFlight, ReportPrinter);
				
				ReportPrinter.append("</table>");
				ReportPrinter.append("<br><br><br>");
			}
			else
			{
				ReportPrinter.append("<td>"+suppPayableReport.isRecordExist()+"</td>"
				+ "<td class='Failed'>Fail</td></tr>");
				ReportPrinter.append("</table>");
				ReportPrinter.append("<br><br><br>");
			}
		}
		catch (Exception e)
		{
			//e.printStackTrace();
			ReportPrinter.append("</table>");
			ReportPrinter.append("<br><br><br>");
		}

		return ReportPrinter;
	}
	
	public StringBuffer validateReservationReportAfterCancel(WebDriver driver, UIConfirmationPage confirmationpage, SearchObject searchObject, XMLPriceItinerary XMLSelectFlight, AirConfig configuration, StringBuffer ReportPrinter, int Reportcount)
	{
		String ReservationNo = "";
		ReservationNo = confirmationpage.getReservationNo().trim();
			
		//RESERVATION REPORT VALIDATION
		try
		{
			ReportPrinter.append("<span><center><p class='Hedding0'>Reservation Report Validation After Cancellation</p></center></span>");
			ReportPrinter.append("<table style=width:100%>"
			+ "<tr><th>Test Case</th>"
			+ "<th>Test Description</th>"
			+ "<th>Expected Result</th>"
			+ "<th>Actual Result</th>"
			+ "<th>Test Status</th></tr>");
			
			ReservationReportReader	resvReportReader	= new ReservationReportReader(Propertymap);
			ReservationReport		reservationReport	= new ReservationReport();
			try {
				reservationReport	= resvReportReader.getReport(driver, ReservationNo);
			} catch (Exception e) {
				System.out.println();
			}
			
			ReportPrinter.append("<tr><td>"+Reportcount+"</td>"
			+ "<td>Reservation should not exist in report after cancellation</td>"
			+ "<td>If Available - true , If Not Available - false</td>");
			
			if(!reservationReport.isAvailable())
			{
				ReportPrinter.append("<td>"+reservationReport.isAvailable()+"</td>"
				+ "<td class='Passed'>PASS</td></tr>");
				Reportcount++;	
				ReportPrinter.append("</table>");
				ReportPrinter.append("<br><br><br>");
			}
			else
			{
				ReportPrinter.append("<td>"+reservationReport.isAvailable()+"</td>"
				+ "<td class='Failed'>Fail</td></tr>");
				ReportPrinter.append("</table>");
				ReportPrinter.append("<br><br><br>");
			}
		}
		catch (Exception e)
		{
			e.printStackTrace();
			ReportPrinter.append("</table>");
			ReportPrinter.append("<br><br><br>");
		}

		return ReportPrinter;
		
	}
	
	public StringBuffer validateBookingListReportAfterCancel(WebDriver driver, UIConfirmationPage confirmationpage, SearchObject searchObject, ReservationInfo fillingObject, XMLPriceItinerary XMLSelectFlight, AirConfig configuration, StringBuffer ReportPrinter, int Reportcount)
	{
		String ReservationNo = "";
		ReservationNo = confirmationpage.getReservationNo().trim();
			
		//BOOKING LIST REPORT VALIDATION
		try
		{
			ReportPrinter.append("<span><center><p class='Hedding0'>Booking List Report Validation After Cancellation</p></center></span>");
			ReportPrinter.append("<table style=width:100%>"
			+ "<tr><th>Test Case</th>"
			+ "<th>Test Description</th>"
			+ "<th>Expected Result</th>"
			+ "<th>Actual Result</th>"
			+ "<th>Test Status</th></tr>");
			
			BookingListReport	report	= new BookingListReport(Propertymap);
			report.getReport(driver, ReservationNo);
			
			ReportPrinter.append("<tr><td>"+Reportcount+"</td>"
			+ "<td>Check Customer Voucher email availability</td>"
			+ "<td>If Available - true , If Not Available - false</td>");
			if(report.isIsreportLoaded())
			{
				ReportPrinter.append("<td>"+report.isIsreportLoaded()+"</td>"
				+ "<td class='Passed'>PASS</td></tr>");
				Reportcount++;
				System.out.println();
				ReportPrinter = validate.validateBookingListReportAfterCancellation(report, fillingObject, XMLSelectFlight, searchObject, confirmationpage, ReportPrinter);
				
				ReportPrinter.append("</table>");
				ReportPrinter.append("<br><br><br>");
			}
			else
			{
				ReportPrinter.append("<td>"+report.isIsreportLoaded()+"</td>"
				+ "<td class='Failed'>Fail</td></tr>");
				ReportPrinter.append("</table>");
				ReportPrinter.append("<br><br><br>");
			}
		}
		catch (Exception e)
		{
			ReportPrinter.append("</table>");
			ReportPrinter.append("<br><br><br>");
		}
		return ReportPrinter;
	}
	
	
	
	
	/*public ValidateReports(HashMap<String, String>	map, Validate validate)
	{
		Propertymap = map;
	}
	
	public StringBuffer validateReservationReport(WebDriver driver, WebConfirmationPage confirmationpage, StringBuffer ReportPrinter, SearchObject searchObject, XMLPriceItinerary XMLSelectFlight, int Reportcount)
	{
		String ReservationNo = "";
		ReservationNo = confirmationpage.getReservationNo().trim();
			
		//RESERVATION REPORT VALIDATION
		try
		{
			ReportPrinter.append("<span><center><p class='Hedding0'>Reservation Report Validation</p></center></span>");
			ReportPrinter.append("<table style=width:100%>"
			+ "<tr><th>Test Case</th>"
			+ "<th>Test Description</th>"
			+ "<th>Expected Result</th>"
			+ "<th>Actual Result</th>"
			+ "<th>Test Status</th></tr>");
			
			ReservationReportReader	resvReportReader	= new ReservationReportReader(Propertymap);
			ReservationReport		reservationReport	= new ReservationReport();
			try {
				reservationReport	= resvReportReader.getReport(driver, ReservationNo);
			} catch (Exception e) {
				
			}
			
			ReportPrinter.append("<tr><td>"+Reportcount+"</td>"
			+ "<td>Check Reservation Report availability</td>"
			+ "<td>If Available - true , If Not Available - false</td>");
			
			if(reservationReport.isAvailable())
			{
				ReportPrinter.append("<td>"+reservationReport.isAvailable()+"</td>"
				+ "<td class='Passed'>PASS</td></tr>");
				Reportcount++;	
				
				ReportPrinter	= validate.validateReservationReport(reservationReport, searchObject, XMLSelectFlight, ReportPrinter);
				
				ReportPrinter.append("</table>");
				ReportPrinter.append("<br><br><br>");
			}
			else
			{
				ReportPrinter.append("<td>"+reservationReport.isAvailable()+"</td>"
				+ "<td class='Failed'>Fail</td></tr>");
				ReportPrinter.append("</table>");
				ReportPrinter.append("<br><br><br>");
			}
		}
		catch (Exception e)
		{
			e.printStackTrace();
			ReportPrinter.append("</table>");
			ReportPrinter.append("<br><br><br>");
		}

		return ReportPrinter;
	}
	
	public StringBuffer validateSuppPlayableReport(WebDriver driver, WebConfirmationPage confirmationpage, StringBuffer ReportPrinter, SearchObject searchObject, ReservationInfo fillingObject, XMLPriceItinerary XMLSelectFlight, int Reportcount)
	{
		String ReservationNo = "";
		ReservationNo = confirmationpage.getReservationNo().trim();
			
		//SUPPLIER PAYABLE REPORT VALIDATION
		try
		{
			ReportPrinter.append("<span><center><p class='Hedding0'>Supplier Payable Report Validation</p></center></span>");
			ReportPrinter.append("<table style=width:100%>"
			+ "<tr><th>Test Case</th>"
			+ "<th>Test Description</th>"
			+ "<th>Expected Result</th>"
			+ "<th>Actual Result</th>"
			+ "<th>Test Status</th></tr>");
			
			SupplierPayableReport suppPayableReport = new SupplierPayableReport(Propertymap);
			
			suppPayableReport.getPaybleDetails(driver, Propertymap.get("Flight_Supplier"), ReservationNo);
			SupplierPayableReport suppPayableReportBeforePay = suppPayableReport;
			
			if( searchObject.getSupplierPayablePayStatus().equalsIgnoreCase("YES") )
			{
				suppPayableReport.paySupplier(Propertymap.get("Payment.Reference"));
			}
			
			SupplierPayableReport suppPayableReportAfterPay = suppPayableReport;
			
			ReportPrinter.append("<tr><td>"+Reportcount+"</td>"
			+ "<td>Check Supplier Payable Report availability</td>"
			+ "<td>If Available - true , If Not Available - false</td>");
			
			if(suppPayableReport.isRecordExist())
			{
				ReportPrinter.append("<td>"+suppPayableReport.isRecordExist()+"</td>"
				+ "<td class='Passed'>PASS</td></tr>");
				Reportcount++;	
				
				ReportPrinter = validate.validateSupplierPayableReport(suppPayableReportBeforePay, suppPayableReportAfterPay, searchObject, XMLSelectFlight, confirmationpage, fillingObject, ReportPrinter);
				
				ReportPrinter.append("</table>");
				ReportPrinter.append("<br><br><br>");
			}
			else
			{
				ReportPrinter.append("<td>"+suppPayableReport.isRecordExist()+"</td>"
				+ "<td class='Failed'>Fail</td></tr>");
				ReportPrinter.append("</table>");
				ReportPrinter.append("<br><br><br>");
			}
		}
		catch (Exception e)
		{
			e.printStackTrace();
			ReportPrinter.append("</table>");
			ReportPrinter.append("<br><br><br>");
		}

		return ReportPrinter;
	}
	
	public StringBuffer validateCustomerVoucherEmail(WebDriver driver, WebConfirmationPage confirmationpage, StringBuffer ReportPrinter, SearchObject searchObject, XMLPriceItinerary XMLSelectFlight, ReservationInfo fillingObject, int Reportcount)
	{
		String ReservationNo = "";
		ReservationNo = confirmationpage.getReservationNo().trim();
		//CUSTOMER VOUCHER VALIDATION
		try
		{
			ReportPrinter.append("<span><center><p class='Hedding0'>Customer Voucher E-mail Validation</p></center></span>");
			ReportPrinter.append("<table style=width:100%>"
			+ "<tr><th>Test Case</th>"
			+ "<th>Test Description</th>"
			+ "<th>Expected Result</th>"
			+ "<th>Actual Result</th>"
			+ "<th>Test Status</th></tr>");
			
			CustomerVoucherEmail	email	= new CustomerVoucherEmail(Propertymap);
			email.getCusVoucherEmail(driver, ReservationNo);
			
			ReportPrinter.append("<tr><td>"+Reportcount+"</td>"
			+ "<td>Check Customer Voucher email availability</td>"
			+ "<td>If Available - true , If Not Available - false</td>");
			
			if(email.isAvailable())
			{
				ReportPrinter.append("<td>"+email.isAvailable()+"</td>"
				+ "<td class='Passed'>PASS</td></tr>");
				Reportcount++;
				
				ReportPrinter			= validate.validateCustomerVoucherEmail(email, fillingObject, confirmationpage, XMLSelectFlight, searchObject, ReportPrinter);
				
				ReportPrinter.append("</table>");
				ReportPrinter.append("<br><br><br>");
			}
			else
			{
				ReportPrinter.append("<td>"+email.isAvailable()+"</td>"
				+ "<td class='Failed'>Fail</td></tr>");
				ReportPrinter.append("</table>");
				ReportPrinter.append("<br><br><br>");
			}
		}
		catch (Exception e)
		{
			e.printStackTrace();
			ReportPrinter.append("</table>");
			ReportPrinter.append("<br><br><br>");
		}

		return ReportPrinter;
	}	
	
	public StringBuffer validateBookingConfReport(WebDriver driver, WebConfirmationPage confirmationpage, SearchObject searchObject, ReservationInfo fillingObject, XMLPriceItinerary XMLSelectFlight, StringBuffer ReportPrinter, int Reportcount)
	{
		String ReservationNo = "";
		ReservationNo = confirmationpage.getReservationNo().trim();
		
		try
		{
			ReportPrinter.append("<span><center><p class='Hedding0'>Booking Confirmation Report Validation</p></center></span>");
			ReportPrinter.append("<table style=width:100%>"
			+ "<tr><th>Test Case</th>"
			+ "<th>Test Description</th>"
			+ "<th>Expected Result</th>"
			+ "<th>Actual Result</th>"
			+ "<th>Test Status</th></tr>");
			
			BookingConfReportReader	report	= new BookingConfReportReader(Propertymap);
			report.getReport(driver, ReservationNo);
			
			ReportPrinter.append("<tr><td>"+Reportcount+"</td>"
			+ "<td>Check Booking Confirmation Report availability</td>"
			+ "<td>If Available - true , If Not Available - false</td>");
			if(report.isAvailable())
			{
				ReportPrinter.append("<td>"+report.isAvailable()+"</td>"
				+ "<td class='Passed'>PASS</td></tr>");
				Reportcount++;  
				
				ReportPrinter = validate.validateBookingConfEmail(report, searchObject, fillingObject, XMLSelectFlight,ReportPrinter);
				
				ReportPrinter.append("</table>");
				ReportPrinter.append("<br><br><br>");
			}
			else
			{
				ReportPrinter.append("<td>"+report.isAvailable()+"</td>"
				+ "<td class='Failed'>Fail</td></tr>");
				ReportPrinter.append("</table>");
				ReportPrinter.append("<br><br><br>");
			}
		}
		catch (Exception e)
		{
			e.printStackTrace();
			ReportPrinter.append("</table>");
			ReportPrinter.append("<br><br><br>");
		}
		
		return ReportPrinter;
	}
	
	public StringBuffer validateBookingListReport(WebDriver driver, WebConfirmationPage confirmationpage, SearchObject searchObject, ReservationInfo fillingObject, XMLPriceItinerary XMLSelectFlight, StringBuffer ReportPrinter, int Reportcount)
	{
		String ReservationNo = "";
		ReservationNo = confirmationpage.getReservationNo().trim();
			
		//BOOKING LIST REPORT VALIDATION
		try
		{
			ReportPrinter.append("<span><center><p class='Hedding0'>Booking List Report Validation</p></center></span>");
			ReportPrinter.append("<table style=width:100%>"
			+ "<tr><th>Test Case</th>"
			+ "<th>Test Description</th>"
			+ "<th>Expected Result</th>"
			+ "<th>Actual Result</th>"
			+ "<th>Test Status</th></tr>");
			
			BookingListReport	report	= new BookingListReport(Propertymap);
			report.getReport(driver, ReservationNo);
			
			ReportPrinter.append("<tr><td>"+Reportcount+"</td>"
			+ "<td>Check Customer Voucher email availability</td>"
			+ "<td>If Available - true , If Not Available - false</td>");
			if(report.isIsreportLoaded())
			{
				ReportPrinter.append("<td>"+report.isIsreportLoaded()+"</td>"
				+ "<td class='Passed'>PASS</td></tr>");
				Reportcount++;
				
				ReportPrinter = validate.validateBookingListReport(report, fillingObject, XMLSelectFlight, searchObject, confirmationpage, ReportPrinter);
				
				ReportPrinter.append("</table>");
				ReportPrinter.append("<br><br><br>");
			}
			else
			{
				ReportPrinter.append("<td>"+report.isIsreportLoaded()+"</td>"
				+ "<td class='Failed'>Fail</td></tr>");
				ReportPrinter.append("</table>");
				ReportPrinter.append("<br><br><br>");
			}
		}
		catch (Exception e)
		{
			//System.out.println();
			e.printStackTrace();
			ReportPrinter.append("</table>");
			ReportPrinter.append("<br><br><br>");
		}
		return ReportPrinter;
	}
	
	public StringBuffer validateProfitLossReport(WebDriver driver, WebConfirmationPage confirmationpage, SearchObject searchObject, ReservationInfo fillingObject, XMLPriceItinerary XMLSelectFlight, StringBuffer ReportPrinter, int Reportcount)
	{
		String ReservationNo = "";
		ReservationNo = confirmationpage.getReservationNo().trim();
			
		//Profit and Loss Report VALIDATION
		try
		{
			ProfitAndLoss report	= new ProfitAndLoss(Propertymap);
			try {
				report.loadReport(driver, ReservationNo);
			} catch (Exception e) {
				
			}
			
			
			ReportPrinter.append("<span><center><p class='Hedding0'>Profit and Loss Validation</p></center></span>");
			ReportPrinter.append("<table style=width:100%>"
			+ "<tr><th>Test Case</th>"
			+ "<th>Test Description</th>"
			+ "<th>Expected Result</th>"
			+ "<th>Actual Result</th>"
			+ "<th>Test Status</th></tr>");
			ReportPrinter.append("<tr><td>"+Reportcount+"</td>"
			+ "<td>Check profit and loss report availability</td>"
			+ "<td>If Available - true , If Not Available - false</td>");
			if(report.isReportLoaded())
			{
				ReportPrinter.append("<td>"+report.isReportLoaded()+"</td>"
				+ "<td class='Passed'>PASS</td></tr>");
				Reportcount++;
				
				ReportPrinter = validate.validateProfitAndLoss(XMLSelectFlight, searchObject, report, ReportPrinter);
				
				ReportPrinter.append("</table>");
				ReportPrinter.append("<br><br><br>");
			}
			else
			{
				ReportPrinter.append("<td>"+report.isReportLoaded()+"</td>"
				+ "<td class='Failed'>Fail</td></tr>");
				ReportPrinter.append("</table>");
				ReportPrinter.append("<br><br><br>");
			}
		}
		catch (Exception e)
		{
			e.printStackTrace();
			ReportPrinter.append("</table>");
			ReportPrinter.append("<br><br><br>");
		}
		return ReportPrinter;
	}
	
	public StringBuffer doCancellation(WebDriver driver, CancellationScreen cancellationScreen, CancellationReport cancellationReport, WebConfirmationPage confirmationpage, SearchObject searchObject, ReservationInfo fillingObject, XMLPriceItinerary XMLSelectFlight, StringBuffer ReportPrinter, int Reportcount)
	{
		//======================================CANCELLATION SCREEN VALIDATION=====================================
		
		try {
			
			try {
				cancellationScreen.getCancellationScreen(driver, confirmationpage.getReservationNo(), XMLSelectFlight, searchObject);
			} catch (Exception e) {
				
			}
			ReportPrinter.append("<span><center><p class='Hedding0'>Cancellation Screen Validation</p></center></span>");
			ReportPrinter.append("<table style=width:100%>"
			+ "<tr><th>Test Case</th>"
			+ "<th>Test Description</th>"
			+ "<th>Expected Result</th>"
			+ "<th>Actual Result</th>"
			+ "<th>Test Status</th></tr>");
			ReportPrinter.append("<tr><td>"+Reportcount+"</td>"
			+ "<td>Check Cancellation Screen Availability</td>"
			+ "<td>If Available - true , If Not Available - false</td>");
			if(!cancellationScreen.isNotFound())
			{
				ReportPrinter.append("<td>"+cancellationScreen.isNotFound()+"</td>"
				+ "<td class = 'Passed'>PASS</td></tr>" );
				Reportcount++;
				
				try {
					ReportPrinter = validate.validateCancellationScreen(XMLSelectFlight, searchObject, fillingObject, confirmationpage, cancellationScreen, ReportPrinter);
				} catch (Exception e) {
					
				}
				
				ReportPrinter.append("<tr><td>"+Reportcount+"</td>"
				+ "<td>Check Cancellation Status</td>"
				+ "<td>If Cancellation Done - true and Display Message , If Not Done - false and Display Error Message</td>");
				if(cancellationScreen.isCancelled())
				{
					ReportPrinter.append("<td>"+cancellationScreen.isCancelled()+"<br>"+cancellationScreen.getCancellationMessage()+"</td>"
					+ "<td class = 'Passed'>PASS</td></tr>" );
					Reportcount++;
				}
				else
				{
					ReportPrinter.append("<td>"+cancellationScreen.isCancelled()+"<br>"+cancellationScreen.getCancellationMessage()+"</td>"
					+ "<td class = 'Failed'>Fail</td></tr>");
					Reportcount++;
				}
				
				ReportPrinter.append("</table>");
				ReportPrinter.append("<br><br><br>");	
			}
			else
			{
				ReportPrinter.append("<td>"+cancellationScreen.isNotFound()+"</td>"
				+ "<td class = 'Failed'>Fail</td></tr>");
				Reportcount++;
				ReportPrinter.append("</table>");
				ReportPrinter.append("<br><br><br>");
			}
			
			if( !cancellationScreen.isNotFound() && cancellationScreen.isCancelled() )
			{
				
				try {
					cancellationReport.loadReport(driver, confirmationpage.getReservationNo());
				} catch (Exception e) {
						
				}
					
				ReportPrinter.append("<span><center><p class='Hedding0'>Cancellation Report Validation</p></center></span>");
				ReportPrinter.append("<table style=width:100%>"
				+ "<tr><th>Test Case</th>"
				+ "<th>Test Description</th>"
				+ "<th>Expected Result</th>"
				+ "<th>Actual Result</th>"
				+ "<th>Test Status</th></tr>");
				ReportPrinter.append("<tr><td>"+Reportcount+"</td>"
				+ "<td>Check Cancellation Report Availability</td>"
				+ "<td>If Available - true , If Not Available - false</td>");
				if(cancellationReport.isReportLoded())
				{
					ReportPrinter.append("<td>"+cancellationReport.isReportLoded()+"</td>"
					+ "<td class='Passed'>PASS</td></tr>");
					Reportcount++;
					try {
						ReportPrinter = validate.validateCancellationReport(XMLSelectFlight, searchObject, fillingObject, confirmationpage, cancellationScreen, cancellationReport, ReportPrinter);
					} catch (Exception e) {
						
					}
					ReportPrinter.append("</table>");
					ReportPrinter.append("<br><br><br>");
					
					
					try {
						
						CancellationRequest cancellationRequest = new CancellationRequest();
						CancellationRequestReader cancellationReqReader = new CancellationRequestReader(Propertymap);
						ReportPrinter.append("<span><center><p class='Hedding0'>Cancellation Request Validation</p></center></span>");
						ReportPrinter.append("<table style=width:100%>"
						+ "<tr><th>Test Case</th>"
						+ "<th>Test Description</th>"
						+ "<th>Expected Result</th>"
						+ "<th>Actual Result</th>"
						+ "<th>Test Status</th></tr>");
						cancellationRequest = cancellationReqReader.RequestReader(XMLLocateType.TRACER, searchObject.getTracer()confirmationpage.getSupplierConfirmationNo().trim(), XMLFileType.AIR1_Cancellationrequest_);
						ReportPrinter = validate.validateCancellationRequest(cancellationRequest, confirmationpage, ReportPrinter);
						ReportPrinter.append("</table>");
						ReportPrinter.append("<br><br><br>");
					} catch (Exception e) {
						
					}
					ReportPrinter.append("</table>");
					ReportPrinter.append("<br><br><br>");
					
					try {
						
						CancellationResponse cancellationResponse = new CancellationResponse();
						CancellationResponseReader cancellationResReader = new CancellationResponseReader(Propertymap);
						ReportPrinter.append("<span><center><p class='Hedding0'>Cancellation Response Validation</p></center></span>");
						ReportPrinter.append("<table style=width:100%>"
						+ "<tr><th>Test Case</th>"
						+ "<th>Test Description</th>"
						+ "<th>Expected Result</th>"
						+ "<th>Actual Result</th>"
						+ "<th>Test Status</th></tr>");
						cancellationResponse = cancellationResReader.RequestReader(XMLLocateType.TRACER, searchObject.getTracer()confirmationpage.getSupplierConfirmationNo().trim(), XMLFileType.AIR1_Cancellationrequest_);
						ReportPrinter = validate.validateCancellationResponse(cancellationResponse, confirmationpage, cancellationReport, ReportPrinter);
						ReportPrinter.append("</table>");
						ReportPrinter.append("<br><br><br>");
					} catch (Exception e) {
						
					}
					ReportPrinter.append("</table>");
					ReportPrinter.append("<br><br><br>");
					
				}
				else
				{
					ReportPrinter.append("<td>"+cancellationReport.isReportLoded()+"</td>"
					+ "<td class='Failed'>Fail</td></tr>");
					Reportcount++;
					ReportPrinter.append("</table>");
					ReportPrinter.append("<br><br><br>");
				}
			}
			
		} catch (Exception e) {
			ReportPrinter.append("</table>");
			ReportPrinter.append("<br><br><br>");
		}
		
		return ReportPrinter;
	}
	
	public StringBuffer validateProfitLossReportAfterCancel(WebDriver driver, CancellationScreen cancellationScreen, CancellationReport cancellationReport, WebConfirmationPage confirmationpage, SearchObject searchObject, ReservationInfo fillingObject, XMLPriceItinerary XMLSelectFlight, StringBuffer ReportPrinter, int Reportcount)
	{
		String ReservationNo = "";
		ReservationNo = confirmationpage.getReservationNo().trim().concat("-C");
			
		//Profit and Loss Report VALIDATION
		try
		{
			ProfitAndLoss profitlossReport	= new ProfitAndLoss(Propertymap);
			profitlossReport.loadReport(driver, ReservationNo);
			//System.out.println();
			ReportPrinter.append("<span><center><p class='Hedding0'>Profit and Loss Validation After Cancellation</p></center></span>");
			ReportPrinter.append("<table style=width:100%>"
			+ "<tr><th>Test Case</th>"
			+ "<th>Test Description</th>"
			+ "<th>Expected Result</th>"
			+ "<th>Actual Result</th>"
			+ "<th>Test Status</th></tr>");
			ReportPrinter.append("<tr><td>"+Reportcount+"</td>"
			+ "<td>Check profit and loss report availability</td>"
			+ "<td>If Available - true , If Not Available - false</td>");
			if(profitlossReport.isReportLoaded())
			{
				ReportPrinter.append("<td>"+profitlossReport.isReportLoaded()+"</td>"
				+ "<td class='Passed'>PASS</td></tr>");
				Reportcount++;
				
				ReportPrinter = validate.validateProfitAndLossAfterCancellation(profitlossReport, cancellationScreen, cancellationReport, XMLSelectFlight, searchObject, ReportPrinter);
				//System.out.println();
				ReportPrinter.append("</table>");
				ReportPrinter.append("<br><br><br>");
			}
			else
			{
				ReportPrinter.append("<td>"+profitlossReport.isReportLoaded()+"</td>"
				+ "<td class='Failed'>Fail</td></tr>");
				ReportPrinter.append("</table>");
				ReportPrinter.append("<br><br><br>");
			}
		}
		catch (Exception e)
		{
			e.printStackTrace();
			ReportPrinter.append("</table>");
			ReportPrinter.append("<br><br><br>");
		}
		return ReportPrinter;
	}
	
	public StringBuffer validateThirdPartySuppPayableAfterCancel(WebDriver driver, WebConfirmationPage uiconfirmationpage, SearchObject searchObject, XMLPriceItinerary XMLSelectFlight, StringBuffer ReportPrinter, int Reportcount)
	{
		String ReservationNo = "";
		ReservationNo = uiconfirmationpage.getReservationNo().trim();
			
		//SUPPLIER PAYABLE REPORT VALIDATION
		try
		{
			ReportPrinter.append("<span><center><p class='Hedding0'>Supplier Payable Report Validation After Cancellation</p></center></span>");
			ReportPrinter.append("<table style=width:100%>"
			+ "<tr><th>Test Case</th>"
			+ "<th>Test Description</th>"
			+ "<th>Expected Result</th>"
			+ "<th>Actual Result</th>"
			+ "<th>Test Status</th></tr>");
			
			SupplierPayableReport suppPayableReport = new SupplierPayableReport(Propertymap);
			
			suppPayableReport.getPaybleDetails(driver, Propertymap.get("Flight_Supplier"), ReservationNo);
			SupplierPayableReport suppPayableReportBeforePay = suppPayableReport;
			
			if( searchObject.getSupplierPayablePayStatus().equalsIgnoreCase("YES") )
			{
				suppPayableReport.paySupplier(Propertymap.get("Payment.Reference"));
				//SupplierPayableReport suppPayableReportAfterPay = suppPayableReport;
			}
			
			ReportPrinter.append("<tr><td>"+Reportcount+"</td>"
			+ "<td>Check Supplier Payable Report availability</td>"
			+ "<td>If Available - true , If Not Available - false</td>");
			
			if(suppPayableReport.isRecordExist())
			{
				ReportPrinter.append("<td>"+suppPayableReport.isRecordExist()+"</td>"
				+ "<td class='Passed'>PASS</td></tr>");
				Reportcount++;	
				
				ReportPrinter = validate.validateSupplierPayableAfterCancellation(uiconfirmationpage, suppPayableReportBeforePay, XMLSelectFlight, ReportPrinter);
				
				ReportPrinter.append("</table>");
				ReportPrinter.append("<br><br><br>");
			}
			else
			{
				ReportPrinter.append("<td>"+suppPayableReport.isRecordExist()+"</td>"
				+ "<td class='Failed'>Fail</td></tr>");
				ReportPrinter.append("</table>");
				ReportPrinter.append("<br><br><br>");
			}
		}
		catch (Exception e)
		{
			//e.printStackTrace();
			ReportPrinter.append("</table>");
			ReportPrinter.append("<br><br><br>");
		}

		return ReportPrinter;
	}
	
	public StringBuffer validateReservationReportAfterCancel(WebDriver driver, WebConfirmationPage confirmationpage, SearchObject searchObject, XMLPriceItinerary XMLSelectFlight, StringBuffer ReportPrinter, int Reportcount)
	{
		String ReservationNo = "";
		ReservationNo = confirmationpage.getReservationNo().trim();
			
		//RESERVATION REPORT VALIDATION
		try
		{
			ReportPrinter.append("<span><center><p class='Hedding0'>Reservation Report Validation After Cancellation</p></center></span>");
			ReportPrinter.append("<table style=width:100%>"
			+ "<tr><th>Test Case</th>"
			+ "<th>Test Description</th>"
			+ "<th>Expected Result</th>"
			+ "<th>Actual Result</th>"
			+ "<th>Test Status</th></tr>");
			
			ReservationReportReader	resvReportReader	= new ReservationReportReader(Propertymap);
			ReservationReport		reservationReport	= new ReservationReport();
			try {
				reservationReport	= resvReportReader.getReport(driver, ReservationNo);
			} catch (Exception e) {
				System.out.println();
			}
			
			ReportPrinter.append("<tr><td>"+Reportcount+"</td>"
			+ "<td>Reservation should not exist in report after cancellation</td>"
			+ "<td>If Available - true , If Not Available - false</td>");
			
			if(!reservationReport.isAvailable())
			{
				ReportPrinter.append("<td>"+reservationReport.isAvailable()+"</td>"
				+ "<td class='Passed'>PASS</td></tr>");
				Reportcount++;	
				ReportPrinter.append("</table>");
				ReportPrinter.append("<br><br><br>");
			}
			else
			{
				ReportPrinter.append("<td>"+reservationReport.isAvailable()+"</td>"
				+ "<td class='Failed'>Fail</td></tr>");
				ReportPrinter.append("</table>");
				ReportPrinter.append("<br><br><br>");
			}
		}
		catch (Exception e)
		{
			e.printStackTrace();
			ReportPrinter.append("</table>");
			ReportPrinter.append("<br><br><br>");
		}

		return ReportPrinter;
		
	}
	
	public StringBuffer validateBookingListReportAfterCancel(WebDriver driver, WebConfirmationPage confirmationpage, SearchObject searchObject, ReservationInfo fillingObject, XMLPriceItinerary XMLSelectFlight, StringBuffer ReportPrinter, int Reportcount)
	{
		String ReservationNo = "";
		ReservationNo = confirmationpage.getReservationNo().trim();
			
		//BOOKING LIST REPORT VALIDATION
		try
		{
			ReportPrinter.append("<span><center><p class='Hedding0'>Booking List Report Validation After Cancellation</p></center></span>");
			ReportPrinter.append("<table style=width:100%>"
			+ "<tr><th>Test Case</th>"
			+ "<th>Test Description</th>"
			+ "<th>Expected Result</th>"
			+ "<th>Actual Result</th>"
			+ "<th>Test Status</th></tr>");
			
			BookingListReport	report	= new BookingListReport(Propertymap);
			report.getReport(driver, ReservationNo);
			
			ReportPrinter.append("<tr><td>"+Reportcount+"</td>"
			+ "<td>Check Customer Voucher email availability</td>"
			+ "<td>If Available - true , If Not Available - false</td>");
			if(report.isIsreportLoaded())
			{
				ReportPrinter.append("<td>"+report.isIsreportLoaded()+"</td>"
				+ "<td class='Passed'>PASS</td></tr>");
				Reportcount++;
				System.out.println();
				ReportPrinter = validate.validateBookingListReportAfterCancellation(report, fillingObject, XMLSelectFlight, searchObject, confirmationpage, ReportPrinter);
				
				ReportPrinter.append("</table>");
				ReportPrinter.append("<br><br><br>");
			}
			else
			{
				ReportPrinter.append("<td>"+report.isIsreportLoaded()+"</td>"
				+ "<td class='Failed'>Fail</td></tr>");
				ReportPrinter.append("</table>");
				ReportPrinter.append("<br><br><br>");
			}
		}
		catch (Exception e)
		{
			//System.out.println();
			//e.printStackTrace();
			ReportPrinter.append("</table>");
			ReportPrinter.append("<br><br><br>");
		}
		return ReportPrinter;
	}
	*/
	
}
